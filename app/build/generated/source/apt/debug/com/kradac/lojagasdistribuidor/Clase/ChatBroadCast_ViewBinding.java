// Generated code from Butter Knife. Do not modify!
package com.kradac.lojagasdistribuidor.Clase;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.kradac.lojagasdistribuidor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ChatBroadCast_ViewBinding implements Unbinder {
  private ChatBroadCast target;

  @UiThread
  public ChatBroadCast_ViewBinding(ChatBroadCast target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ChatBroadCast_ViewBinding(ChatBroadCast target, View source) {
    this.target = target;

    target.tabLayout = Utils.findRequiredViewAsType(source, R.id.tab_layout, "field 'tabLayout'", TabLayout.class);
    target.pager = Utils.findRequiredViewAsType(source, R.id.pager, "field 'pager'", ViewPager.class);
    target.txtVersion = Utils.findRequiredViewAsType(source, R.id.txt_version, "field 'txtVersion'", TextView.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.mainContent = Utils.findRequiredViewAsType(source, R.id.main_content, "field 'mainContent'", CoordinatorLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ChatBroadCast target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tabLayout = null;
    target.pager = null;
    target.txtVersion = null;
    target.toolbar = null;
    target.mainContent = null;
  }
}
