// Generated code from Butter Knife. Do not modify!
package com.kradac.lojagasdistribuidor.Clase;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.kradac.lojagasdistribuidor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HistorialSolicitudes_ViewBinding implements Unbinder {
  private HistorialSolicitudes target;

  @UiThread
  public HistorialSolicitudes_ViewBinding(HistorialSolicitudes target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public HistorialSolicitudes_ViewBinding(HistorialSolicitudes target, View source) {
    this.target = target;

    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.recyclerHistorial = Utils.findRequiredViewAsType(source, R.id.recycler_historial, "field 'recyclerHistorial'", RecyclerView.class);
    target.txtVersion = Utils.findRequiredViewAsType(source, R.id.txt_version, "field 'txtVersion'", TextView.class);
    target.txtNumeroSolicitudes = Utils.findRequiredViewAsType(source, R.id.txt_numero_solicitudes, "field 'txtNumeroSolicitudes'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    HistorialSolicitudes target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.toolbar = null;
    target.recyclerHistorial = null;
    target.txtVersion = null;
    target.txtNumeroSolicitudes = null;
  }
}
