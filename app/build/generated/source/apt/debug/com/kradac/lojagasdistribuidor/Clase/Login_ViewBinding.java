// Generated code from Butter Knife. Do not modify!
package com.kradac.lojagasdistribuidor.Clase;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.kradac.lojagasdistribuidor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Login_ViewBinding implements Unbinder {
  private Login target;

  @UiThread
  public Login_ViewBinding(Login target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public Login_ViewBinding(Login target, View source) {
    this.target = target;

    target.editPassUsuario = Utils.findRequiredViewAsType(source, R.id.editPassUsuario, "field 'editPassUsuario'", EditText.class);
    target.chPass = Utils.findRequiredViewAsType(source, R.id.ch_pass, "field 'chPass'", CheckBox.class);
    target.btnIniciarSesion = Utils.findRequiredViewAsType(source, R.id.btn_iniciar_sesion, "field 'btnIniciarSesion'", Button.class);
    target.btnRegistrarse = Utils.findRequiredViewAsType(source, R.id.btn_registrarse, "field 'btnRegistrarse'", Button.class);
    target.btnRecuperar = Utils.findRequiredViewAsType(source, R.id.btn_recuperar, "field 'btnRecuperar'", Button.class);
    target.editUsuario = Utils.findRequiredViewAsType(source, R.id.editUsuario, "field 'editUsuario'", EditText.class);
    target.mainContent = Utils.findRequiredViewAsType(source, R.id.main_content, "field 'mainContent'", CoordinatorLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    Login target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.editPassUsuario = null;
    target.chPass = null;
    target.btnIniciarSesion = null;
    target.btnRegistrarse = null;
    target.btnRecuperar = null;
    target.editUsuario = null;
    target.mainContent = null;
  }
}
