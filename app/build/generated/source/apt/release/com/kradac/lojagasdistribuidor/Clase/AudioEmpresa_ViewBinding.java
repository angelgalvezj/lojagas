// Generated code from Butter Knife. Do not modify!
package com.kradac.lojagasdistribuidor.Clase;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.kradac.lojagasdistribuidor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AudioEmpresa_ViewBinding implements Unbinder {
  private AudioEmpresa target;

  @UiThread
  public AudioEmpresa_ViewBinding(AudioEmpresa target, View source) {
    this.target = target;

    target.messagesContainer = Utils.findRequiredViewAsType(source, R.id.messagesContainer, "field 'messagesContainer'", ListView.class);
    target.btnAudioEmpresa = Utils.findRequiredViewAsType(source, R.id.btn_audio_empresa, "field 'btnAudioEmpresa'", ImageButton.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AudioEmpresa target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.messagesContainer = null;
    target.btnAudioEmpresa = null;
  }
}
