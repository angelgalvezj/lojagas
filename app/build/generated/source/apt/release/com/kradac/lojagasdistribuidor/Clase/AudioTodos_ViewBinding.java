// Generated code from Butter Knife. Do not modify!
package com.kradac.lojagasdistribuidor.Clase;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.kradac.lojagasdistribuidor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AudioTodos_ViewBinding implements Unbinder {
  private AudioTodos target;

  @UiThread
  public AudioTodos_ViewBinding(AudioTodos target, View source) {
    this.target = target;

    target.btnAudioTodos = Utils.findRequiredViewAsType(source, R.id.btn_audio_todos, "field 'btnAudioTodos'", ImageButton.class);
    target.messagesContainer = Utils.findRequiredViewAsType(source, R.id.messagesContainer, "field 'messagesContainer'", ListView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AudioTodos target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnAudioTodos = null;
    target.messagesContainer = null;
  }
}
