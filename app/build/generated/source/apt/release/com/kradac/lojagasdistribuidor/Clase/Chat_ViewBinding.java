// Generated code from Butter Knife. Do not modify!
package com.kradac.lojagasdistribuidor.Clase;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.kradac.lojagasdistribuidor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Chat_ViewBinding implements Unbinder {
  private Chat target;

  @UiThread
  public Chat_ViewBinding(Chat target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public Chat_ViewBinding(Chat target, View source) {
    this.target = target;

    target.txtVersion = Utils.findRequiredViewAsType(source, R.id.txt_version, "field 'txtVersion'", TextView.class);
    target.mainContent = Utils.findRequiredViewAsType(source, R.id.main_content, "field 'mainContent'", CoordinatorLayout.class);
    target.messagesContainer = Utils.findRequiredViewAsType(source, R.id.messagesContainer, "field 'messagesContainer'", ListView.class);
    target.messageEdit = Utils.findRequiredViewAsType(source, R.id.messageEdit, "field 'messageEdit'", EditText.class);
    target.chatSendButton = Utils.findRequiredViewAsType(source, R.id.chatSendButton, "field 'chatSendButton'", ImageButton.class);
    target.btnAudio = Utils.findRequiredViewAsType(source, R.id.btn_audio, "field 'btnAudio'", ImageButton.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    Chat target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtVersion = null;
    target.mainContent = null;
    target.messagesContainer = null;
    target.messageEdit = null;
    target.chatSendButton = null;
    target.btnAudio = null;
    target.toolbar = null;
  }
}
