// Generated code from Butter Knife. Do not modify!
package com.kradac.lojagasdistribuidor.Clase;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.kradac.lojagasdistribuidor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ListaSolicitudes_ViewBinding implements Unbinder {
  private ListaSolicitudes target;

  @UiThread
  public ListaSolicitudes_ViewBinding(ListaSolicitudes target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ListaSolicitudes_ViewBinding(ListaSolicitudes target, View source) {
    this.target = target;

    target.txtVersion = Utils.findRequiredViewAsType(source, R.id.txt_version, "field 'txtVersion'", TextView.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.recyclerSolicitud = Utils.findRequiredViewAsType(source, R.id.recycler_solicitud, "field 'recyclerSolicitud'", RecyclerView.class);
    target.recyclerSolicitudPendientes = Utils.findRequiredViewAsType(source, R.id.recycler_solicitud_pendientes, "field 'recyclerSolicitudPendientes'", RecyclerView.class);
    target.contSolicitudes = Utils.findRequiredViewAsType(source, R.id.cont_solicitudes, "field 'contSolicitudes'", LinearLayout.class);
    target.contSolicitudesPendientes = Utils.findRequiredViewAsType(source, R.id.cont_solicitudes_pendientes, "field 'contSolicitudesPendientes'", LinearLayout.class);
    target.txtMensaje = Utils.findRequiredViewAsType(source, R.id.txt_mensaje, "field 'txtMensaje'", TextView.class);
    target.mainContent = Utils.findRequiredViewAsType(source, R.id.main_content, "field 'mainContent'", CoordinatorLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ListaSolicitudes target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtVersion = null;
    target.toolbar = null;
    target.recyclerSolicitud = null;
    target.recyclerSolicitudPendientes = null;
    target.contSolicitudes = null;
    target.contSolicitudesPendientes = null;
    target.txtMensaje = null;
    target.mainContent = null;
  }
}
