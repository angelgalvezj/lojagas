// Generated code from Butter Knife. Do not modify!
package com.kradac.lojagasdistribuidor.Clase;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.kradac.lojagasdistribuidor.R;
import com.kradac.lojagasdistribuidor.Util.CustomEventMapView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainActivity_ViewBinding implements Unbinder {
  private MainActivity target;

  @UiThread
  public MainActivity_ViewBinding(MainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainActivity_ViewBinding(MainActivity target, View source) {
    this.target = target;

    target.mainContent = Utils.findRequiredViewAsType(source, R.id.main_content, "field 'mainContent'", CoordinatorLayout.class);
    target.mapView = Utils.findRequiredViewAsType(source, R.id.mapView, "field 'mapView'", CustomEventMapView.class);
    target.txtPlaca = Utils.findRequiredViewAsType(source, R.id.txt_placa, "field 'txtPlaca'", TextView.class);
    target.txtVersion = Utils.findRequiredViewAsType(source, R.id.txt_version, "field 'txtVersion'", TextView.class);
    target.imgUbicacion = Utils.findRequiredViewAsType(source, R.id.img_ubicacion, "field 'imgUbicacion'", ImageView.class);
    target.imgListaSolicitudes = Utils.findRequiredViewAsType(source, R.id.img_lista_solicitudes, "field 'imgListaSolicitudes'", ImageView.class);
    target.imgRed = Utils.findRequiredViewAsType(source, R.id.img_red, "field 'imgRed'", ImageView.class);
    target.txtUsuario = Utils.findRequiredViewAsType(source, R.id.txt_usuario, "field 'txtUsuario'", TextView.class);
    target.txtBarrio = Utils.findRequiredViewAsType(source, R.id.txt_barrio, "field 'txtBarrio'", TextView.class);
    target.txtCallePrincipal = Utils.findRequiredViewAsType(source, R.id.txt_calle_principal, "field 'txtCallePrincipal'", TextView.class);
    target.txtCalleSecundaria = Utils.findRequiredViewAsType(source, R.id.txt_calle_secundaria, "field 'txtCalleSecundaria'", TextView.class);
    target.contInfo = Utils.findRequiredViewAsType(source, R.id.cont_info, "field 'contInfo'", LinearLayout.class);
    target.imgMore = Utils.findRequiredViewAsType(source, R.id.img_more, "field 'imgMore'", ImageView.class);
    target.contMore = Utils.findRequiredViewAsType(source, R.id.cont_more, "field 'contMore'", LinearLayout.class);
    target.imgLugar = Utils.findRequiredViewAsType(source, R.id.img_lugar, "field 'imgLugar'", ImageView.class);
    target.imgLlamar = Utils.findRequiredViewAsType(source, R.id.img_llamar, "field 'imgLlamar'", ImageView.class);
    target.imgEntregar = Utils.findRequiredViewAsType(source, R.id.img_entregar, "field 'imgEntregar'", ImageView.class);
    target.imgChat = Utils.findRequiredViewAsType(source, R.id.img_chat, "field 'imgChat'", ImageView.class);
    target.txtDetalleSolicitud = Utils.findRequiredViewAsType(source, R.id.txt_detalle_solicitud, "field 'txtDetalleSolicitud'", TextView.class);
    target.btnCancelarEntrega = Utils.findRequiredViewAsType(source, R.id.btn_cancelar_entrega, "field 'btnCancelarEntrega'", Button.class);
    target.contSecundaria = Utils.findRequiredViewAsType(source, R.id.cont_secundaria, "field 'contSecundaria'", LinearLayout.class);
    target.contChat = Utils.findRequiredViewAsType(source, R.id.cont_chat, "field 'contChat'", LinearLayout.class);
    target.recyclerPedido = Utils.findRequiredViewAsType(source, R.id.recycler_pedido, "field 'recyclerPedido'", RecyclerView.class);
    target.contCaracteristicas = Utils.findRequiredViewAsType(source, R.id.cont_caracteristicas, "field 'contCaracteristicas'", LinearLayout.class);
    target.txtReferencia = Utils.findRequiredViewAsType(source, R.id.txt_referencia, "field 'txtReferencia'", TextView.class);
    target.contReferencia = Utils.findRequiredViewAsType(source, R.id.cont_referencia, "field 'contReferencia'", LinearLayout.class);
    target.imgUbicacionSeguimiento = Utils.findRequiredViewAsType(source, R.id.img_ubicacion_seguimiento, "field 'imgUbicacionSeguimiento'", ImageView.class);
    target.imgInfo = Utils.findRequiredViewAsType(source, R.id.img_info, "field 'imgInfo'", ImageView.class);
    target.contUser = Utils.findRequiredViewAsType(source, R.id.cont_user, "field 'contUser'", LinearLayout.class);
    target.txtCronometro = Utils.findRequiredViewAsType(source, R.id.txt_cronometro, "field 'txtCronometro'", TextView.class);
    target.txtIdSolicitud = Utils.findRequiredViewAsType(source, R.id.txt_idSolicitud, "field 'txtIdSolicitud'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mainContent = null;
    target.mapView = null;
    target.txtPlaca = null;
    target.txtVersion = null;
    target.imgUbicacion = null;
    target.imgListaSolicitudes = null;
    target.imgRed = null;
    target.txtUsuario = null;
    target.txtBarrio = null;
    target.txtCallePrincipal = null;
    target.txtCalleSecundaria = null;
    target.contInfo = null;
    target.imgMore = null;
    target.contMore = null;
    target.imgLugar = null;
    target.imgLlamar = null;
    target.imgEntregar = null;
    target.imgChat = null;
    target.txtDetalleSolicitud = null;
    target.btnCancelarEntrega = null;
    target.contSecundaria = null;
    target.contChat = null;
    target.recyclerPedido = null;
    target.contCaracteristicas = null;
    target.txtReferencia = null;
    target.contReferencia = null;
    target.imgUbicacionSeguimiento = null;
    target.imgInfo = null;
    target.contUser = null;
    target.txtCronometro = null;
    target.txtIdSolicitud = null;
  }
}
