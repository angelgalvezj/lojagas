package com.kradac.lojagasdistribuidor.Adaptador;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kradac.lojagasdistribuidor.Modelo.Historial;
import com.kradac.lojagasdistribuidor.Modelo.Usuario;
import com.kradac.lojagasdistribuidor.R;

import java.util.List;

public class AdaptadorListHistorial extends RecyclerView.Adapter<AdaptadorListHistorial.CustomViewHolder> {
    List<Historial> historialList;
    public Context context;
    private OnClicklistener onClicklistener;
    private int anio, mes;

    public interface OnClicklistener {
        void onClic(Historial historial, int posicion);
    }

    public AdaptadorListHistorial(Context context, List<Historial> historialList, int anio,
                                  int mes, OnClicklistener onClicklistener) {
        this.historialList = historialList;
        this.context = context;
        this.onClicklistener = onClicklistener;
        this.anio = anio;
        this.mes = mes;
    }

    @Override
    public AdaptadorListHistorial.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_historial_solicitud, null);
        return new AdaptadorListHistorial.CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdaptadorListHistorial.CustomViewHolder customViewHolder, int i) {
        final Historial historial = historialList.get(i);
        customViewHolder.txtBarrio.setText(historial.getB());
        customViewHolder.txtCalle.setText(historial.getcP() + " y " + historial.getcS());
        Log.e("dia",historial.getD()+"");
        customViewHolder.txtFecha.setText(getFecha(historial.getD()) + " " + historial.getH());

    }

    @Override
    public int getItemCount() {
        return (null != historialList ? historialList.size() : 0);
    }


    class CustomViewHolder extends RecyclerView.ViewHolder {
        private TextView txtBarrio, txtCalle, txtFecha, txtDetalle;


        CustomViewHolder(View view) {
            super(view);
            this.txtBarrio = view.findViewById(R.id.txt_barrio);
            this.txtCalle = view.findViewById(R.id.txt_calle);
            this.txtFecha = view.findViewById(R.id.txt_fecha);
            this.txtDetalle = view.findViewById(R.id.txt_detalle);

            txtDetalle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClicklistener.onClic(historialList.get(getAdapterPosition()), getAdapterPosition());
                }
            });
        }
    }


    public String getFecha(int d) {
        String mesString = mes < 10 ? "0" + mes : mes + "";
        String diaString = d < 10 ? "0" + d : d + "";
        return anio + "-" + mesString + "-" + diaString;
    }


}



