package com.kradac.lojagasdistribuidor.Adaptador;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kradac.lojagasdistribuidor.Modelo.Pedido;
import com.kradac.lojagasdistribuidor.R;

import java.util.List;

public class AdaptadorListaCaracteristicas extends RecyclerView.Adapter<AdaptadorListaCaracteristicas.CustomViewHolder> {
    List<Pedido> pedidoList;
    public Context context;
    private OnClicklistener onClicklistener;

    public interface OnClicklistener {
        void onClic(int posicion);
    }

    public AdaptadorListaCaracteristicas(Context context, List<Pedido> pedidoList, OnClicklistener onClicklistener) {
        this.pedidoList = pedidoList;
        this.context = context;
        this.onClicklistener = onClicklistener;
    }

    @Override
    public AdaptadorListaCaracteristicas.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pedido, null);
        return new AdaptadorListaCaracteristicas.CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdaptadorListaCaracteristicas.CustomViewHolder customViewHolder, int i) {
        final Pedido pedido = pedidoList.get(i);
        customViewHolder.txtPedido.setText("* " + pedido.getCaracteristica().toUpperCase());

    }

    @Override
    public int getItemCount() {
        return (null != pedidoList ? pedidoList.size() : 0);
    }


    class CustomViewHolder extends RecyclerView.ViewHolder {
        private TextView txtPedido;


        CustomViewHolder(View view) {
            super(view);
            this.txtPedido = view.findViewById(R.id.txt_pedido);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClicklistener.onClic(getAdapterPosition());
                }
            });
        }
    }

}



