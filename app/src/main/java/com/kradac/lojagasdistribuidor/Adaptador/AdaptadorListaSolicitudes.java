package com.kradac.lojagasdistribuidor.Adaptador;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kradac.lojagasdistribuidor.Clase.Preferencia;
import com.kradac.lojagasdistribuidor.Modelo.Solicitud;
import com.kradac.lojagasdistribuidor.R;
import com.kradac.lojagasdistribuidor.Util.Funciones;

import java.util.List;

public class AdaptadorListaSolicitudes extends RecyclerView.Adapter<AdaptadorListaSolicitudes.CustomViewHolder> {
    List<Solicitud> solicitudList;
    public Context context;
    private OnClicklistener onClicklistener;
    private OnClicklistenerTiempo1 onClicklistenerTiempo1;
    private OnClicklistenerTiempo2 onClicklistenerTiempo2;
    private OnClicklistenerTiempo3 onClicklistenerTiempo3;
    private OnClicklistenerTiempo4 onClicklistenerTiempo4;
    private OnClicklistenerAumentarTiempo onClicklistenerAumentarTiempo;
    private Funciones funciones;
    private Preferencia preferencia;
    private AdaptadorListaCaracteristicas adaptadorListaCaracteristicas;

    public interface OnClicklistener {
        void onClic(Solicitud solicitud, int posicion);
    }

    public interface OnClicklistenerTiempo1 {
        void onClicTiempo1(Solicitud solicitud, int posicion);
    }

    public interface OnClicklistenerTiempo2 {
        void onClicTiempo2(Solicitud solicitud, int posicion);
    }


    public interface OnClicklistenerTiempo3 {
        void onClicTiempo3(Solicitud solicitud, int posicion);
    }

    public interface OnClicklistenerTiempo4 {
        void onClicTiempo4(Solicitud solicitud, int posicion);
    }

    public interface OnClicklistenerAumentarTiempo {
        void onClicAumentarTiempo(Solicitud solicitud, int posicion);
    }


    public AdaptadorListaSolicitudes(Context context, List<Solicitud> listModelServiceStores,
                                     OnClicklistener onClicklistener, OnClicklistenerTiempo1 onClicklistenerTiempo1,
                                     OnClicklistenerTiempo2 onClicklistenerTiempo2, OnClicklistenerTiempo3 onClicklistenerTiempo3,
                                     OnClicklistenerTiempo4 onClicklistenerTiempo4, OnClicklistenerAumentarTiempo onClicklistenerAumentarTiempo) {
        this.solicitudList = listModelServiceStores;
        this.context = context;
        this.onClicklistener = onClicklistener;
        this.onClicklistenerTiempo1 = onClicklistenerTiempo1;
        this.onClicklistenerTiempo2 = onClicklistenerTiempo2;
        this.onClicklistenerTiempo3 = onClicklistenerTiempo3;
        this.onClicklistenerTiempo4 = onClicklistenerTiempo4;
        this.onClicklistenerAumentarTiempo = onClicklistenerAumentarTiempo;
        this.funciones = new Funciones();
        this.preferencia = new Preferencia();

    }

    @Override
    public AdaptadorListaSolicitudes.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_solicitud, null);
        return new AdaptadorListaSolicitudes.CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdaptadorListaSolicitudes.CustomViewHolder customViewHolder, int i) {
        final Solicitud solicitud = solicitudList.get(i);
        customViewHolder.txtBarrio.setText(solicitud.getDatosSolicitud().getBarrioCliente());
        customViewHolder.txtCallePrincipal.setText(solicitud.getDatosSolicitud().getCallePrincipal());

        if (solicitud.getDatosSolicitud().getCalleSecundaria() == null
                || solicitud.getDatosSolicitud().getCalleSecundaria().isEmpty()) {
            customViewHolder.contCalleSecundaria.setVisibility(View.GONE);
        } else {
            customViewHolder.contCalleSecundaria.setVisibility(View.GONE);
            customViewHolder.txtCalleSecundaria.setText(solicitud.getDatosSolicitud().getCalleSecundaria());
        }

        if (solicitud.getIdSolicitud() != 0) {
            customViewHolder.txtTiempo1.setVisibility(View.VISIBLE);
            customViewHolder.txtTiempo2.setVisibility(View.VISIBLE);
            customViewHolder.txtTiempo3.setVisibility(View.VISIBLE);
            customViewHolder.txtTiempo4.setVisibility(View.VISIBLE);

            customViewHolder.txtTiempo1.setText(solicitud.getDatosSolicitud().getTiempos().get(0) + " minutos");
            customViewHolder.txtTiempo2.setText(solicitud.getDatosSolicitud().getTiempos().get(1) + " minutos");
            customViewHolder.txtTiempo3.setText(solicitud.getDatosSolicitud().getTiempos().get(2) + " minutos");
            customViewHolder.txtTiempo4.setText(solicitud.getDatosSolicitud().getTiempos().get(3) + " minutos");
        } else {
            customViewHolder.txtTiempo1.setVisibility(View.GONE);
            customViewHolder.txtTiempo2.setVisibility(View.GONE);
            customViewHolder.txtTiempo3.setVisibility(View.GONE);
            customViewHolder.txtTiempo4.setVisibility(View.GONE);
        }


        if (solicitud.getEstadoSolicitud() == 1) {
            customViewHolder.txtBarrio.setBackgroundResource(R.color.colorPrimary);
        }

        double distancia = solicitud.getDatosSolicitud().getDistancia() * 1000;

        if (distancia > 1000) {
            customViewHolder.txtDistancia.setText("DISTANCIA: " + (int) solicitud.getDatosSolicitud().getDistancia() + " Km");
        } else {
            customViewHolder.txtDistancia.setText("DISTANCIA: " + (int) distancia + " metros");
        }

        if (solicitud.getlC() != null) {
            if (solicitud.getlC().size() > 0) {
                customViewHolder.contCaracteristicas.setVisibility(View.VISIBLE);
                LinearLayoutManager layoutManagerNueva = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                customViewHolder.recyclerCaracteristicas.setNestedScrollingEnabled(false);
                customViewHolder.recyclerCaracteristicas.setLayoutManager(layoutManagerNueva);

                adaptadorListaCaracteristicas = new AdaptadorListaCaracteristicas(context, solicitud.getlC(), new AdaptadorListaCaracteristicas.OnClicklistener() {
                    @Override
                    public void onClic(int posicion) {

                    }
                });

                customViewHolder.recyclerCaracteristicas.setAdapter(adaptadorListaCaracteristicas);
            } else {
                customViewHolder.contCaracteristicas.setVisibility(View.GONE);
            }

        } else {
            customViewHolder.contCaracteristicas.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return (null != solicitudList ? solicitudList.size() : 0);
    }


    class CustomViewHolder extends RecyclerView.ViewHolder {
        private TextView txtBarrio, txtCallePrincipal,
                txtCalleSecundaria, txtTiempo1, txtTiempo2, txtTiempo3,
                txtTiempo4, txtDistancia;
        private Button btn_rechazar, btnTiempo;
        private LinearLayout contCalleSecundaria, contCaracteristicas;
        private RecyclerView recyclerCaracteristicas;


        CustomViewHolder(View view) {
            super(view);
            this.txtBarrio = view.findViewById(R.id.txt_barrio);
            this.txtCallePrincipal = view.findViewById(R.id.txt_calle_principal);
            this.txtCalleSecundaria = view.findViewById(R.id.txt_calle_secundaria);
            this.txtTiempo1 = view.findViewById(R.id.txt_tiempo1);
            this.txtTiempo2 = view.findViewById(R.id.txt_tiempo2);
            this.txtTiempo3 = view.findViewById(R.id.txt_tiempo3);
            this.txtTiempo4 = view.findViewById(R.id.txt_tiempo4);
            this.btn_rechazar = view.findViewById(R.id.btn_rechazar);
            this.contCalleSecundaria = view.findViewById(R.id.cont_calle_secundaria);
            this.contCaracteristicas = view.findViewById(R.id.cont_caracteristicas);
            this.recyclerCaracteristicas = view.findViewById(R.id.recycler_pedido);
            this.btnTiempo = view.findViewById(R.id.btn_tiempo);
            this.txtDistancia = view.findViewById(R.id.txt_distancia);


            btn_rechazar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClicklistener.onClic(solicitudList.get(getAdapterPosition()), getAdapterPosition());
                }
            });

            txtTiempo1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClicklistenerTiempo1.onClicTiempo1(solicitudList.get(getAdapterPosition()), getAdapterPosition());
                }
            });

            txtTiempo2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClicklistenerTiempo2.onClicTiempo2(solicitudList.get(getAdapterPosition()), getAdapterPosition());
                }
            });

            txtTiempo3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClicklistenerTiempo3.onClicTiempo3(solicitudList.get(getAdapterPosition()), getAdapterPosition());
                }
            });

            txtTiempo4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClicklistenerTiempo4.onClicTiempo4(solicitudList.get(getAdapterPosition()), getAdapterPosition());
                }
            });

            btnTiempo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClicklistenerAumentarTiempo.onClicAumentarTiempo(solicitudList.get(getAdapterPosition()), getAdapterPosition());
                }
            });
        }

    }


    public void actualizarSolicitudCancelada(int idSolicitud) {
        for (int i = 0; i < solicitudList.size(); i++) {
            if (solicitudList.get(i).getIdSolicitud() == idSolicitud) {
                solicitudList.remove(i);
                notifyItemRemoved(i);
            }
        }
    }

    public void borrarSolicitud(int idSolicitud, int posicion) {
        for (int i = 0; i < solicitudList.size(); i++) {
            if (solicitudList.get(i).getIdSolicitud() == idSolicitud) {
                solicitudList.remove(i);
                notifyItemRemoved(i);
            }
        }

    }

    public void borrarTodasLasSolicitudes() {
        for (int i = 0; i < solicitudList.size(); i++) {
            if (solicitudList.get(i).getEstadoSolicitud() == 1) {
                solicitudList.remove(i);
                notifyItemRemoved(i);
            }
        }
    }


    public void actualizarSolicitudAceptada(int idSolicitud) {
        for (int i = 0; i < solicitudList.size(); i++) {
            if (solicitudList.get(i).getIdSolicitud() == idSolicitud) {
                solicitudList.remove(i);
                notifyItemRemoved(i);
            }
        }

    }

}



