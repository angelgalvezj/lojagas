package com.kradac.lojagasdistribuidor.Adaptador;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kradac.lojagasdistribuidor.Modelo.Solicitud;
import com.kradac.lojagasdistribuidor.R;

import java.util.List;

public class AdaptadorListaSolicitudesPendientes extends RecyclerView.Adapter<AdaptadorListaSolicitudesPendientes.CustomViewHolder> {
    List<Solicitud> solicitudList;
    public Context context;
    private OnClicklistener onClicklistener;
    private AdaptadorListaCaracteristicas adaptadorListaCaracteristicas;

    public interface OnClicklistener {
        void onClic(int idSolicitud, int posicion);
    }

    public AdaptadorListaSolicitudesPendientes(Context context, List<Solicitud> solicitudList, OnClicklistener onClicklistener) {
        this.solicitudList = solicitudList;
        this.context = context;
        this.onClicklistener = onClicklistener;
    }

    @Override
    public AdaptadorListaSolicitudesPendientes.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_pendiente, null);
        return new AdaptadorListaSolicitudesPendientes.CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdaptadorListaSolicitudesPendientes.CustomViewHolder customViewHolder, int i) {
        final Solicitud solicitud = solicitudList.get(i);
        customViewHolder.txtBarrio.setText(solicitud.getDatosSolicitud().getBarrioCliente());
        customViewHolder.txtCallePrincipal.setText(solicitud.getDatosSolicitud().getCallePrincipal());
        customViewHolder.txtCalleSecundaria.setText(solicitud.getDatosSolicitud().getCalleSecundaria());

        if (solicitud.getDatosSolicitud().getReferenciaCliente() != null && !solicitud.getDatosSolicitud().getReferenciaCliente().isEmpty()) {
            customViewHolder.contReferencia.setVisibility(View.VISIBLE);
            customViewHolder.txtReferencia.setText(solicitud.getDatosSolicitud().getReferenciaCliente());
        } else {
            customViewHolder.contReferencia.setVisibility(View.GONE);
        }

        if (solicitud.getDatosSolicitud().getCalleSecundaria()==null || solicitud.getDatosSolicitud().getCalleSecundaria().isEmpty()) {
            customViewHolder.contCalleSecundaria.setVisibility(View.GONE);
        } else {
            customViewHolder.contCalleSecundaria.setVisibility(View.VISIBLE);
            customViewHolder.txtCalleSecundaria.setText(solicitud.getDatosSolicitud().getCalleSecundaria());
        }

        customViewHolder.txtBarrio.setBackgroundResource(R.color.colorverde);

        if (solicitud.getlC() != null) {
            if (solicitud.getlC().size() > 0) {
                customViewHolder.contCaracteristicas.setVisibility(View.VISIBLE);
                LinearLayoutManager layoutManagerNueva = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                customViewHolder.recyclerCaracteristicas.setNestedScrollingEnabled(false);
                customViewHolder.recyclerCaracteristicas.setLayoutManager(layoutManagerNueva);

                adaptadorListaCaracteristicas = new AdaptadorListaCaracteristicas(context, solicitud.getlC(), new AdaptadorListaCaracteristicas.OnClicklistener() {
                    @Override
                    public void onClic(int posicion) {

                    }
                });

                customViewHolder.recyclerCaracteristicas.setAdapter(adaptadorListaCaracteristicas);
            } else {
                customViewHolder.contCaracteristicas.setVisibility(View.GONE);
            }

        } else {
            customViewHolder.contCaracteristicas.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return (null != solicitudList ? solicitudList.size() : 0);
    }


    class CustomViewHolder extends RecyclerView.ViewHolder {
        private TextView txtBarrio, txtCallePrincipal,
                txtCalleSecundaria,txtReferencia;
        private LinearLayout contCalleSecundaria,contCaracteristicas,contReferencia;
        private Button btnEntregar;
        private RecyclerView recyclerCaracteristicas;


        CustomViewHolder(View view) {
            super(view);
            this.txtBarrio = view.findViewById(R.id.txt_barrio);
            this.txtCallePrincipal = view.findViewById(R.id.txt_calle_principal);
            this.txtCalleSecundaria = view.findViewById(R.id.txt_calle_secundaria);
            this.contCalleSecundaria = view.findViewById(R.id.cont_calle_secundaria);
            this.btnEntregar = view.findViewById(R.id.btn_entregar);
            this.contCaracteristicas = view.findViewById(R.id.cont_caracteristicas);
            this.recyclerCaracteristicas = view.findViewById(R.id.recycler_pedido);
            this.contReferencia = view.findViewById(R.id.cont_referencia);
            this.txtReferencia = view.findViewById(R.id.txt_referencia);

            btnEntregar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClicklistener.onClic(solicitudList.get(getAdapterPosition()).getIdSolicitud(), getAdapterPosition());
                }
            });
        }
    }

    public void actualizarSolicitudCancelada(int idSolicitud) {
        for (int i = 0; i < solicitudList.size(); i++) {
            if (solicitudList.get(i).getIdSolicitud() == idSolicitud) {
                solicitudList.remove(i);
                notifyItemRemoved(i);
            }
        }
    }

}



