package com.kradac.lojagasdistribuidor.Adaptador;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kradac.lojagasdistribuidor.Modelo.Pedido;
import com.kradac.lojagasdistribuidor.Modelo.Usuario;
import com.kradac.lojagasdistribuidor.R;

import java.util.List;

public class AdaptadorListaVehiculo extends RecyclerView.Adapter<AdaptadorListaVehiculo.CustomViewHolder> {
    List<Usuario> usuarioList;
    public Context context;
    private OnClicklistener onClicklistener;

    public interface OnClicklistener {
        void onClic(Usuario usuario, int posicion);
    }

    public AdaptadorListaVehiculo(Context context, List<Usuario> usuarioList, OnClicklistener onClicklistener) {
        this.usuarioList = usuarioList;
        this.context = context;
        this.onClicklistener = onClicklistener;
    }

    @Override
    public AdaptadorListaVehiculo.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_lista_vehiculo, null);
        return new AdaptadorListaVehiculo.CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdaptadorListaVehiculo.CustomViewHolder customViewHolder, int i) {
        final Usuario usuario = usuarioList.get(i);
        customViewHolder.txtEmpresa.setText(usuario.getEmpresa());
        customViewHolder.txtPlaca.setText(usuario.getPlacaVehiculo());

    }

    @Override
    public int getItemCount() {
        return (null != usuarioList ? usuarioList.size() : 0);
    }


    class CustomViewHolder extends RecyclerView.ViewHolder {
        private TextView txtEmpresa, txtPlaca;


        CustomViewHolder(View view) {
            super(view);
            this.txtEmpresa = view.findViewById(R.id.txt_empresa);
            this.txtPlaca = view.findViewById(R.id.txt_placa);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClicklistener.onClic(usuarioList.get(getAdapterPosition()), getAdapterPosition());
                }
            });
        }
    }

}



