package com.kradac.lojagasdistribuidor.Adaptador;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.kradac.lojagasdistribuidor.Modelo.Mensaje;
import com.kradac.lojagasdistribuidor.R;
import java.util.ArrayList;


public class ListAdapterMensajes extends ArrayAdapter<Mensaje> {

    private ArrayList<Mensaje> objects;

    public ListAdapterMensajes(Context context, int resource, ArrayList<Mensaje> objects) {
        super(context, resource, objects);
        this.objects = objects;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            view = vi.inflate(R.layout.item_mensaje_predeterminado, null);
        }
        TextView title = (TextView) view.findViewById(R.id.tv_mensaje);
        title.setText(objects.get(position).getMensaje());
        return view;
    }
}
