package com.kradac.lojagasdistribuidor.Adaptador;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.kradac.lojagasdistribuidor.Clase.AudioCallCenter;
import com.kradac.lojagasdistribuidor.Clase.AudioEmpresa;
import com.kradac.lojagasdistribuidor.Clase.AudioTodos;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    private AudioCallCenter tab1;
    private AudioEmpresa tab2;
    private AudioTodos tab3;
    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }



    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                tab1 = new AudioCallCenter();
                return tab1;
            case 1:
                tab2 = new AudioEmpresa();
                return tab2;
            case 2:
                tab3 = new AudioTodos();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    public AudioCallCenter getTab1() {
        return tab1;
    }

    public AudioEmpresa getTab2() {
        return tab2;
    }

    public AudioTodos getTab3() {
        return tab3;
    }
}