package com.kradac.lojagasdistribuidor.Api;

import com.kradac.lojagasdistribuidor.Modelo.Historial;
import com.kradac.lojagasdistribuidor.Response.ResponseApi;
import com.kradac.lojagasdistribuidor.Util.VariablesGlobales;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface LojagasApi {

    @FormUrlEncoded
    @POST("imei-conductor/registrar")
    Call<ResponseApi> validarImei(@Field("idUsuario") int idUsuario,
                                  @Field("idDispositivo") String idDispositivo,
                                  @Field("marca") String marca,
                                  @Field("modelo") String modelo,
                                  @Field("versionSo") String versionSo,
                                  @Field("versionAplicativo") String versionAplicativo,
                                  @Field("ltGps") double latitud,
                                  @Field("lgGps") double longitud);

    @FormUrlEncoded
    @POST("ktaxi-driver/meta/")
    Call<ResponseApi> enviarMetaData(@Field("idPlataformaKtaxi") int idPlataformaKtaxi,
                                     @Field("versionKtaxi") String versionKtaxi,
                                     @Field("versionSo") String versionSo,
                                     @Field("marca") String marca,
                                     @Field("modelo") String modelo,
                                     @Field("idUsuario") int idUsuario);


    @FormUrlEncoded
    @POST("ktaxi-driver/cambiar-clave-driver/")
    Call<ResponseApi> cambioClave(@Field("idUsuario") int idUsuario,
                                  @Field("claveAnterior") String claveAnterior,
                                  @Field("claveNueva") String claveNueva,
                                  @Field("idAplicativo") int idAplicativo);


    @FormUrlEncoded
    @POST("/feedback/enviar-contactenos")
    Call<ResponseApi> enviarSugerencia(
            @Field("tipo") int tipo,
            @Field("idAplicativo") int idAplicativo,
            @Field("idCliente") int idCliente,
            @Field("idUsuario") int idUsuario,
            @Field("idClienteUsuario") int idClienteUsuario,
            @Field("idPlataformaKtaxi") int idPlataformaKtaxi,
            @Field("idSolicitud") int idSolicitud,
            @Field("persona") String persona,
            @Field("correo") String correo,
            @Field("telefono") String telefono,
            @Field("motivo") String motivo,
            @Field("mensaje") String mensaje,
            @Field("latitud") double latitud,
            @Field("longitud") double longitud,
            @Field("versionApp") String versionApp,
            @Field("versionSo") String versionSo,
            @Field("marca") String marca,
            @Field("modelo") String modelo,
            @Field("tipoRed") int tipoRed,
            @Field("usandoGps") int usandoGps,
            @Field("tipoConexion") int tipoConexion,
            @Field("operadora") String operadora,
            @Field("imei") String imei,
            @Field("APP") int APP,
            @Field("paisSeleccionado") String paisSeleccionado,
            @Field("idCiudad") int idCiudad);

    @FormUrlEncoded
    @POST("ktaxi-driver/recuperar-contrasenia-driver/")
    Call<ResponseApi> recuperarContrasenia(
            @Field("usuario") String usuario,
            @Field("idAplicativo") int idAplicativo);

    @GET("/ktaxi-driver/historial/anio-mes/{idUsuario}/{anio}/{mes}/{tipo}")
    Call<ResponseApi> historialSolicitud(
            @Path("idUsuario") int idUsuario,
            @Path("anio") int anio,
            @Path("mes") int mes,
            @Path("tipo") int tipo);

    @FormUrlEncoded
    @POST("ktaxi-driver/historial/anio-mes")
    Call<List<Historial>> obtenerHistorialAnioMes(
            @Field("idUsuario") int idUsuario,
            @Field("anio") int anio,
            @Field("mes") int mes,
            @Field("tipo") int tipo,
            @Field("desde") int desde,
            @Field("cuantos") int hasta
    );


}
