package com.kradac.lojagasdistribuidor.Clase;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kradac.lojagasdistribuidor.Adaptador.ChatAdapter;
import com.kradac.lojagasdistribuidor.Adaptador.ListAdapterMensajes;
import com.kradac.lojagasdistribuidor.Modelo.ChatMessage;
import com.kradac.lojagasdistribuidor.Modelo.Mensaje;
import com.kradac.lojagasdistribuidor.R;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionChat;
import com.kradac.lojagasdistribuidor.Servicio.Servicio;
import com.kradac.lojagasdistribuidor.Util.ConnectivityReceiver;
import com.kradac.lojagasdistribuidor.Util.Funciones;
import com.kradac.lojagasdistribuidor.Util.Gps;
import com.kradac.lojagasdistribuidor.Util.LojagasAplicacion;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Chat extends Funciones implements ConnectivityReceiver.ConnectivityReceiverListener,
        OnComunicacionChat {

    public static final String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
    @BindView(R.id.txt_version)
    TextView txtVersion;
    @BindView(R.id.main_content)
    CoordinatorLayout mainContent;
    @BindView(R.id.messagesContainer)
    ListView messagesContainer;
    @BindView(R.id.messageEdit)
    EditText messageEdit;
    @BindView(R.id.chatSendButton)
    ImageButton chatSendButton;
    @BindView(R.id.btn_audio)
    ImageButton btnAudio;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private Servicio conexionServer;
    private boolean mBound, verf;
    private int idSolicitud, userName;
    private MediaRecorder mediaRecorder;
    private boolean isPlay = false;
    private boolean isAudio = true;
    private String nameAudio;
    private static final int MY_PERMISSIONS_REQUEST_CODE = 100;
    private ChatAdapter adapter;
    private AlertDialog alertDialog;

    private MediaPlayer mediaPlayer;

    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Servicio.LocalBinder binder = (Servicio.LocalBinder) service;
            conexionServer = binder.getService();
            conexionServer.registrarUsuario(Chat.this, "ChatActivity");
            mBound = true;

            conexionServer.obtenerMensaje();

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };

    @SuppressLint("InvalidWakeLockTag")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        Intent intent = new Intent(this, Servicio.class);
        startService(intent);

        final Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);

        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        this.wakelock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "INFO");
        wakelock.acquire();


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        idSolicitud = bundle.getInt("idSolicitud");
        userName = bundle.getInt("userName");

        checkPermission();

        txtVersion.setText("V. " + obtenerVersion(getApplicationContext()));


        chatSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (messageEdit.getText().toString().trim().isEmpty()) {
                    Toast.makeText(Chat.this, "Ingrese el mensaje a enviar", Toast.LENGTH_SHORT).show();
                    return;
                }

                conexionServer.enviarChat(idSolicitud, userName, messageEdit.getText().toString().trim());

            }
        });

        btnAudio.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_UP:
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                if (mediaRecorder != null) {
                                    if (isPlay) {
                                        try {
                                            Thread.sleep(1000);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        mediaPlayer = MediaPlayer.create(Chat.this, R.raw.grabar);
                                        mediaPlayer.start();
                                        mediaRecorder.stop();
                                    }
                                    conexionServer.enviarAudio(idSolicitud, userName, nameAudio);
                                    isPlay = false;
                                    mediaRecorder = null;
                                }
                            }
                        }).start();


                        break;
                }
                return false;
            }
        });


        btnAudio.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (ContextCompat.checkSelfPermission(Chat.this, Manifest.permission.RECORD_AUDIO)
                        + ContextCompat.checkSelfPermission(
                        Chat.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) +
                        +ContextCompat.checkSelfPermission(
                                Chat.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Chat.this, new String[]{
                            Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_CODE);
                } else {
                    nameAudio = conexionServer.nameAudio();
                    recordAudio(path + nameAudio);
                }


                return false;
            }
        });

        initControls();

        if (!verf) {
            if (mConnection != null) {
                Intent intent2 = new Intent(this, Servicio.class);
                bindService(intent2, mConnection, Context.BIND_AUTO_CREATE);

            }
        }
    }

    public void recordAudio(String fileName) {

        try {
            mediaRecorder = new MediaRecorder();
            ContentValues values = new ContentValues(3);
            values.put(MediaStore.MediaColumns.TITLE, fileName);
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
            mediaRecorder.setAudioSamplingRate(16000);
            mediaRecorder.setAudioEncodingBitRate(256);
            mediaRecorder.setOutputFile(fileName);
            mediaRecorder.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            mediaRecorder.start();
            isPlay = true;
        } catch (IllegalStateException ignored) {
        }
    }

    private void initControls() {
        adapter = new ChatAdapter(Chat.this, new ArrayList<ChatMessage>());
        messagesContainer.setAdapter(adapter);
        chatSendButton.setVisibility(View.VISIBLE);
        chatSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageText = messageEdit.getText().toString();
                if (TextUtils.isEmpty(messageText)) {
                    return;
                }
                if (messageEdit.getText().toString().trim().equals("")) {
                    return;
                }
                conexionServer.enviarChat(idSolicitud, userName, messageEdit.getText().toString().trim());
                messageEdit.setText("");

            }
        });

        messageEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (isAudio) {
                    if (!messageEdit.getText().toString().isEmpty()) {
                        chatSendButton.setVisibility(View.VISIBLE);
                        btnAudio.setVisibility(View.GONE);
                    } else {
                        chatSendButton.setVisibility(View.VISIBLE);
                        btnAudio.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    private void scroll() {
        messagesContainer.setSelection(messagesContainer.getCount() - 1);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_chat) {
            alertDialog = dialogMensajes();
            alertDialog.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Conexion server


    public void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (!isConnected) {
            message = "Se ha perdido la conexión";
            color = Color.GREEN;
            Snackbar snackbar = Snackbar.make(mainContent, message, Snackbar.LENGTH_INDEFINITE);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();

        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (!isConnected) {
            String message = "Se ha perdido la conexión";
            int color = Color.GREEN;
            Snackbar snackbar = Snackbar.make(mainContent, message, Snackbar.LENGTH_INDEFINITE);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();

        } else {
            String message = "Se ha establecido la conexión";
            int color = Color.WHITE;
            Snackbar snackbar = Snackbar.make(mainContent, message, Snackbar.LENGTH_LONG);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();

            Intent intent = new Intent(getApplicationContext(), Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }

    }


    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            Intent intentbind = new Intent(Chat.this, Servicio.class);
            bindService(intentbind, mConnection, Context.BIND_AUTO_CREATE);
        }
    };

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {
        super.onResume();
        verf = false;
        LojagasAplicacion.getInstance().setConnectivityListener(this);
    }

    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        this.wakelock.release();
    }


    @Override
    public void onPause() {
        super.onPause();
        verf = true;
        wakelock.acquire();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.wakelock.release();

    }

    @Override
    public void conectadoServ() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    @Override
    public void mensajesUsuario(final ArrayList<ChatMessage> chatHistory) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.removeAll();
                adapter.notifyDataSetChanged();
                adapter.add(chatHistory);
                adapter.notifyDataSetChanged();
                scroll();
                cerrarTeclado(messageEdit);
            }
        });
    }


    public AlertDialog dialogMensajes() {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(true);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.item_lista_mensaje, null);
        ListView listView = (ListView) v.findViewById(R.id.listView);
        final ArrayList<Mensaje> itemTiemposList = new ArrayList<>();
        itemTiemposList.add(new Mensaje("Estoy en camino"));
        itemTiemposList.add(new Mensaje("Me encuentro en la ubicación seleccionada"));
        itemTiemposList.add(new Mensaje("Espere por favor"));
        itemTiemposList.add(new Mensaje("Solicito un poco más de tiempo de espera"));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mBound) {
                    conexionServer.enviarChat(idSolicitud, userName, itemTiemposList.get(position).getMensaje());
                    alertDialog.dismiss();
                }
            }
        });
        ListAdapterMensajes miAdaptador = new ListAdapterMensajes(this, R.layout.item_lista_mensaje, itemTiemposList);
        listView.setAdapter(miAdaptador);


        localBuilder.setView(v);
        return localBuilder.create();
    }


    protected void checkPermission() {
        if (ContextCompat.checkSelfPermission(
                Chat.this, Manifest.permission.RECORD_AUDIO) +
                ContextCompat.checkSelfPermission(
                        Chat.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) +
                ContextCompat.checkSelfPermission(
                        Chat.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Do something, when permissions not granted
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    Chat.this, Manifest.permission.RECORD_AUDIO)
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                    Chat.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(
                            Chat.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an alert dialog here with request explanation
                AlertDialog.Builder builder = new AlertDialog.Builder(Chat.this);
                builder.setCancelable(false);
                builder.setMessage("Es un requisito necesario para el envio de audios de la aplicación");
                builder.setTitle("Por favor, Conceda los permisos");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ActivityCompat.requestPermissions(
                                Chat.this,
                                new String[]{
                                        Manifest.permission.RECORD_AUDIO,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                        Manifest.permission.READ_EXTERNAL_STORAGE
                                },
                                MY_PERMISSIONS_REQUEST_CODE
                        );
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                ActivityCompat.requestPermissions(
                        Chat.this,
                        new String[]{
                                Manifest.permission.RECORD_AUDIO,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        },
                        MY_PERMISSIONS_REQUEST_CODE
                );
            }
        } else {
            checkConnection();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CODE: {
                if (((grantResults.length > 0) && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                        && ((grantResults.length > 0) && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                        && ((grantResults.length > 0) && grantResults[2] == PackageManager.PERMISSION_GRANTED)) {
                    checkConnection();
                } else {
                    checkPermission();
                }

            }

            break;
        }
    }

}
