package com.kradac.lojagasdistribuidor.Clase;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.kradac.lojagasdistribuidor.Adaptador.PagerAdapter;
import com.kradac.lojagasdistribuidor.Modelo.ChatMessage;
import com.kradac.lojagasdistribuidor.R;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionChatBrodcast;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionMensajesBroadCast;
import com.kradac.lojagasdistribuidor.Response.OnHandleEnviarAudio;
import com.kradac.lojagasdistribuidor.Servicio.Servicio;
import com.kradac.lojagasdistribuidor.Util.ConnectivityReceiver;
import com.kradac.lojagasdistribuidor.Util.Funciones;
import com.kradac.lojagasdistribuidor.Util.LojagasAplicacion;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatBroadCast extends Funciones implements ConnectivityReceiver.ConnectivityReceiverListener,
        OnComunicacionChatBrodcast, OnComunicacionMensajesBroadCast, OnHandleEnviarAudio {

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.txt_version)
    TextView txtVersion;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.main_content)
    CoordinatorLayout mainContent;

    private Servicio conexionServer;
    private boolean mBound, verf;

    public ArrayList<ChatMessage> chatCallCenter,chatBroadCastEmpresa,chatBroadCast;
    private PagerAdapter adapter;


    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Servicio.LocalBinder binder = (Servicio.LocalBinder) service;
            conexionServer = binder.getService();
            conexionServer.registrarUsuario(ChatBroadCast.this, "ChatBrodcast");
            mBound = true;
            conexionServer.obtenerMensajesCallCenter();
            conexionServer.obtenerMensajesBroadCastEmpresa();
            conexionServer.obtenerMensajesBroadCast();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };

    @SuppressLint("InvalidWakeLockTag")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_brodcast);
        ButterKnife.bind(this);

        Intent intent = new Intent(this, Servicio.class);
        startService(intent);

        final Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);

        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        this.wakelock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "INFO");
        wakelock.acquire();



        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtVersion.setText("V. " + obtenerVersion(getApplicationContext()));

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("CALL CENTER"));
        tabLayout.addTab(tabLayout.newTab().setText("EMPRESA"));
        //tabLayout.addTab(tabLayout.newTab().setText("TODOS"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        adapter = new com.kradac.lojagasdistribuidor.Adaptador.PagerAdapter
                (getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        if (!verf) {
            if (mConnection != null) {
                Intent intent2 = new Intent(this, Servicio.class);
                bindService(intent2, mConnection, Context.BIND_AUTO_CREATE);

            }
        }

    }

    @Override
    public void onGrabarEnviarAudio(String nameAudio,int fragment) {
        switch (fragment){
            case 1:
                conexionServer.enviarAudioCallCenter(nameAudio);
                break;
            case 2:
                conexionServer.enviarAudioBroasCastEmpresa(nameAudio);
                break;
            case 3:
                conexionServer.enviarAudioBroasCast(nameAudio);
                break;
            default:
                break;
        }
    }

    @Override
    public Servicio onServicioSocket() {
        return conexionServer;
    }

    @Override
    public void mensajesCallCenter(ArrayList<ChatMessage> chatHistory) {
        chatCallCenter = chatHistory;
        if (adapter.getTab1()!=null){
            adapter.getTab1().mensajesUsuarioCallCenter();
        }

    }

    @Override
    public void mensajesBroadCastEmpresa(ArrayList<ChatMessage> chatHistory) {
        chatBroadCastEmpresa = chatHistory;
        if (adapter.getTab2()!=null){
            adapter.getTab2().mensajesUsuarioEmpresa();
        }

    }

    @Override
    public void mensajesBroadCast(ArrayList<ChatMessage> chatHistory) {
        chatBroadCast = chatHistory;
        if (adapter.getTab3()!=null){
            adapter.getTab3().mensajesUsuarioTodos();
        }

    }

    @Override
    public ArrayList<ChatMessage> getChatCallCenter() {
        return chatCallCenter;
    }
    @Override
    public ArrayList<ChatMessage> getChatBroadCastEmpresa() {
        return chatBroadCastEmpresa;
    }
    @Override
    public ArrayList<ChatMessage> getChatBroadCast() {
        return chatBroadCast;
    }


    //Conexion server

    public void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (!isConnected) {
            message = "Se ha perdido la conexión";
            color = Color.GREEN;
            Snackbar snackbar = Snackbar.make(mainContent, message, Snackbar.LENGTH_INDEFINITE);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();

        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (!isConnected) {
            String message = "Se ha perdido la conexión";
            int color = Color.GREEN;
            Snackbar snackbar = Snackbar.make(mainContent, message, Snackbar.LENGTH_INDEFINITE);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();

        } else {
            String message = "Se ha establecido la conexión";
            int color = Color.WHITE;
            Snackbar snackbar = Snackbar.make(mainContent, message, Snackbar.LENGTH_LONG);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();

            Intent intent = new Intent(getApplicationContext(), Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }

    }


    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            Intent intentbind = new Intent(ChatBroadCast.this, Servicio.class);
            bindService(intentbind, mConnection, Context.BIND_AUTO_CREATE);
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        verf = false;
        LojagasAplicacion.getInstance().setConnectivityListener(this);

    }

    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        this.wakelock.release();
    }


    @Override
    public void onPause() {
        super.onPause();
        verf = true;
        wakelock.acquire();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.wakelock.release();

    }

    @Override
    public void conectadoServ() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    // Fin conexion server
}