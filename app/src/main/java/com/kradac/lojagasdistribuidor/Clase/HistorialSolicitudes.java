package com.kradac.lojagasdistribuidor.Clase;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.kradac.lojagasdistribuidor.Adaptador.AdaptadorListHistorial;
import com.kradac.lojagasdistribuidor.Modelo.Historial;
import com.kradac.lojagasdistribuidor.Presenter.PresenterHistorialAnioMes;
import com.kradac.lojagasdistribuidor.Presenter.PresenterHistorialSolicitud;
import com.kradac.lojagasdistribuidor.R;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionHistorialAnioMes;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionHistorialSolicitud;
import com.kradac.lojagasdistribuidor.Response.ResponseApi;
import com.kradac.lojagasdistribuidor.Util.Funciones;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;

public class HistorialSolicitudes extends Funciones implements OnComunicacionHistorialSolicitud, OnComunicacionHistorialAnioMes {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_historial)
    RecyclerView recyclerHistorial;
    @BindView(R.id.txt_version)
    TextView txtVersion;
    @BindView(R.id.txt_numero_solicitudes)
    TextView txtNumeroSolicitudes;
    private HorizontalCalendar horizontalCalendar;
    private PresenterHistorialSolicitud presenterHistorialSolicitud;
    private PresenterHistorialAnioMes presenterHistorialAnioMes;
    private int mes, anio,mesActual;
    private SimpleDateFormat mesFormat, anioFormat;
    private Date date;
    private AdaptadorListHistorial adaptadorListHistorial;

    @SuppressLint("InvalidWakeLockTag")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial);
        ButterKnife.bind(this);

        final Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        this.wakelock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "INFO");
        wakelock.acquire();


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenterHistorialSolicitud = new PresenterHistorialSolicitud(this);
        presenterHistorialAnioMes = new PresenterHistorialAnioMes(this);

        txtVersion.setText("V. " + obtenerVersion(getApplicationContext()));

        LinearLayoutManager layoutManagerNueva = new LinearLayoutManager(getApplicationContext(),
                LinearLayoutManager.VERTICAL, false);
        recyclerHistorial.setNestedScrollingEnabled(false);
        recyclerHistorial.setLayoutManager(layoutManagerNueva);

        mesFormat = new SimpleDateFormat("MM", Locale.getDefault());
        anioFormat = new SimpleDateFormat("yyyy", Locale.getDefault());
        date = new Date();

        mes = Integer.parseInt(mesFormat.format(date));

        anio = Integer.parseInt(anioFormat.format(date));

        final Calendar defaultSelectedDate = Calendar.getInstance();

        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, 0);

        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, Integer.parseInt(mesFormat.format(date))-23);

        horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.calendarView)
                .range(endDate, startDate)
                .datesNumberOnScreen(5)
                .mode(HorizontalCalendar.Mode.MONTHS)
                .configure()
                .formatMiddleText("MMM")
                .formatBottomText("yyyy")
                .showBottomText(true)
                .colorTextMiddle(Color.LTGRAY, Color.parseColor("#ffd54f"))
                .end()
                .defaultSelectedDate(defaultSelectedDate)
                .build();


        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                String mesFormat = DateFormat.format("MM", date).toString();
                String anioFormat = DateFormat.format("yyyy", date).toString();

                mes = Integer.parseInt(mesFormat);
                anio = Integer.parseInt(anioFormat);

                presenterHistorialSolicitud.historialSolicitud(obtenerDatosUsuario(getApplicationContext()).getIdUsuario(),
                        anio, mes, 1);
            }

        });

        Log.e("mes",Integer.parseInt(mesFormat.format(date))+"");

        presenterHistorialSolicitud.historialSolicitud(obtenerDatosUsuario(getApplicationContext()).getIdUsuario(), anio,
                mes, 1);

    }

    @Override
    public void respuestaHistorial(ResponseApi responseApi) {
        if (responseApi != null) {
            if (responseApi.getRegistros() != 0) {
                recyclerHistorial.setVisibility(View.VISIBLE);

                presenterHistorialAnioMes.historialAnioMes(obtenerDatosUsuario(getApplicationContext()).getIdUsuario(),
                        anio, mes, 1, 0, responseApi.getRegistros());
            } else {
                recyclerHistorial.setVisibility(View.GONE);
                txtNumeroSolicitudes.setText("Total mes: 0");
                new AlertDialog.Builder(HistorialSolicitudes.this)
                        .setTitle("Aviso")
                        .setMessage("No hay datos que mostrar")
                        .setCancelable(false)
                        .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }

        }

    }

    @Override
    public void respuestaHistorialAnioMes(List<Historial> historialList) {
        if (historialList != null) {
            if (historialList.size() > 0) {
                txtNumeroSolicitudes.setText("Total mes: " + historialList.size());
                adaptadorListHistorial = new AdaptadorListHistorial(getApplicationContext(),
                        historialList, anio, Integer.parseInt(mesFormat.format(date)), new AdaptadorListHistorial.OnClicklistener() {
                    @Override
                    public void onClic(Historial historial, int posicion) {

                    }
                });
                recyclerHistorial.setAdapter(adaptadorListHistorial);
            }

        }
    }
}
