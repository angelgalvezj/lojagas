package com.kradac.lojagasdistribuidor.Clase;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kradac.lojagasdistribuidor.Adaptador.AdaptadorListaSolicitudes;
import com.kradac.lojagasdistribuidor.Adaptador.AdaptadorListaSolicitudesPendientes;
import com.kradac.lojagasdistribuidor.Modelo.Solicitud;
import com.kradac.lojagasdistribuidor.R;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionListaSolicitudes;
import com.kradac.lojagasdistribuidor.Servicio.Servicio;
import com.kradac.lojagasdistribuidor.Util.ConnectivityReceiver;
import com.kradac.lojagasdistribuidor.Util.Funciones;
import com.kradac.lojagasdistribuidor.Util.LojagasAplicacion;
import com.kradac.lojagasdistribuidor.Util.ReproducirTextAudio;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListaSolicitudes extends Funciones implements OnComunicacionListaSolicitudes,
        ConnectivityReceiver.ConnectivityReceiverListener {

    @BindView(R.id.txt_version)
    TextView txtVersion;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_solicitud)
    RecyclerView recyclerSolicitud;
    @BindView(R.id.recycler_solicitud_pendientes)
    RecyclerView recyclerSolicitudPendientes;
    @BindView(R.id.cont_solicitudes)
    LinearLayout contSolicitudes;
    @BindView(R.id.cont_solicitudes_pendientes)
    LinearLayout contSolicitudesPendientes;
    @BindView(R.id.txt_mensaje)
    TextView txtMensaje;
    @BindView(R.id.main_content)
    CoordinatorLayout mainContent;

    private AdaptadorListaSolicitudes adaptadorListaSolicitudes;
    private AdaptadorListaSolicitudesPendientes adaptadorListaSolicitudesPendientes;
    private Servicio conexionServer;
    private boolean mBound, verf;
    private AlertDialog dialogRezice;
    private Preferencia preferencia;
    private ReproducirTextAudio reproducirTextAudio;
    private String tiempo1;
    private boolean valortiempo1;
    private ProgressDialog pDialog;
    private CountDownTimer countDownTimer, countDownTimerBorrarSolicitud;
    private AlertDialog alertDialog;
    private List<Solicitud> solicitudNueva, solicitudPendiente;
    private int idSolicitudActiva, userNameActiva, tiempoSolicitud;

    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Servicio.LocalBinder binder = (Servicio.LocalBinder) service;
            conexionServer = binder.getService();
            conexionServer.registrarUsuario(ListaSolicitudes.this, "ListaSolicitudes");
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };

    @SuppressLint("InvalidWakeLockTag")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_solicitudes);
        ButterKnife.bind(this);

        Intent intent = new Intent(this, Servicio.class);
        startService(intent);

        final Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        this.wakelock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "INFO");
        wakelock.acquire();


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        preferencia = new Preferencia();
        reproducirTextAudio = new ReproducirTextAudio(this);


        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getBoolean("solicitudCentral")) {
                if (dialogRezice != null) {
                    dialogRezice.dismiss();
                }
                dialogRezice = dialogConfirmacionCallCenter("Se le asignado una solicitud desde la central");
                dialogRezice.show();
            }
        }

        txtVersion.setText("V. " + obtenerVersion(getApplicationContext()));

        LinearLayoutManager layoutManagerNueva = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerSolicitud.setNestedScrollingEnabled(false);
        recyclerSolicitud.setLayoutManager(layoutManagerNueva);

        LinearLayoutManager layoutManagerPendiente = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerSolicitudPendientes.setNestedScrollingEnabled(false);
        recyclerSolicitudPendientes.setLayoutManager(layoutManagerPendiente);

        cargarListasSolicitudes();

        if (!verf) {
            if (mConnection != null) {
                Intent intent2 = new Intent(this, Servicio.class);
                bindService(intent2, mConnection, Context.BIND_AUTO_CREATE);

            }
        }


    }

    @Override
    public void mensajeDeuda(final String mensaje, final int idSolicitud) {
        if (mensaje != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ocultarProgressDialog();
                    dialogRezice = dialogMensajeDeuda(mensaje, idSolicitud);
                    dialogRezice.show();
                }
            });
        }
    }

    @Override
    public void clienteCanceloPedido(final int idSolicitud) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                finish();

            }
        });
    }

    @Override
    public void cerrarSesion(final String nombre, final String mensaje) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        });

    }

    @Override
    public void noAceptoTiempo(final int idSolicitud) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ocultarProgressDialog();
                List<Solicitud> list = obtenerSolicitud(getApplicationContext());
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getIdSolicitud() == idSolicitud) {
                        list.get(i).setEstadoSolicitud(4);
                        guardarSolicitud(getApplicationContext(), list);
                        adaptadorListaSolicitudes.actualizarSolicitudCancelada(idSolicitud);
                        if (countDownTimer != null) {
                            countDownTimer.cancel();
                        }
                    }
                }
                cargarListasSolicitudes();
            }
        });

    }

    @Override
    public void borrarSolicitud(final int idSolicitud) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                List<Solicitud> list = obtenerSolicitud(getApplicationContext());
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getIdSolicitud() == idSolicitud && !list.get(i).isPostulacion()) {
                        list.get(i).setEstadoSolicitud(4);
                        guardarSolicitud(getApplicationContext(), list);
                        adaptadorListaSolicitudes.actualizarSolicitudCancelada(idSolicitud);
                        if (countDownTimer != null) {
                            countDownTimer.cancel();
                        }
                        break;
                    }

                }

                cargarListasSolicitudes();
            }
        });
    }

    @Override
    public void aceptoTiempo(final int idSolicitud) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ocultarProgressDialog();
                List<Solicitud> list = obtenerSolicitud(getApplicationContext());

                GregorianCalendar calendario = (GregorianCalendar) GregorianCalendar.getInstance();
                calendario.add(GregorianCalendar.MINUTE, tiempoSolicitud);

                if (list == null) return;

                for (int i = 0; i < list.size(); i++) {

                    if (list.get(i).getIdSolicitud() == idSolicitud && list.get(i).getEstadoSolicitud() == 1) {
                        if (obtenerEstadoActividadConductor(getApplicationContext()).isEstadoActividadConductor()) {

                            list.get(i).setEstadoSolicitud(3);
                            list.get(i).setTiempoAceptado(tiempoSolicitud);
                            list.get(i).setTiempoEntrega(new SimpleDateFormat("HH:mm").format(calendario.getTime()));
                            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

                            if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {

                                v.vibrate(2000);
                                reproducirTextAudio.speak("La solicitud se ha guardado en la lista de pedidos pendientes");
                            }
                            if (countDownTimer != null) {
                                countDownTimer.cancel();
                            }


                            int numeroSolicitudes = obtenerNumeroSolicitudesPendientes(getApplicationContext()).getNumeroSolicitudes() + 1;

                            preferencia.setNumeroSolicitudes(numeroSolicitudes);
                            guardarNumeroSolicitudesPendientes(preferencia, getApplicationContext());
                            tiempoSolicitud = 0;

                        } else {
                            list.get(i).setEstadoSolicitud(2);
                            list.get(i).setTiempoAceptado(tiempoSolicitud);
                            list.get(i).setTiempoEntrega(new SimpleDateFormat("HH:mm").format(calendario.getTime()));
                            preferencia.setEstadoActividadConductor(true);
                            guardarEstadoActividadConductor(preferencia, getApplicationContext());
                            if (countDownTimer != null) {
                                countDownTimer.cancel();
                            }
                            tiempoSolicitud = 0;
                        }
                    }
                }

                guardarSolicitud(getApplicationContext(), list);


                adaptadorListaSolicitudes.actualizarSolicitudAceptada(idSolicitud);
                finish();
            }
        });
    }

    @Override
    public void nuevaSolicitud() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                cargarListasSolicitudes();

            }
        });

    }

    @Override
    public void solicitudAsignadaCallCenter() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                        v.vibrate(2000);
                        if (reproducirTextAudio != null) {
                            reproducirTextAudio.speak("Se le ha asignado un pedido desde la central");

                        }

                    }
                    if (dialogRezice != null) {
                        dialogRezice.dismiss();
                    }
                    dialogRezice = dialogConfirmacionCallCenter("Se le asignado una solicitud desde la central");
                    dialogRezice.show();
                } catch (Exception e) {

                }

            }
        });
    }

    public AlertDialog dialogConfirmacionCallCenter(String mensaje) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_mensaje_confirmacion_central, null);
        Button btnAceptar = v.findViewById(R.id.btn_aceptar);
        TextView txtMensaje = v.findViewById(R.id.txt_mensaje);

        txtMensaje.setText(mensaje);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conexionServer.enviarValidacionCallCenter(3);
                if (obtenerEstadoActividadConductor(getApplicationContext()).isEstadoActividadConductor()) {
                    Vibrator v1 = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                        v1.vibrate(2000);
                        reproducirTextAudio.speak("La solicitud se ha guardado en la lista de pedidos pendientes");
                    }

                    int numeroSolicitudes = obtenerNumeroSolicitudesPendientes(getApplicationContext()).getNumeroSolicitudes() + 1;

                    preferencia.setNumeroSolicitudes(numeroSolicitudes);
                    guardarNumeroSolicitudesPendientes(preferencia, getApplicationContext());
                    tiempoSolicitud = 0;

                    dialogRezice.dismiss();
                    conexionServer.enviarValidacionCallCenter(1);
                    finish();
                } else {
                    preferencia.setEstadoActividadConductor(true);
                    guardarEstadoActividadConductor(preferencia, getApplicationContext());
                    dialogRezice.dismiss();
                    conexionServer.enviarValidacionCallCenter(1);
                    finish();
                }
            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }

    public AlertDialog dialogMensajeDeuda(String mensaje, final int idSolicitud) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_mensaje_deuda, null);
        TextView txtMensaje = v.findViewById(R.id.txt_mensaje);

        Button btnAceptar = v.findViewById(R.id.btn_aceptar);

        txtMensaje.setText(mensaje);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogRezice.dismiss();

                List<Solicitud> list = obtenerSolicitud(getApplicationContext());
                if (list == null) return;
                if (list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getIdSolicitud() == idSolicitud) {
                            list.get(i).setEstadoSolicitud(4);
                        }
                    }

                    guardarSolicitud(getApplicationContext(), list);

                }
                adaptadorListaSolicitudes.actualizarSolicitudCancelada(idSolicitud);
                cargarListasSolicitudes();
                if (countDownTimer != null) {
                    countDownTimer.cancel();
                }

            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }


    public void cargarListasSolicitudes() {
        final List<Solicitud> list = obtenerSolicitud(getApplicationContext());

        solicitudNueva = new ArrayList<>();
        solicitudPendiente = new ArrayList<>();

        if (list != null) {
            for (Solicitud solicitud : list) {
                if (solicitud.getEstadoSolicitud() == 1) {
                    solicitudNueva.add(solicitud);
                    contSolicitudes.setVisibility(View.VISIBLE);
                    txtMensaje.setVisibility(View.GONE);
                }

                if (solicitud.getEstadoSolicitud() == 3) {
                    solicitudPendiente.add(solicitud);
                    contSolicitudesPendientes.setVisibility(View.VISIBLE);
                    txtMensaje.setVisibility(View.GONE);
                }
            }

            if (solicitudNueva.size() > 0 && solicitudPendiente.size() == 0) {
                txtMensaje.setVisibility(View.GONE);
                contSolicitudes.setVisibility(View.VISIBLE);
                contSolicitudesPendientes.setVisibility(View.GONE);

            } else if (solicitudPendiente.size() > 0 && solicitudNueva.size() == 0) {
                txtMensaje.setVisibility(View.GONE);
                contSolicitudes.setVisibility(View.GONE);
                contSolicitudesPendientes.setVisibility(View.VISIBLE);
            } else if (solicitudPendiente.size() == 0 && solicitudNueva.size() == 0) {
                txtMensaje.setVisibility(View.VISIBLE);
                contSolicitudes.setVisibility(View.GONE);
                contSolicitudesPendientes.setVisibility(View.GONE);
            }

            /*if (solicitudNueva != null) {
                if (solicitudNueva.size() == 1) {
                    cronometroBorrarLista();
                }
            }*/

            adaptadorListaSolicitudes = new AdaptadorListaSolicitudes(getApplicationContext(),
                    solicitudNueva, new AdaptadorListaSolicitudes.OnClicklistener() {
                @Override
                public void onClic(Solicitud solicitud, int posicion) {
                    List<Solicitud> list = obtenerSolicitud(getApplicationContext());
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getIdSolicitud() == solicitud.getIdSolicitud()) {
                            list.get(i).setEstadoSolicitud(4);
                            guardarSolicitud(getApplicationContext(), list);
                            adaptadorListaSolicitudes.borrarSolicitud(solicitud.getIdSolicitud(), posicion);
                        }
                    }
                    cargarListasSolicitudes();

                }
            }, new AdaptadorListaSolicitudes.OnClicklistenerTiempo1() {
                @Override
                public void onClicTiempo1(Solicitud solicitud, int posicion) {
                    mostrarProgressDialogEnvioTiempo(ListaSolicitudes.this, "Esperando confirmación del cliente...");
                    cronometro(solicitud.getIdSolicitud());
                    idSolicitudActiva = solicitud.getIdSolicitud();
                    userNameActiva = solicitud.getUsername();

                    List<Solicitud> solicitudList = obtenerSolicitud(getApplicationContext());
                    for (int i = 0; i < solicitudList.size(); i++) {
                        if (solicitudList.get(i).getIdSolicitud() == idSolicitudActiva) {
                            solicitudList.get(i).setPostulacion(true);
                        }
                    }
                    guardarSolicitud(getApplicationContext(), solicitudList);

                    tiempoSolicitud = solicitud.getDatosSolicitud().getTiempos().get(0);
                    SimpleDateFormat f = new SimpleDateFormat("HH:mm");
                    int tiempoTotalEntrega = 0;
                    Date d1 = null;
                    for (Solicitud sol : list) {
                        if (sol.getEstadoSolicitud() == 2 || sol.getEstadoSolicitud() == 3) {
                            try {
                                d1 = f.parse(sol.getTiempoEntrega());
                                tiempoTotalEntrega = tiempoTotalEntrega + (int) TimeUnit.MILLISECONDS.toMinutes(d1.getTime());

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    Log.e("tiempo", tiempoTotalEntrega + "");
                    //conexionServer.enviarTiempo(solicitud, solicitud.getDatosSolicitud().getTiempos().get(0), solicitud.getUsername());
                    if (countDownTimerBorrarSolicitud != null) {
                        countDownTimerBorrarSolicitud.cancel();
                    }
                }
            }, new AdaptadorListaSolicitudes.OnClicklistenerTiempo2() {
                @Override
                public void onClicTiempo2(Solicitud solicitud, int posicion) {
                    mostrarProgressDialogEnvioTiempo(ListaSolicitudes.this, "Esperando confirmación del cliente...");
                    cronometro(solicitud.getIdSolicitud());
                    idSolicitudActiva = solicitud.getIdSolicitud();
                    userNameActiva = solicitud.getUsername();

                    List<Solicitud> solicitudList = obtenerSolicitud(getApplicationContext());
                    for (int i = 0; i < solicitudList.size(); i++) {
                        if (solicitudList.get(i).getIdSolicitud() == idSolicitudActiva) {
                            solicitudList.get(i).setPostulacion(true);
                        }
                    }
                    guardarSolicitud(getApplicationContext(), solicitudList);

                    tiempoSolicitud = solicitud.getDatosSolicitud().getTiempos().get(1);
                    conexionServer.enviarTiempo(solicitud, solicitud.getDatosSolicitud().getTiempos().get(1), solicitud.getUsername());
                    if (countDownTimerBorrarSolicitud != null) {
                        countDownTimerBorrarSolicitud.cancel();
                    }
                }
            }, new AdaptadorListaSolicitudes.OnClicklistenerTiempo3() {
                @Override
                public void onClicTiempo3(Solicitud solicitud, int posicion) {
                    mostrarProgressDialogEnvioTiempo(ListaSolicitudes.this, "Esperando confirmación del cliente...");
                    cronometro(solicitud.getIdSolicitud());
                    idSolicitudActiva = solicitud.getIdSolicitud();
                    userNameActiva = solicitud.getUsername();

                    List<Solicitud> solicitudList = obtenerSolicitud(getApplicationContext());
                    for (int i = 0; i < solicitudList.size(); i++) {
                        if (solicitudList.get(i).getIdSolicitud() == idSolicitudActiva) {
                            solicitudList.get(i).setPostulacion(true);
                        }
                    }
                    guardarSolicitud(getApplicationContext(), solicitudList);

                    tiempoSolicitud = solicitud.getDatosSolicitud().getTiempos().get(2);
                    conexionServer.enviarTiempo(solicitud, solicitud.getDatosSolicitud().getTiempos().get(2), solicitud.getUsername());
                    if (countDownTimerBorrarSolicitud != null) {
                        countDownTimerBorrarSolicitud.cancel();
                    }
                }
            }, new AdaptadorListaSolicitudes.OnClicklistenerTiempo4() {
                @Override
                public void onClicTiempo4(Solicitud solicitud, int posicion) {
                    mostrarProgressDialogEnvioTiempo(ListaSolicitudes.this, "Esperando confirmación del cliente...");
                    cronometro(solicitud.getIdSolicitud());
                    idSolicitudActiva = solicitud.getIdSolicitud();
                    userNameActiva = solicitud.getUsername();

                    List<Solicitud> solicitudList = obtenerSolicitud(getApplicationContext());
                    for (int i = 0; i < solicitudList.size(); i++) {
                        if (solicitudList.get(i).getIdSolicitud() == idSolicitudActiva) {
                            solicitudList.get(i).setPostulacion(true);
                        }
                    }
                    guardarSolicitud(getApplicationContext(), solicitudList);

                    tiempoSolicitud = solicitud.getDatosSolicitud().getTiempos().get(3);
                    conexionServer.enviarTiempo(solicitud, solicitud.getDatosSolicitud().getTiempos().get(3), solicitud.getUsername());
                    if (countDownTimerBorrarSolicitud != null) {
                        countDownTimerBorrarSolicitud.cancel();
                    }
                }
            }, new AdaptadorListaSolicitudes.OnClicklistenerAumentarTiempo() {
                @Override
                public void onClicAumentarTiempo(Solicitud solicitud, int posicion) {
                    dialogRezice = dialogAumentarTiempo(solicitud);
                    dialogRezice.show();
                }
            });

            recyclerSolicitud.setAdapter(adaptadorListaSolicitudes);

            adaptadorListaSolicitudesPendientes = new AdaptadorListaSolicitudesPendientes(getApplicationContext(),
                    solicitudPendiente, new AdaptadorListaSolicitudesPendientes.OnClicklistener() {
                @Override
                public void onClic(final int idSolicitud, int posicion) {
                    if (obtenerEstadoActividadConductor(getApplicationContext()).isEstadoActividadConductor()) {
                        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                        if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                            v.vibrate(2000);
                            reproducirTextAudio.speak("Aun se encuentra realizando una entrega");
                        }
                    } else {
                        new AlertDialog.Builder(ListaSolicitudes.this)
                                .setTitle("Aviso")
                                .setMessage("¿Está seguro de realizar esta entrega?")
                                .setCancelable(false)
                                .setPositiveButton("CONFIRMAR", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        List<Solicitud> solicitudList = obtenerSolicitud(getApplicationContext());
                                        for (int i = 0; i < solicitudList.size(); i++) {
                                            if (solicitudList.get(i).getIdSolicitud() == idSolicitud &&
                                                    solicitudList.get(i).getEstadoSolicitud() == 3) {
                                                solicitudList.get(i).setEstadoSolicitud(2);
                                            }
                                        }
                                        preferencia.setEstadoActividadConductor(true);
                                        guardarEstadoActividadConductor(preferencia, getApplicationContext());
                                        guardarSolicitud(getApplicationContext(), solicitudList);

                                        int numeroSolicitudes = obtenerNumeroSolicitudesPendientes(getApplicationContext()).getNumeroSolicitudes() - 1;
                                        preferencia.setNumeroSolicitudes(numeroSolicitudes);
                                        guardarNumeroSolicitudesPendientes(preferencia, getApplicationContext());

                                        finish();
                                    }
                                })
                                .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                }).show();
                    }

                }
            });

            recyclerSolicitudPendientes.setAdapter(adaptadorListaSolicitudesPendientes);


        } else {
            txtMensaje.setVisibility(View.VISIBLE);
            contSolicitudes.setVisibility(View.GONE);
            contSolicitudesPendientes.setVisibility(View.GONE);
        }
    }

    public AlertDialog dialogAumentarTiempo(final Solicitud solicitud) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = this.getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_aumentar_tiempo, null);
        final Button btnAceptar = (Button) v.findViewById(R.id.btn_aceptar);
        Button btnCancelar = (Button) v.findViewById(R.id.btn_cancelar);
        final TextView txtEditTiempo = (TextView) v.findViewById(R.id.txtEditTiempo);
        ImageView imgMas = (ImageView) v.findViewById(R.id.imgMas);
        ImageView imgMenos = (ImageView) v.findViewById(R.id.imgMenos);
        final int[] cont = {0};

        final ArrayList<String> items_array1 = new ArrayList<>();
        items_array1.add("10");
        items_array1.add("15");
        items_array1.add("20");
        items_array1.add("25");
        items_array1.add("30");
        items_array1.add("35");

        txtEditTiempo.setText(items_array1.get(0));
        valortiempo1 = true;
        tiempo1 = items_array1.get(0);

        imgMas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                valortiempo1 = true;
                if (!txtEditTiempo.getText().toString().trim().isEmpty()) {
                    if (!txtEditTiempo.getText().toString().trim().equals("35")) {
                        cont[0] = cont[0] + 1;
                        txtEditTiempo.setText(items_array1.get(cont[0]));
                        tiempo1 = items_array1.get(cont[0]);
                    }
                }

            }
        });

        imgMenos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                valortiempo1 = true;
                if (!txtEditTiempo.getText().toString().trim().isEmpty()) {
                    if (!txtEditTiempo.getText().toString().trim().equals("10")) {
                        cont[0] = cont[0] - 1;
                        txtEditTiempo.setText(items_array1.get(cont[0]));
                        tiempo1 = items_array1.get(cont[0]);
                    }
                }
            }
        });

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (valortiempo1) {
                    cronometro(solicitud.getIdSolicitud());
                    idSolicitudActiva = solicitud.getIdSolicitud();
                    userNameActiva = solicitud.getUsername();
                    List<Solicitud> solicitudList = obtenerSolicitud(getApplicationContext());
                    for (int i = 0; i < solicitudList.size(); i++) {
                        if (solicitudList.get(i).getIdSolicitud() == idSolicitudActiva) {
                            solicitudList.get(i).setPostulacion(true);
                        }
                    }
                    guardarSolicitud(getApplicationContext(), solicitudList);

                    mostrarProgressDialogEnvioTiempo(ListaSolicitudes.this, "Esperando confirmación del cliente...");
                    tiempoSolicitud = Integer.parseInt(txtEditTiempo.getText().toString().trim());
                    conexionServer.enviarTiempo(solicitud, Integer.parseInt(txtEditTiempo.getText().toString().trim()), solicitud.getUsername());
                    if (countDownTimerBorrarSolicitud != null) {
                        countDownTimerBorrarSolicitud.cancel();
                    }
                }
                dialogRezice.dismiss();
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogRezice.dismiss();
            }
        });

        localBuilder.setView(v);
        return localBuilder.create();
    }


    public void cronometro(final int idSolicitud) {

        countDownTimer = new CountDownTimer(35000, 1000) {
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                        v.vibrate(2000);
                        reproducirTextAudio.speak("No hemos recibido respuesta del cliente");
                    }
                    ocultarProgressDialog();

                    List<Solicitud> list = obtenerSolicitud(getApplicationContext());
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getIdSolicitud() == idSolicitud) {
                            list.get(i).setEstadoSolicitud(4);
                            break;
                        }
                    }
                    adaptadorListaSolicitudes.actualizarSolicitudCancelada(idSolicitud);
                    guardarSolicitud(getApplicationContext(), list);

                }
                if (countDownTimer != null) {
                    countDownTimer.cancel();
                }
                cargarListasSolicitudes();

            }
        };
        countDownTimer.start();
    }

    public void cronometroBorrarLista() {

        countDownTimerBorrarSolicitud = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                Log.e("cronometro", millisUntilFinished * 1000 + "");
            }

            public void onFinish() {
                if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                    List<Solicitud> list = obtenerSolicitud(getApplicationContext());
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getEstadoSolicitud() == 1) {
                            list.get(i).setEstadoSolicitud(4);
                        }
                    }
                    adaptadorListaSolicitudes.borrarTodasLasSolicitudes();
                    guardarSolicitud(getApplicationContext(), list);

                }
                if (countDownTimerBorrarSolicitud != null) {
                    countDownTimerBorrarSolicitud.cancel();
                }
                cargarListasSolicitudes();

            }
        };
        countDownTimerBorrarSolicitud.start();
    }


    //Conexion server

    public void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (!isConnected) {
            message = "Se ha perdido la conexión";
            color = Color.GREEN;
            Snackbar snackbar = Snackbar.make(mainContent, message, Snackbar.LENGTH_INDEFINITE);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();

        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (!isConnected) {
            String message = "Se ha perdido la conexión";
            int color = Color.GREEN;
            Snackbar snackbar = Snackbar.make(mainContent, message, Snackbar.LENGTH_INDEFINITE);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();

        } else {
            String message = "Se ha establecido la conexión";
            int color = Color.WHITE;
            Snackbar snackbar = Snackbar.make(mainContent, message, Snackbar.LENGTH_LONG);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();

            Intent intent = new Intent(getApplicationContext(), Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }

    }


    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            Intent intentbind = new Intent(ListaSolicitudes.this, Servicio.class);
            bindService(intentbind, mConnection, Context.BIND_AUTO_CREATE);
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        verf = false;
        LojagasAplicacion.getInstance().setConnectivityListener(this);

    }

    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        this.wakelock.release();
    }


    @Override
    public void onPause() {
        super.onPause();
        verf = true;
        wakelock.acquire();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.wakelock.release();

    }

    @Override
    public void conectadoServ() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(ListaSolicitudes.this, MainActivity.class));
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }
}
