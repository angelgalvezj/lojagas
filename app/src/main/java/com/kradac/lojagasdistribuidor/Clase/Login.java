package com.kradac.lojagasdistribuidor.Clase;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.awareness.fence.LocationFence;
import com.google.android.gms.common.api.Status;
import com.google.gson.JsonObject;
import com.kradac.lojagasdistribuidor.Adaptador.AdaptadorListaVehiculo;
import com.kradac.lojagasdistribuidor.Api.LojagasApi;
import com.kradac.lojagasdistribuidor.Modelo.Pedido;
import com.kradac.lojagasdistribuidor.Modelo.Solicitud;
import com.kradac.lojagasdistribuidor.Modelo.Usuario;
import com.kradac.lojagasdistribuidor.Presenter.PresenterMetadata;
import com.kradac.lojagasdistribuidor.Presenter.PresenterRecuperarClave;
import com.kradac.lojagasdistribuidor.Presenter.PresenterValidarImei;
import com.kradac.lojagasdistribuidor.R;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionLogin;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionRecuperarClave;
import com.kradac.lojagasdistribuidor.Response.ResponseApi;
import com.kradac.lojagasdistribuidor.Servicio.Servicio;
import com.kradac.lojagasdistribuidor.Util.ConnectivityReceiver;
import com.kradac.lojagasdistribuidor.Util.Funciones;
import com.kradac.lojagasdistribuidor.Util.Gps;
import com.kradac.lojagasdistribuidor.Util.LojagasAplicacion;
import com.kradac.lojagasdistribuidor.Util.VariablesGlobales;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.kradac.lojagasdistribuidor.Util.Gps.REQUEST_CHECK_SETTINGS;

public class Login extends Funciones implements OnComunicacionLogin,
        ConnectivityReceiver.ConnectivityReceiverListener, OnComunicacionRecuperarClave {

    @BindView(R.id.editPassUsuario)
    EditText editPassUsuario;
    @BindView(R.id.ch_pass)
    CheckBox chPass;
    @BindView(R.id.btn_iniciar_sesion)
    Button btnIniciarSesion;
    @BindView(R.id.btn_registrarse)
    Button btnRegistrarse;
    @BindView(R.id.btn_recuperar)
    Button btnRecuperar;
    @BindView(R.id.editUsuario)
    EditText editUsuario;
    @BindView(R.id.main_content)
    CoordinatorLayout mainContent;


    private Servicio conexionServer;
    private boolean mBound, verf;
    private AlertDialog dialogRezice;
    private PresenterValidarImei presenterValidarImei;
    private PresenterMetadata presenterMetadata;
    private double lt_gps, lg_gps;
    private AlertDialog alertDialog;
    private PresenterRecuperarClave presenterRecuperarClave;
    private AdaptadorListaVehiculo adaptadorListaVehiculo;

    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Servicio.LocalBinder binder = (Servicio.LocalBinder) service;
            conexionServer = binder.getService();
            conexionServer.registrarUsuario(Login.this, "Login");
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        if (!verf) {
            if (mConnection != null) {
                Intent intent = new Intent(this, Servicio.class);
                bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            }
        }

        verificarPermisosGps();

        presenterValidarImei = new PresenterValidarImei(this);
        presenterMetadata = new PresenterMetadata(this);
        presenterRecuperarClave = new PresenterRecuperarClave(this);

        btnRecuperar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog = dialogRecuperarClave();
                alertDialog.show();
            }
        });

        chPass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    editPassUsuario.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    int cursor = editPassUsuario.getText().length();
                    editPassUsuario.setSelection(cursor, cursor);
                } else {
                    editPassUsuario.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    int cursor = editPassUsuario.getText().length();
                    editPassUsuario.setSelection(cursor, cursor);
                }
            }
        });

        btnIniciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editPassUsuario.getText().toString().trim().isEmpty()) {
                    Toast.makeText(Login.this, "Ingrese su usuario", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (editPassUsuario.getText().toString().trim().isEmpty()) {
                    Toast.makeText(Login.this, "Ingrese su contraseña", Toast.LENGTH_SHORT).show();
                    return;
                }

                mostrarProgressDialog(Login.this, "Procesando, por favor espere...");
                conexionServer.iniciarSesion(editUsuario.getText().toString().trim(), editPassUsuario.getText().toString().trim());

            }
        });

    }

    public AlertDialog dialogRecuperarClave() {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_recuperar_contrasena, null);
        Button btnAceptar = v.findViewById(R.id.btn_enviar);
        Button btnCancelar = v.findViewById(R.id.btn_cancelar);

        final EditText editUsuario = v.findViewById(R.id.edit_usuario);


        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editUsuario.getText().toString().trim().isEmpty()) {
                    Toast.makeText(Login.this, "Ingrese su usuario", Toast.LENGTH_SHORT).show();
                    return;
                }

                mostrarProgressDialog(Login.this, "Procesando, Espere por favor.");
                presenterRecuperarClave.recuperarClave(editUsuario.getText().toString().trim(), VariablesGlobales.NUM_ID_APLICATIVO);


            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }


    private void verificarPermisosGps() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                + ContextCompat.checkSelfPermission(
                Login.this, Manifest.permission.READ_PHONE_STATE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_PHONE_STATE}, 1);
        } else {
            centrarUbicacionGps();
        }
    }

    private void centrarUbicacionGps() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                + ContextCompat.checkSelfPermission(
                Login.this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_PHONE_STATE}, 1);
        } else {
            if (gps == null) {
                gps = new Gps(getApplicationContext(), locationListener);
            } else {
                Location location = gps.getLastLocation();


            }


        }
    }

    Gps gps;

    Gps.LocationListener locationListener = new Gps.LocationListener() {
        @Override
        public void onLocationChange(Location location) {
            if (gps != null) {
                centrarUbicacionGps();
                lt_gps = location.getLatitude();
                lg_gps = location.getLongitude();
                gps.stopLocationUpdates();
                gps.onstop();
                //gps = null;
            } else {
                verificarPermisosGps();
            }
        }

        @Override
        public void onGooglePlayServicesDisable(int status) {

        }

        @Override
        public void onGpsDisableNewApi(final Status status) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        status.startResolutionForResult(Login.this, REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        public void onGpsDisable() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder alertDialog;
                    alertDialog = new AlertDialog.Builder(Login.this);
                    alertDialog.setTitle(getString(R.string.msg_sericios_ubicacion_deshabilitados));
                    alertDialog.setMessage(getString(R.string.msg_activar_sericios_ubicacion));
                    alertDialog.setPositiveButton(getString(R.string.str_ajustes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent("android.settings.LOCATION_SOURCE_SETTINGS");
                            startActivity(intent);

                        }
                    });
                    alertDialog.setNegativeButton(getString(R.string.str_ignorar), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();

                        }
                    });
                    alertDialog.show();
                }
            });
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int
            resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        Toast.makeText(Login.this, "GPS activado", Toast.LENGTH_LONG).show();
                        centrarUbicacionGps();
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        gps = null;
                        centrarUbicacionGps();
                    }
                    default: {
                        break;
                    }
                }
                break;
        }
    }


    @Override
    public void mensajeErrorContrasena(final String mensaje) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ocultarProgressDialog();
                dialogRezice = dialogMensajeErrorContrasena(mensaje);
                dialogRezice.show();

            }
        });

    }

    @Override
    public void respuestaValidarImei(ResponseApi responseApi) {
        if (responseApi != null) {
            ocultarProgressDialog();
        }

    }

    @Override
    public void cerrarSesion(final String mensaje, final String datos) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ocultarProgressDialog();
                dialogRezice = dialogCerrarSesionDispositivo(mensaje, datos);
                dialogRezice.show();
            }
        });
    }


    @Override
    public void cerrarSesionListaVehiculo(final String mensaje, final Usuario usuario) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ocultarProgressDialog();
                dialogRezice = dialogCerrarSesionListaVehiculo(mensaje, usuario);
                dialogRezice.show();
            }
        });
    }

    @Override
    public void mensajeRespuesta(final String mensaje, final String datos) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mensaje != null) {
                    ocultarProgressDialog();
                    dialogRezice = dialogMensajeConfirmacion(mensaje, datos);
                    dialogRezice.show();
                }
            }
        });
    }

    @Override
    public void mensajeRespuestaCierreListaVehiculos(final String mensaje, final Usuario usuario) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mensaje != null) {
                    ocultarProgressDialog();
                    dialogRezice = dialogMensajeConfirmacionCierreListaVehiculo(mensaje, usuario);
                    dialogRezice.show();
                }
            }
        });
    }

    @Override
    public void mensajeDeuda(final String mensaje) {
        if (mensaje != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ocultarProgressDialog();
                    dialogRezice = dialogMensajeDeuda(mensaje);
                    dialogRezice.show();
                }
            });

        }
    }

    @Override
    public void enviarMetaData(ResponseApi responseApi) {

    }

    @Override
    public void logueoExitoso(String mensaje, final String datos) {
        if (mensaje != null && datos != null) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject datosUsuario = null;
                    String valor = "";
                    try {
                        datosUsuario = new JSONObject(datos);

                        Usuario usuario = new Usuario();
                        Preferencia preferencia = new Preferencia();

                        if (datosUsuario.has("usuario")) {
                            valor = "usuario";

                        }

                        usuario.setBloqueado(datosUsuario.getJSONObject(valor).getInt("bloqueado"));
                        usuario.setId_rol_usuario(datosUsuario.getJSONObject(valor).getInt("id_rol_usuario"));
                        usuario.setRol(datosUsuario.getJSONObject(valor).getInt("rol"));
                        usuario.setIdEquipo(datosUsuario.getJSONObject(valor).getInt("idEquipo"));
                        usuario.setTipoVehiculo(datosUsuario.getJSONObject(valor).getInt("tipoVehiculo"));
                        usuario.setIdCiudad(datosUsuario.getJSONObject(valor).getInt("idCiudad"));
                        usuario.setIdEmpresa(datosUsuario.getJSONObject(valor).getInt("idEmpresa"));
                        usuario.setAnioVehiculo(datosUsuario.getJSONObject(valor).getInt("anioVehiculo"));
                        usuario.setUnidadVehiculo(datosUsuario.getJSONObject(valor).getInt("unidadVehiculo"));
                        usuario.setIdVehiculo(datosUsuario.getJSONObject(valor).getInt("idVehiculo"));
                        usuario.setHabilitado_ktaxidriver(datosUsuario.getJSONObject(valor).getInt("habilitado_ktaxidriver"));
                        usuario.setIdUsuario(datosUsuario.getJSONObject(valor).getInt("idUsuario"));
                        usuario.setMensaje_bloqueo_driver(datosUsuario.getJSONObject(valor).getString("mensaje_bloqueo_driver"));
                        usuario.setUsuario(datosUsuario.getJSONObject(valor).getString("usuario"));
                        usuario.setImagenConductor(datosUsuario.getJSONObject(valor).getString("imagenConductor"));
                        usuario.setNombres(datosUsuario.getJSONObject(valor).getString("nombres"));
                        usuario.setApellidos(datosUsuario.getJSONObject(valor).getString("apellidos"));
                        usuario.setCorreo(datosUsuario.getJSONObject(valor).getString("correo"));
                        usuario.setCelular(datosUsuario.getJSONObject(valor).getString("celular"));
                        usuario.setCedula(datosUsuario.getJSONObject(valor).getString("cedula"));
                        usuario.setEmpresa(datosUsuario.getJSONObject(valor).getString("empresa"));
                        usuario.setPlacaVehiculo(datosUsuario.getJSONObject(valor).getString("placaVehiculo"));
                        usuario.setRegMunVehiculo(datosUsuario.getJSONObject(valor).getString("regMunVehiculo"));
                        usuario.setMarcaVehiculo(datosUsuario.getJSONObject(valor).getString("marcaVehiculo"));
                        usuario.setModeloVehiculo(datosUsuario.getJSONObject(valor).getString("modeloVehiculo"));
                        usuario.setCiudad(datosUsuario.getJSONObject(valor).getString("ciudad"));

                        preferencia.setEstadoLogin(true);

                        guardarDatosUsuario(usuario);
                        guardarEstadoLogin(preferencia, getApplicationContext());

                        startActivity(new Intent(Login.this, MainActivity.class));
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

        }
    }

    @Override
    public void logueoExitosoListaVehiculo(String mensaje, final Usuario usuario) {
        if (mensaje != null && usuario != null) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    ocultarProgressDialog();

                    Usuario datosUsuario = new Usuario();
                    Preferencia preferencia = new Preferencia();
                    preferencia.setEstadoLogin(true);
                    guardarEstadoLogin(preferencia, getApplicationContext());

                    datosUsuario.setBloqueado(usuario.getBloqueado());
                    datosUsuario.setId_rol_usuario(usuario.getId_rol_usuario());
                    datosUsuario.setRol(usuario.getRol());
                    datosUsuario.setIdEquipo(usuario.getIdEquipo());
                    datosUsuario.setTipoVehiculo(usuario.getTipoVehiculo());
                    datosUsuario.setIdCiudad(usuario.getIdCiudad());
                    datosUsuario.setIdEmpresa(usuario.getIdEmpresa());
                    datosUsuario.setAnioVehiculo(usuario.getAnioVehiculo());
                    datosUsuario.setUnidadVehiculo(usuario.getUnidadVehiculo());
                    datosUsuario.setIdVehiculo(usuario.getIdVehiculo());
                    datosUsuario.setHabilitado_ktaxidriver(usuario.getHabilitado_ktaxidriver());
                    datosUsuario.setIdUsuario(usuario.getIdUsuario());
                    datosUsuario.setMensaje_bloqueo_driver(usuario.getMensaje_bloqueo_driver());
                    datosUsuario.setUsuario(usuario.getUsuario());
                    datosUsuario.setImagenConductor(usuario.getImagenConductor());
                    datosUsuario.setNombres(usuario.getNombres());
                    datosUsuario.setApellidos(usuario.getApellidos());
                    datosUsuario.setCorreo(usuario.getCorreo());
                    datosUsuario.setCelular(usuario.getCelular());
                    datosUsuario.setCedula(usuario.getCedula());
                    datosUsuario.setEmpresa(usuario.getEmpresa());
                    datosUsuario.setPlacaVehiculo(usuario.getPlacaVehiculo());
                    datosUsuario.setRegMunVehiculo(usuario.getRegMunVehiculo());
                    datosUsuario.setMarcaVehiculo(usuario.getMarcaVehiculo());
                    datosUsuario.setModeloVehiculo(usuario.getModeloVehiculo());
                    datosUsuario.setCiudad(usuario.getCiudad());

                    if(alertDialog!=null){
                        alertDialog.dismiss();
                    }

                    guardarDatosUsuario(datosUsuario);

                    startActivity(new Intent(Login.this, MainActivity.class));
                    finish();


                }
            });

        }
    }

    public AlertDialog dialogListVehiculos(final List<Usuario> usuarioList, final String datos) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(true);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_lista_vehiculos, null);

        Button btnCancelar = (Button) v.findViewById(R.id.btn_cancelar);
        RecyclerView recyclerLista = (RecyclerView) v.findViewById(R.id.recycler_vehiculo);

        LinearLayoutManager layoutManagerNueva = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerLista.setNestedScrollingEnabled(false);
        recyclerLista.setLayoutManager(layoutManagerNueva);

        adaptadorListaVehiculo = new AdaptadorListaVehiculo(getApplicationContext(), usuarioList,
                new AdaptadorListaVehiculo.OnClicklistener() {
                    @Override
                    public void onClic(Usuario usuario, int posicion) {
                        dialogRezice.dismiss();
                        mostrarProgressDialog(Login.this,"Procesando. Espere por favor...");

                        conexionServer.logueoPorVehiculo(usuario.getIdVehiculo(),
                                usuario.getIdCiudad(),
                                usuario.getIdEmpresa(),
                                usuario.getIdEmpresa(), datos, usuario);
                    }
                });

        recyclerLista.setAdapter(adaptadorListaVehiculo);

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogRezice.dismiss();
            }
        });

        localBuilder.setView(v);
        return localBuilder.create();
    }


    @Override
    public void dispositivoRegistrado(final String mensaje) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ocultarProgressDialog();
                dialogRezice = dialogMensajeDispositivoActivado(mensaje);
                dialogRezice.show();
            }
        });

    }

    @Override
    public void seleccionDispositivo(final String datos) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                JSONObject datosUsuario = null;
                try {
                    if (datos != null) {

                        datosUsuario = new JSONObject(datos);

                        if (datosUsuario.has("usuarios")) {
                            JSONArray lcarac = datosUsuario.getJSONArray("usuarios");
                            List<Usuario> usuarioList = new ArrayList<>();
                            for (int i = 0; i < lcarac.length(); i++) {
                                JSONObject datos = lcarac.getJSONObject(i);
                                Usuario usuario = new Usuario();
                                usuario.setBloqueado(datos.getInt("bloqueado"));
                                usuario.setId_rol_usuario(datos.getInt("id_rol_usuario"));
                                usuario.setRol(datos.getInt("rol"));
                                usuario.setIdEquipo(datos.getInt("idEquipo"));
                                usuario.setTipoVehiculo(datos.getInt("tipoVehiculo"));
                                usuario.setIdCiudad(datos.getInt("idCiudad"));
                                usuario.setIdEmpresa(datos.getInt("idEmpresa"));
                                usuario.setAnioVehiculo(datos.getInt("anioVehiculo"));
                                usuario.setUnidadVehiculo(datos.getInt("unidadVehiculo"));
                                usuario.setIdVehiculo(datos.getInt("idVehiculo"));
                                usuario.setHabilitado_ktaxidriver(datos.getInt("habilitado_ktaxidriver"));
                                usuario.setIdUsuario(datos.getInt("idUsuario"));
                                usuario.setMensaje_bloqueo_driver(datos.getString("mensaje_bloqueo_driver"));
                                usuario.setUsuario(datos.getString("usuario"));
                                usuario.setImagenConductor(datos.getString("imagenConductor"));
                                usuario.setNombres(datos.getString("nombres"));
                                usuario.setApellidos(datos.getString("apellidos"));
                                usuario.setCorreo(datos.getString("correo"));
                                usuario.setCelular(datos.getString("celular"));
                                usuario.setCedula(datos.getString("cedula"));
                                usuario.setEmpresa(datos.getString("empresa"));
                                usuario.setPlacaVehiculo(datos.getString("placaVehiculo"));
                                usuario.setRegMunVehiculo(datos.getString("regMunVehiculo"));
                                usuario.setMarcaVehiculo(datos.getString("marcaVehiculo"));
                                usuario.setModeloVehiculo(datos.getString("modeloVehiculo"));
                                usuario.setCiudad(datos.getString("ciudad"));
                                usuarioList.add(usuario);
                            }
                            ocultarProgressDialog();
                            dialogRezice = dialogListVehiculos(usuarioList, datos);
                            dialogRezice.show();
                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    @Override
    public void mensajeActivacionDispositivo(final int auxIdUsuario, final String mensaje) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ocultarProgressDialog();
                dialogRezice = dialogMensajeActivarDispositivo(auxIdUsuario, mensaje);
                dialogRezice.show();
            }
        });
    }


    public AlertDialog dialogMensajeErrorContrasena(String mensaje) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_mensaje_error_contrasena, null);
        TextView txtMensaje = (TextView) v.findViewById(R.id.txt_mensaje);
        Button btnReintentar = (Button) v.findViewById(R.id.btn_reitentar);

        txtMensaje.setText(mensaje);

        btnReintentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogRezice.dismiss();
            }
        });

        localBuilder.setView(v);
        return localBuilder.create();
    }


    public AlertDialog dialogMensajeDispositivoActivado(String mensaje) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_dispositivo_activado, null);
        TextView txtMensaje = v.findViewById(R.id.txt_mensaje);

        Button btnAceptar = v.findViewById(R.id.btn_aceptar);

        txtMensaje.setText(mensaje);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogRezice.dismiss();

            }
        });
        localBuilder.setView(v);
        return localBuilder.create();
    }

    public AlertDialog dialogMensajeActivarDispositivo(final int auxIdUsuario, String mensaje) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_mensaje_activacion_dispositivo, null);
        TextView txtMensaje = v.findViewById(R.id.txt_mensaje);

        Button btnAceptar = v.findViewById(R.id.btn_aceptar);
        Button btnCancelar = v.findViewById(R.id.btn_cancelar);

        txtMensaje.setText(mensaje);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogRezice.dismiss();
                mostrarProgressDialog(Login.this, "Activando dispositivo, por favor espere...");
                presenterValidarImei.validarImei(auxIdUsuario, obtenerIMEI(getApplicationContext()),
                        obtenerMarca(), obtenerModelo(), obtenerVersionSo(),
                        obtenerVersion(getApplicationContext()), lt_gps, lg_gps);
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogRezice.dismiss();
            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }

    public AlertDialog dialogCerrarSesionDispositivo(String mensaje, final String datos) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_mensaje_activacion_dispositivo, null);
        TextView txtMensaje = v.findViewById(R.id.txt_mensaje);

        Button btnAceptar = v.findViewById(R.id.btn_aceptar);
        Button btnCancelar = v.findViewById(R.id.btn_cancelar);

        txtMensaje.setText(mensaje + " " + getString(R.string.cerrar_sesion));

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarProgressDialog(Login.this, "Cerrando sesión. Espere por favor...");
                try {
                    JSONObject datosUsuario = new JSONObject(datos);
                    if (datosUsuario.has("usuario")) {
                        conexionServer.cerrarSesionDispositivo(datos,
                                datosUsuario.getJSONObject("usuario").getInt("idVehiculo"),
                                datosUsuario.getJSONObject("usuario").getInt("idCiudad"),
                                datosUsuario.getJSONObject("usuario").getInt("idEmpresa"),
                                datosUsuario.getJSONObject("usuario").getInt("idUsuario"),
                                datosUsuario.getJSONObject("usuario").getString("nombres"));
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialogRezice.dismiss();

            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogRezice.dismiss();
            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }


    public AlertDialog dialogCerrarSesionListaVehiculo(String mensaje, final Usuario usuario) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_mensaje_activacion_dispositivo, null);
        TextView txtMensaje = v.findViewById(R.id.txt_mensaje);

        Button btnAceptar = v.findViewById(R.id.btn_aceptar);
        Button btnCancelar = v.findViewById(R.id.btn_cancelar);

        txtMensaje.setText(mensaje + " " + getString(R.string.cerrar_sesion));

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarProgressDialog(Login.this, "Cerrando sesión. Espere por favor...");
                conexionServer.cerrarSesionListaDispositivo(usuario);
                dialogRezice.dismiss();

            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogRezice.dismiss();
            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }


    public AlertDialog dialogMensajeConfirmacion(String mensaje, final String datos) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_mensaje_confirmacion, null);
        TextView txtMensaje = v.findViewById(R.id.txt_mensaje);

        Button btnAceptar = v.findViewById(R.id.btn_aceptar);

        txtMensaje.setText(mensaje);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject datosUsuario = null;
                String valor = "";
                try {
                    datosUsuario = new JSONObject(datos);
                    Usuario usuario = new Usuario();
                    Preferencia preferencia = new Preferencia();

                    if (datosUsuario.has("usuario")) {
                        valor = "usuario";
                    }

                    usuario.setBloqueado(datosUsuario.getJSONObject(valor).getInt("bloqueado"));
                    usuario.setId_rol_usuario(datosUsuario.getJSONObject(valor).getInt("id_rol_usuario"));
                    usuario.setRol(datosUsuario.getJSONObject(valor).getInt("rol"));
                    usuario.setIdEquipo(datosUsuario.getJSONObject(valor).getInt("idEquipo"));
                    usuario.setTipoVehiculo(datosUsuario.getJSONObject(valor).getInt("tipoVehiculo"));
                    usuario.setIdCiudad(datosUsuario.getJSONObject(valor).getInt("idCiudad"));
                    usuario.setIdEmpresa(datosUsuario.getJSONObject(valor).getInt("idEmpresa"));
                    usuario.setAnioVehiculo(datosUsuario.getJSONObject(valor).getInt("anioVehiculo"));
                    usuario.setUnidadVehiculo(datosUsuario.getJSONObject(valor).getInt("unidadVehiculo"));
                    usuario.setIdVehiculo(datosUsuario.getJSONObject(valor).getInt("idVehiculo"));
                    usuario.setHabilitado_ktaxidriver(datosUsuario.getJSONObject(valor).getInt("habilitado_ktaxidriver"));
                    usuario.setIdUsuario(datosUsuario.getJSONObject(valor).getInt("idUsuario"));
                    usuario.setMensaje_bloqueo_driver(datosUsuario.getJSONObject(valor).getString("mensaje_bloqueo_driver"));
                    usuario.setUsuario(datosUsuario.getJSONObject(valor).getString("usuario"));
                    usuario.setImagenConductor(datosUsuario.getJSONObject(valor).getString("imagenConductor"));
                    usuario.setNombres(datosUsuario.getJSONObject(valor).getString("nombres"));
                    usuario.setApellidos(datosUsuario.getJSONObject(valor).getString("apellidos"));
                    usuario.setCorreo(datosUsuario.getJSONObject(valor).getString("correo"));
                    usuario.setCelular(datosUsuario.getJSONObject(valor).getString("celular"));
                    usuario.setCedula(datosUsuario.getJSONObject(valor).getString("cedula"));
                    usuario.setEmpresa(datosUsuario.getJSONObject(valor).getString("empresa"));
                    usuario.setPlacaVehiculo(datosUsuario.getJSONObject(valor).getString("placaVehiculo"));
                    usuario.setRegMunVehiculo(datosUsuario.getJSONObject(valor).getString("regMunVehiculo"));
                    usuario.setMarcaVehiculo(datosUsuario.getJSONObject(valor).getString("marcaVehiculo"));
                    usuario.setModeloVehiculo(datosUsuario.getJSONObject(valor).getString("modeloVehiculo"));
                    usuario.setCiudad(datosUsuario.getJSONObject(valor).getString("ciudad"));

                    preferencia.setEstadoLogin(true);

                    guardarDatosUsuario(usuario);
                    guardarEstadoLogin(preferencia, getApplicationContext());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                presenterMetadata.enviarMetaData(1, obtenerVersion(getApplicationContext()),
                        obtenerVersionSo(), obtenerMarca(), obtenerModelo(), obtenerDatosUsuario(getApplicationContext()).getIdUsuario());
                dialogRezice.dismiss();
                startActivity(new Intent(Login.this, MainActivity.class));
                finish();

            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }

    public AlertDialog dialogMensajeConfirmacionCierreListaVehiculo(String mensaje, final Usuario usuario) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_mensaje_confirmacion, null);
        TextView txtMensaje = v.findViewById(R.id.txt_mensaje);

        Button btnAceptar = v.findViewById(R.id.btn_aceptar);

        txtMensaje.setText(mensaje);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Usuario datosUsuario = new Usuario();
                Preferencia preferencia = new Preferencia();

                datosUsuario.setBloqueado(usuario.getBloqueado());
                datosUsuario.setId_rol_usuario(usuario.getId_rol_usuario());
                datosUsuario.setRol(usuario.getRol());
                datosUsuario.setIdEquipo(usuario.getIdEquipo());
                datosUsuario.setTipoVehiculo(usuario.getTipoVehiculo());
                datosUsuario.setIdCiudad(usuario.getIdCiudad());
                datosUsuario.setIdEmpresa(usuario.getIdEmpresa());
                datosUsuario.setAnioVehiculo(usuario.getAnioVehiculo());
                datosUsuario.setUnidadVehiculo(usuario.getUnidadVehiculo());
                datosUsuario.setIdVehiculo(usuario.getIdVehiculo());
                datosUsuario.setHabilitado_ktaxidriver(usuario.getHabilitado_ktaxidriver());
                datosUsuario.setIdUsuario(usuario.getIdUsuario());
                datosUsuario.setMensaje_bloqueo_driver(usuario.getMensaje_bloqueo_driver());
                datosUsuario.setUsuario(usuario.getUsuario());
                datosUsuario.setImagenConductor(usuario.getImagenConductor());
                datosUsuario.setNombres(usuario.getNombres());
                datosUsuario.setApellidos(usuario.getApellidos());
                datosUsuario.setCorreo(usuario.getCorreo());
                datosUsuario.setCelular(usuario.getCelular());
                datosUsuario.setCedula(usuario.getCedula());
                datosUsuario.setEmpresa(usuario.getEmpresa());
                datosUsuario.setPlacaVehiculo(usuario.getPlacaVehiculo());
                datosUsuario.setRegMunVehiculo(usuario.getRegMunVehiculo());
                datosUsuario.setMarcaVehiculo(usuario.getMarcaVehiculo());
                datosUsuario.setModeloVehiculo(usuario.getModeloVehiculo());
                datosUsuario.setCiudad(usuario.getCiudad());

                preferencia.setEstadoLogin(true);

                guardarDatosUsuario(datosUsuario);
                guardarEstadoLogin(preferencia, getApplicationContext());


                presenterMetadata.enviarMetaData(1, obtenerVersion(getApplicationContext()),
                        obtenerVersionSo(), obtenerMarca(), obtenerModelo(), obtenerDatosUsuario(getApplicationContext()).getIdUsuario());
                dialogRezice.dismiss();
                startActivity(new Intent(Login.this, MainActivity.class));
                finish();

            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }

    public AlertDialog dialogMensajeDeuda(String mensaje) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_mensaje_deuda, null);
        TextView txtMensaje = v.findViewById(R.id.txt_mensaje);

        Button btnAceptar = v.findViewById(R.id.btn_aceptar);

        txtMensaje.setText(mensaje);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogRezice.dismiss();

            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }


    //Conexion servicio

    @Override
    public void conectadoServ() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });
    }

    public void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (!isConnected) {
            message = "Se ha perdido la conexión";
            color = Color.GREEN;
            Snackbar snackbar = Snackbar.make(mainContent, message, Snackbar.LENGTH_INDEFINITE);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();

        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (!isConnected) {
            String message = "Se ha perdido la conexión";
            int color = Color.GREEN;
            Snackbar snackbar = Snackbar.make(mainContent, message, Snackbar.LENGTH_INDEFINITE);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();

        } else {
            String message = "Se ha establecido la conexión";
            int color = Color.WHITE;
            Snackbar snackbar = Snackbar.make(mainContent, message, Snackbar.LENGTH_LONG);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();

            Intent intent = new Intent(getApplicationContext(), Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }

    }

    //Conexion server
    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            Intent intentbind = new Intent(Login.this, Servicio.class);
            bindService(intentbind, mConnection, Context.BIND_AUTO_CREATE);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        verf = false;
        LojagasAplicacion.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        verf = true;
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void respuestaRecuperarClave(ResponseApi responseApi) {
        if (responseApi != null) {
            if (responseApi.getEstado() == 1) {
                ocultarProgressDialog();
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
                new AlertDialog.Builder(Login.this)
                        .setTitle("Aviso")
                        .setMessage(responseApi.getMensaje())
                        .setCancelable(false)
                        .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            } else {
                ocultarProgressDialog();
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
                new AlertDialog.Builder(Login.this)
                        .setTitle("Aviso")
                        .setMessage(responseApi.getMensaje())
                        .setCancelable(false)
                        .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        }

    }
}
