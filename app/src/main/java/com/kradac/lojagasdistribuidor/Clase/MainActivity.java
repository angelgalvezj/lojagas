package com.kradac.lojagasdistribuidor.Clase;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.kradac.lojagasdistribuidor.Adaptador.AdaptadorListaCaracteristicas;
import com.kradac.lojagasdistribuidor.Adaptador.ChatAdapter;
import com.kradac.lojagasdistribuidor.Adaptador.ListAdapterMensajes;
import com.kradac.lojagasdistribuidor.Modelo.ChatMessage;
import com.kradac.lojagasdistribuidor.Modelo.Mensaje;
import com.kradac.lojagasdistribuidor.Modelo.Solicitud;
import com.kradac.lojagasdistribuidor.Modelo.Usuario;
import com.kradac.lojagasdistribuidor.Presenter.PresenterCambioClave;
import com.kradac.lojagasdistribuidor.Presenter.PresenterSugerencia;
import com.kradac.lojagasdistribuidor.R;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionCambioClave;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionMainActivity;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionSugerencia;
import com.kradac.lojagasdistribuidor.Response.ResponseApi;
import com.kradac.lojagasdistribuidor.Servicio.Servicio;
import com.kradac.lojagasdistribuidor.Util.ConnectivityReceiver;
import com.kradac.lojagasdistribuidor.Util.CustomEventMapView;
import com.kradac.lojagasdistribuidor.Util.Funciones;
import com.kradac.lojagasdistribuidor.Util.Gps;
import com.kradac.lojagasdistribuidor.Util.LatLngGoogleEvaluator;
import com.kradac.lojagasdistribuidor.Util.LojagasAplicacion;
import com.kradac.lojagasdistribuidor.Util.ReproducirTextAudio;
import com.kradac.lojagasdistribuidor.Util.VariablesGlobales;
import com.mapbox.services.Constants;
import com.mapbox.services.api.ServicesException;
import com.mapbox.services.api.directions.v5.DirectionsCriteria;
import com.mapbox.services.api.directions.v5.MapboxDirections;
import com.mapbox.services.api.directions.v5.models.DirectionsResponse;
import com.mapbox.services.api.directions.v5.models.DirectionsRoute;
import com.mapbox.services.commons.geojson.LineString;
import com.mapbox.services.commons.models.Position;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kradac.lojagasdistribuidor.Util.Gps.REQUEST_CHECK_SETTINGS;


public class MainActivity extends Funciones
        implements NavigationView.OnNavigationItemSelectedListener,
        ConnectivityReceiver.ConnectivityReceiverListener, OnComunicacionMainActivity,
        OnComunicacionCambioClave, OnComunicacionSugerencia {

    @BindView(R.id.main_content)
    CoordinatorLayout mainContent;
    @BindView(R.id.mapView)
    CustomEventMapView mapView;
    @BindView(R.id.txt_placa)
    TextView txtPlaca;
    @BindView(R.id.txt_version)
    TextView txtVersion;
    @BindView(R.id.img_ubicacion)
    ImageView imgUbicacion;
    @BindView(R.id.img_lista_solicitudes)
    ImageView imgListaSolicitudes;
    @BindView(R.id.img_red)
    ImageView imgRed;
    @BindView(R.id.txt_usuario)
    TextView txtUsuario;
    @BindView(R.id.txt_barrio)
    TextView txtBarrio;
    @BindView(R.id.txt_calle_principal)
    TextView txtCallePrincipal;
    @BindView(R.id.txt_calle_secundaria)
    TextView txtCalleSecundaria;
    @BindView(R.id.cont_info)
    LinearLayout contInfo;
    @BindView(R.id.img_more)
    ImageView imgMore;
    @BindView(R.id.cont_more)
    LinearLayout contMore;
    @BindView(R.id.img_lugar)
    ImageView imgLugar;
    @BindView(R.id.img_llamar)
    ImageView imgLlamar;
    @BindView(R.id.img_entregar)
    ImageView imgEntregar;
    @BindView(R.id.img_chat)
    ImageView imgChat;
    @BindView(R.id.txt_detalle_solicitud)
    TextView txtDetalleSolicitud;
    @BindView(R.id.btn_cancelar_entrega)
    Button btnCancelarEntrega;
    @BindView(R.id.cont_secundaria)
    LinearLayout contSecundaria;
    @BindView(R.id.cont_chat)
    LinearLayout contChat;
    @BindView(R.id.recycler_pedido)
    RecyclerView recyclerPedido;
    @BindView(R.id.cont_caracteristicas)
    LinearLayout contCaracteristicas;
    @BindView(R.id.txt_referencia)
    TextView txtReferencia;
    @BindView(R.id.cont_referencia)
    LinearLayout contReferencia;
    @BindView(R.id.img_ubicacion_seguimiento)
    ImageView imgUbicacionSeguimiento;
    @BindView(R.id.img_info)
    ImageView imgInfo;
    @BindView(R.id.cont_user)
    LinearLayout contUser;
    @BindView(R.id.txt_cronometro)
    TextView txtCronometro;
    @BindView(R.id.txt_idSolicitud)
    TextView txtIdSolicitud;
    private Servicio conexionServer;
    private boolean mBound, verf;
    private AlertDialog alertDialog, alertDialogPedidoCancelado,
            alertDialogUsuarioPedidoPendienteEntregado, alertDialogChatPedidoPendiente1,
            alertDialogChatOpciones, alertDialogChatPedidoPendiente2;
    private GoogleMap mapaGoogle;
    private double lt_mapa, lg_mapa;
    private Location locationConductor;
    protected Drawable drawable;
    private Marker marcadorConductor;
    private Window win;
    private Marker marcadoUsuario;
    private Polyline polyline;
    private Location miPosicion;
    private int idSolicitudActiva, userNameActiva;
    private String telefonoUsuario;
    private static final int MY_PERMISSIONS_REQUEST_CALL = 3;
    private Preferencia preferencia;
    private ReproducirTextAudio reproducirTextAudio;
    private int contAudio = 0;
    protected PowerManager.WakeLock wakelock;
    private PresenterCambioClave presenterCambioClave;
    private PresenterSugerencia presenterSugerencia;
    private LatLng latLngTrazado;
    private int auxTrazado = 0;
    private AdaptadorListaCaracteristicas adaptadorListaCaracteristicas;
    private static final int MY_PERMISSIONS_REQUEST_CODE = 100;
    private boolean cerrarSesion;
    private Solicitud informacionSolicitud;
    private CountDownTimer countDownTimer;
    private String tiempoSeleccionado;
    private ChatAdapter adapter;
    private boolean isAudio = true;
    private List<ChatMessage> chatMessageList1, chatMessageList2;
    private int idSolicitudChatPedido1, idSolicitudChatPedido2;


    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Servicio.LocalBinder binder = (Servicio.LocalBinder) service;
            conexionServer = binder.getService();
            conexionServer.registrarUsuario(MainActivity.this, "MainActivity");
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };
    private CountDownTimer countDownTimer1;

    @SuppressLint("InvalidWakeLockTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        final Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        this.wakelock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "INFO");
        wakelock.acquire();

        Intent intent = new Intent(this, Servicio.class);
        startService(intent);

        preferencia = new Preferencia();
        reproducirTextAudio = new ReproducirTextAudio(MainActivity.this);
        presenterCambioClave = new PresenterCambioClave(this);
        presenterSugerencia = new PresenterSugerencia(getApplicationContext(), this);
        chatMessageList1 = new ArrayList<>();
        chatMessageList2 = new ArrayList<>();

        mapView.onCreate(savedInstanceState);

        verificarPermisosGps();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txtPlaca.setText(obtenerDatosUsuario(getApplicationContext()).getPlacaVehiculo());
        txtVersion.setText("V. " + obtenerVersion(getApplicationContext()));

        if (obtenerSeguimiento(getApplicationContext()).isSeguimiento()) {
            imgUbicacionSeguimiento.setVisibility(View.VISIBLE);
            imgUbicacion.setVisibility(View.GONE);
        } else {
            imgUbicacionSeguimiento.setVisibility(View.GONE);
            imgUbicacion.setVisibility(View.VISIBLE);
        }

        imgUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (obtenerSeguimiento(getApplicationContext()).isSeguimiento()) {
                    preferencia.setSeguimiento(false);
                    imgUbicacionSeguimiento.setVisibility(View.GONE);
                    imgUbicacion.setVisibility(View.VISIBLE);
                } else {
                    preferencia.setSeguimiento(true);
                    imgUbicacionSeguimiento.setVisibility(View.VISIBLE);
                    imgUbicacion.setVisibility(View.GONE);
                }
                guardarSeguimiento(preferencia, getApplicationContext());

                verificarPermisosGps();
            }
        });

        imgInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contInfo.getVisibility() == View.VISIBLE) {
                    contInfo.setVisibility(View.GONE);
                }
                alertDialog = informacionSolicitud();
                alertDialog.show();
            }
        });

        imgListaSolicitudes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ListaSolicitudes.class));
            }
        });

        imgMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contInfo.getVisibility() == View.VISIBLE) {
                    contInfo.setVisibility(View.GONE);
                    mapaGoogle.getUiSettings().setZoomGesturesEnabled(true);
                    mapaGoogle.getUiSettings().setScrollGesturesEnabled(true);
                } else {
                    contInfo.setVisibility(View.VISIBLE);
                    mapaGoogle.getUiSettings().setZoomGesturesEnabled(false);
                    mapaGoogle.getUiSettings().setScrollGesturesEnabled(false);
                }

            }
        });


        imgLugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conexionServer.enviarValidacionEnLugarCallCenter(informacionSolicitud.getDatosSolicitud().getIdSolicitudCc(), 5);
                conexionServer.avisoEnLugar(idSolicitudActiva, userNameActiva, "Estimado usuario me encuentro en el lugar");
            }
        });

        imgLlamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_CALL,
                        Uri.parse("tel:" + telefonoUsuario));
                if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL);
                    return;
                } else {
                    startActivity(i);
                }
            }
        });

        imgEntregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Aviso")
                        .setMessage("¿Está seguro que el pedido ha sido entregado?")
                        .setCancelable(false)
                        .setPositiveButton("CONFIRMAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if (idSolicitudActiva <= 0) {
                                    conexionServer.enviarValidacionEntregadoCallCenter(informacionSolicitud.getDatosSolicitud().getIdSolicitudCc(), 2);
                                } else {
                                    conexionServer.avisoEntregarGas(idSolicitudActiva, userNameActiva);
                                }

                            }
                        })
                        .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (alertDialog != null) {
                                    alertDialog.dismiss();
                                }
                            }
                        }).show();

            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        TextView txt_nombre_conductor = (TextView) headerView.findViewById(R.id.txt_nombre_conductor);
        TextView txt_correo_conductor = (TextView) headerView.findViewById(R.id.txt_correo);
        final CircleImageView img_conductor = (CircleImageView) headerView.findViewById(R.id.img_conductor);

        txt_nombre_conductor.setText(obtenerDatosUsuario(getApplicationContext()).getNombres() + " " + obtenerDatosUsuario(getApplicationContext()).getApellidos());
        txt_correo_conductor.setText(obtenerDatosUsuario(getApplicationContext()).getCorreo());
        obtenerImagen(img_conductor, obtenerDatosUsuario(getApplicationContext()).getImagenConductor(), getApplicationContext());


        mapView.setOnTouchListerner(new CustomEventMapView.onTouchListerner() {
            @Override
            public void onTouch(MotionEvent ev) {
                switch (ev.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_UP:
                        break;
                    case MotionEvent.ACTION_DOWN:
                        break;
                }
            }
        });

        mapView.getMapAsync(new OnMapReadyCallback() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mapaGoogle = googleMap;
                LatLng sydney = new LatLng(-4.014765, -79.205271);
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                try {
                    boolean success = googleMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    MainActivity.this, R.raw.style_json));

                    if (!success) {
                        Log.e("style mapa", "Style parsing failed.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e("style map", "Can't find style. Error: ", e);
                }

                if (mapaGoogle != null) {
                    consultarSolicitud();
                }

                if (mapaGoogle != null) {
                    mapaGoogle.moveCamera(CameraUpdateFactory.zoomTo(18));
                    mapaGoogle.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                    mapaGoogle.getUiSettings().setZoomControlsEnabled(false);
                    mapaGoogle.getUiSettings().setZoomGesturesEnabled(true);
                    mapaGoogle.getUiSettings().setCompassEnabled(false);
                }

                mapaGoogle.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        mapaGoogle.getUiSettings().setZoomGesturesEnabled(true);
                        mapaGoogle.getUiSettings().setScrollGesturesEnabled(true);
                    }
                });

                mapaGoogle.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {

                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
                        lt_mapa = cameraPosition.target.latitude;
                        lg_mapa = cameraPosition.target.longitude;
                    }
                });


                mapaGoogle.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        boolean isConnected = ConnectivityReceiver.isConnected();
                        if (isConnected) {

                        }
                        return false;
                    }
                });

            }
        });

        mapView.setOnTouchListerner(new CustomEventMapView.onTouchListerner() {
            @Override
            public void onTouch(MotionEvent ev) {
                switch (ev.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_UP:
                        imgUbicacion.setVisibility(View.VISIBLE);
                        imgUbicacionSeguimiento.setVisibility(View.GONE);
                        preferencia.setSeguimiento(false);
                        guardarSeguimiento(preferencia, getApplicationContext());
                        break;
                    case MotionEvent.ACTION_DOWN:
                        break;
                }
            }
        });

        btnCancelarEntrega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog = dialogCancelacionEntrega();
                alertDialog.show();
            }
        });

        imgChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Chat.class);
                intent.putExtra("idSolicitud", idSolicitudActiva);
                intent.putExtra("userName", userNameActiva);
                startActivity(intent);
            }
        });

        if (!verf) {
            if (mConnection != null) {
                Intent intent2 = new Intent(this, Servicio.class);
                bindService(intent2, mConnection, Context.BIND_AUTO_CREATE);

            }
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_audio) {
            checkPermission();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_perfil) {
            alertDialog = dialogPerfil();
            alertDialog.show();
        } else if (id == R.id.nav_cambiar_contrasena) {
            alertDialog = dialogCambiarContrasena();
            alertDialog.show();

        } else if (id == R.id.nav_contacto) {
            alertDialog = dialogContacto();
            alertDialog.show();

        } else if (id == R.id.nav_sugerencia) {
            alertDialog = dialogSugerencia();
            alertDialog.show();

        } else if (id == R.id.nav_evaluar) {
            calificarApp(getApplicationContext());

        } else if (id == R.id.nav_historial_solicitudes) {
            startActivity(new Intent(MainActivity.this, HistorialSolicitudes.class));

        } else if (id == R.id.nav_cerrar) {
            List<Solicitud> solicitudList = obtenerSolicitud(getApplicationContext());
            for (Solicitud solicitud : solicitudList) {

                if (solicitud.getEstadoSolicitud() == 3) {
                    cerrarSesion = true;
                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                        v.vibrate(2000);
                        if (reproducirTextAudio != null) {
                            reproducirTextAudio.speak("Aun posee pedidos pendientes por realizar");
                        }

                    }
                }
            }

            if (obtenerEstadoActividadConductor(getApplicationContext()).isEstadoActividadConductor()) {
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {

                    v.vibrate(2000);
                    if (reproducirTextAudio != null) {
                        reproducirTextAudio.speak("Aun se encuentra entregando un pedido");
                    }
                }
            } else if (cerrarSesion) {
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                    v.vibrate(2000);
                    if (reproducirTextAudio != null) {
                        reproducirTextAudio.speak("Aun posee pedidos pendientes por realizar");
                    }

                }
            } else {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Aviso")
                        .setMessage("¿Está seguro que desea cerrar sesión de la aplicación?")
                        .setCancelable(false)
                        .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (conexionServer != null) {
                                    Intent intent = new Intent(MainActivity.this, Servicio.class);
                                    if (mBound) {
                                        conexionServer.cancelNotification(MainActivity.this, 1);
                                    }
                                    if (mConnection != null) {
                                        unbindService(mConnection);
                                        mConnection = null;
                                    }
                                    stopService(intent);
                                    conexionServer.cerrar();
                                    preferencia.setEstadoLogin(false);
                                    preferencia.setEstadoActividadConductor(false);
                                    clearSolicitud();
                                    guardarEstadoActividadConductor(preferencia, getApplicationContext());
                                    guardarEstadoLogin(preferencia, getApplicationContext());
                                    startActivity(new Intent(MainActivity.this, Login.class));

                                    finish();
                                }
                            }
                        })
                        .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();

            }

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void verificarPermisosGps() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                + ContextCompat.checkSelfPermission(
                MainActivity.this, Manifest.permission.READ_PHONE_STATE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_PHONE_STATE}, 1);
        } else {
            centrarUbicacionGps();
        }
    }

    private void centrarUbicacionGps() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                + ContextCompat.checkSelfPermission(
                MainActivity.this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_PHONE_STATE}, 1);
        } else {
            if (gps == null) {
                gps = new Gps(getApplicationContext(), locationListener);
            } else {
                locationConductor = gps.getLastLocation();
                if (locationConductor != null) {
                    LatLng latLng = new LatLng(locationConductor.getLatitude(), locationConductor.getLongitude());
                    lt_mapa = locationConductor.getLatitude();
                    lg_mapa = locationConductor.getLongitude();

                    if (mapaGoogle != null) {
                        if (obtenerSeguimiento(getApplicationContext()).isSeguimiento()) {

                            mapaGoogle.animateCamera(CameraUpdateFactory.newCameraPosition(
                                    new CameraPosition.Builder()
                                            .target(latLng)
                                            .zoom(18)
                                            .bearing(locationConductor.getBearing())
                                            .tilt(0)
                                            .build()));
                        }

                        if (marcadorConductor != null) {
                            marcadorConductor.remove();
                        }

                        marcadorConductor = mapaGoogle.addMarker(
                                new MarkerOptions()
                                        .position(new LatLng(lt_mapa, lg_mapa))
                                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.navegacion))
                                        .anchor(0.5f, 0.5f));
                        marcadorConductor.showInfoWindow();


                        if (obtenerEstadoActividadConductor(getApplicationContext()).isEstadoActividadConductor()) {
                            if (auxTrazado == 0) {
                                if (locationConductor != null && latLngTrazado != null) {
                                    getRoute(Position.fromCoordinates(locationConductor.getLongitude(), locationConductor.getLatitude()), Position.fromCoordinates(latLngTrazado.longitude, latLngTrazado.latitude));
                                    auxTrazado = auxTrazado + 1;
                                }

                            }
                        }

                    }
                }


            }


        }
    }

    Gps gps;

    Gps.LocationListener locationListener = new Gps.LocationListener() {
        @Override
        public void onLocationChange(Location location) {
            if (gps != null) {
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                miPosicion = gps.getLastLocation();
                lt_mapa = location.getLatitude();
                lg_mapa = location.getLongitude();

                if (!obtenerEstadoActividadConductor(getApplicationContext()).isEstadoActividadConductor()) {
                    if (polyline != null) {
                        polyline.remove();
                        polyline = null;
                    }
                }

                if (mapaGoogle != null) {

                    if (obtenerSeguimiento(getApplicationContext()).isSeguimiento()) {

                        mapaGoogle.animateCamera(CameraUpdateFactory.newCameraPosition(
                                new CameraPosition.Builder()
                                        .target(latLng)
                                        .zoom(18)
                                        .bearing(location.getBearing())
                                        .tilt(0)
                                        .build()));
                    }

                    if (marcadorConductor != null) {
                        marcadorConductor.remove();
                    }

                    marcadorConductor = mapaGoogle.addMarker(
                            new MarkerOptions()
                                    .position(new LatLng(lt_mapa, lg_mapa))
                                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.navegacion))
                                    .anchor(0.5f, 0.5f));
                    marcadorConductor.showInfoWindow();

                    animateMarkerGoogle(latLng);

                    if (obtenerEstadoActividadConductor(getApplicationContext()).isEstadoActividadConductor()) {
                        if (latLngTrazado != null) {
                            if (auxTrazado == 0) {
                                if (miPosicion != null && latLngTrazado != null) {
                                    getRoute(Position.fromCoordinates(miPosicion.getLongitude(), miPosicion.getLatitude()), Position.fromCoordinates(latLngTrazado.longitude, latLngTrazado.latitude));
                                    auxTrazado = auxTrazado + 1;
                                }

                            }
                        }

                    }


                }

                //gps.stopLocationUpdates();
                //gps.onstop();
                //gps = null;
            } else {
                verificarPermisosGps();
            }
        }

        @Override
        public void onGooglePlayServicesDisable(int status) {

        }

        @Override
        public void onGpsDisableNewApi(final Status status) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        status.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        public void onGpsDisable() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder alertDialog;
                    alertDialog = new AlertDialog.Builder(MainActivity.this);
                    alertDialog.setTitle(getString(R.string.msg_sericios_ubicacion_deshabilitados));
                    alertDialog.setMessage(getString(R.string.msg_activar_sericios_ubicacion));
                    alertDialog.setPositiveButton(getString(R.string.str_ajustes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent("android.settings.LOCATION_SOURCE_SETTINGS");
                            startActivity(intent);

                        }
                    });
                    alertDialog.setNegativeButton(getString(R.string.str_ignorar), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();

                        }
                    });
                    alertDialog.show();
                }
            });
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int
            resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        Toast.makeText(MainActivity.this, "GPS activado", Toast.LENGTH_LONG).show();
                        centrarUbicacionGps();
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        gps = null;
                        centrarUbicacionGps();
                    }
                    default: {
                        break;
                    }
                }
                break;
        }
    }


    private void animateMarkerGoogle(final LatLng toPosition) {

        ValueAnimator markerAnimator = ObjectAnimator.ofObject(marcadorConductor, "position",
                new LatLngGoogleEvaluator(),
                marcadorConductor.getPosition(), new LatLng(toPosition.latitude, toPosition.longitude));
        markerAnimator.setInterpolator(new LinearInterpolator());
        markerAnimator.setDuration(1000);
        markerAnimator.start();
    }


    @Override
    public void dispositivoNoAutorizado(final String mensaje, final String nombres) {
        if (mensaje != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    alertDialog = dialogDispositivoNoAutorizado(mensaje, nombres);
                    alertDialog.show();
                }
            });
        }
    }

    @Override
    public void actualizacionUsuario(final String datosUsuario) {
        if (datosUsuario != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject datos = null;
                    try {
                        datos = new JSONObject(datosUsuario);
                        Usuario usuario = new Usuario();
                        usuario.setBloqueado(datos.getJSONObject("usuario").getInt("bloqueado"));
                        usuario.setId_rol_usuario(datos.getJSONObject("usuario").getInt("id_rol_usuario"));
                        usuario.setRol(datos.getJSONObject("usuario").getInt("rol"));
                        usuario.setIdEquipo(datos.getJSONObject("usuario").getInt("idEquipo"));
                        usuario.setTipoVehiculo(datos.getJSONObject("usuario").getInt("tipoVehiculo"));
                        usuario.setIdCiudad(datos.getJSONObject("usuario").getInt("idCiudad"));
                        usuario.setIdEmpresa(datos.getJSONObject("usuario").getInt("idEmpresa"));
                        usuario.setAnioVehiculo(datos.getJSONObject("usuario").getInt("anioVehiculo"));
                        usuario.setUnidadVehiculo(datos.getJSONObject("usuario").getInt("unidadVehiculo"));
                        usuario.setIdVehiculo(datos.getJSONObject("usuario").getInt("idVehiculo"));
                        usuario.setHabilitado_ktaxidriver(datos.getJSONObject("usuario").getInt("habilitado_ktaxidriver"));
                        usuario.setIdUsuario(datos.getJSONObject("usuario").getInt("idUsuario"));
                        usuario.setMensaje_bloqueo_driver(datos.getJSONObject("usuario").getString("mensaje_bloqueo_driver"));
                        usuario.setUsuario(datos.getJSONObject("usuario").getString("usuario"));
                        usuario.setImagenConductor(datos.getJSONObject("usuario").getString("imagenConductor"));
                        usuario.setNombres(datos.getJSONObject("usuario").getString("nombres"));
                        usuario.setApellidos(datos.getJSONObject("usuario").getString("apellidos"));
                        usuario.setCorreo(datos.getJSONObject("usuario").getString("correo"));
                        usuario.setCelular(datos.getJSONObject("usuario").getString("celular"));
                        usuario.setCedula(datos.getJSONObject("usuario").getString("cedula"));
                        usuario.setEmpresa(datos.getJSONObject("usuario").getString("empresa"));
                        usuario.setPlacaVehiculo(datos.getJSONObject("usuario").getString("placaVehiculo"));
                        usuario.setRegMunVehiculo(datos.getJSONObject("usuario").getString("regMunVehiculo"));
                        usuario.setMarcaVehiculo(datos.getJSONObject("usuario").getString("marcaVehiculo"));
                        usuario.setModeloVehiculo(datos.getJSONObject("usuario").getString("modeloVehiculo"));
                        usuario.setCiudad(datos.getJSONObject("usuario").getString("ciudad"));

                        guardarDatosUsuario(usuario);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });


        }
    }

    @Override
    public void encenderPantalla() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, 4 * 60 * 60 * 1000);
                win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        });
    }

    @Override
    public void confirmacionPedidoEntregado() {
        runOnUiThread(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void run() {
                resetearSolicitud();
                alertDialog = dialogCalificacionCliente();
                alertDialog.show();
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                    v.vibrate(2000);
                    if (reproducirTextAudio != null) {
                        reproducirTextAudio.speak("El pedido ha sido entregado con exito.");
                    }
                }
            }
        });
    }

    @Override
    public void entregaGasPendiente(final int idSolicitud) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                List<Solicitud> solicitudList = obtenerSolicitud(getApplicationContext());
                for (int i = 0; i < solicitudList.size(); i++) {
                    if (solicitudList.get(i).getEstadoSolicitud() == 3 && solicitudList.get(i).getIdSolicitud() == idSolicitud) {
                        solicitudList.get(i).setEstadoSolicitud(4);
                    }
                }

                if (idSolicitudChatPedido1 == idSolicitud) {
                    idSolicitudChatPedido1 = 0;
                }

                if (idSolicitudChatPedido2 == idSolicitud) {
                    idSolicitudChatPedido2 = 0;
                }

                preferencia.setEstadoActividadConductor(false);
                guardarSolicitud(getApplicationContext(), solicitudList);

                alertDialog = dialogCalificacionClientePedidoPendienteEntregado(idSolicitud);
                alertDialog.show();
            }
        });
    }

    @Override
    public void avisoClientePedidoEntregado(final int idSolicitud) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (idSolicitudActiva == idSolicitud) {
                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                        v.vibrate(2000);
                        if (reproducirTextAudio != null) {
                            reproducirTextAudio.speak("El cliente informa que el producto ha sido entregado.");
                        }
                    }
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Aviso")
                            .setMessage("El cliente informa que el producto ha sido entregado. ¿Estó es correcto?")
                            .setCancelable(false)
                            .setPositiveButton("CORRECTO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    conexionServer.avisoEntregarGas(idSolicitud, userNameActiva);
                                }
                            })
                            .setNegativeButton("NO ES CORRECTO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                } else {
                    final List<Solicitud> solicitudList = obtenerSolicitud(getApplicationContext());
                    for (int i = 0; i < solicitudList.size(); i++) {
                        if (solicitudList.get(i).getEstadoSolicitud() == 3 && solicitudList.get(i).getIdSolicitud() == idSolicitud
                                || solicitudList.get(i).getEstadoSolicitud() == 3 && solicitudList.get(i).getIdSolicitud() == (idSolicitud * -1)) {
                            final int solicitudPendiente = solicitudList.get(i).getIdSolicitud();
                            final int userName = solicitudList.get(i).getUsername();
                            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                            if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                                v.vibrate(2000);
                                if (reproducirTextAudio != null) {
                                    reproducirTextAudio.speak("El cliente informa que el producto ha sido entregado.");
                                }
                            }

                            if (alertDialogUsuarioPedidoPendienteEntregado != null) {
                                alertDialogUsuarioPedidoPendienteEntregado.dismiss();
                            }
                            alertDialogUsuarioPedidoPendienteEntregado = dialogUsuarioPedidoPendienteEntrego(solicitudPendiente, userName);
                            alertDialogUsuarioPedidoPendienteEntregado.show();

                        }
                    }
                }

            }
        });

    }

    @Override
    public void calificacionCliente(final String mensaje) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ocultarProgressDialog();
                if (alertDialog != null) {
                    alertDialog.dismiss();

                }
                if (alertDialogPedidoCancelado != null) {
                    alertDialogPedidoCancelado.dismiss();

                }

            }
        });


    }

    @Override
    public void cancelacionPedido() {
        runOnUiThread(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void run() {
                ocultarProgressDialog();
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                    v.vibrate(2000);
                    if (reproducirTextAudio != null) {
                        reproducirTextAudio.speak("El pedido ha sido cancelado y notificado al cliente.");
                    }
                }
                resetearSolicitud();
            }
        });


    }

    @Override
    public void solicitudAsignadaCallCenter() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    @Override
    public void mensajesUsuario(ArrayList<ChatMessage> chatHistory) {
        Intent intent = new Intent(MainActivity.this, Chat.class);
        intent.putExtra("idSolicitud", idSolicitudActiva);
        intent.putExtra("userName", userNameActiva);
        intent.putExtra("HistorialChat", chatHistory);
        startActivity(intent);


    }


    @Override
    public void clienteCanceloPedido(final int idSolicitud) {
        runOnUiThread(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void run() {
                List<Solicitud> solicitudList = obtenerSolicitud(getApplicationContext());
                for (int i = 0; i < solicitudList.size(); i++) {
                    if (solicitudList.get(i).getEstadoSolicitud() == 2 && solicitudList.get(i).getIdSolicitud() == idSolicitud
                            || solicitudList.get(i).getEstadoSolicitud() == 2 && solicitudList.get(i).getIdSolicitud() == (idSolicitud * -1)) {
                        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                        if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                            v.vibrate(2000);
                            if (reproducirTextAudio != null) {
                                reproducirTextAudio.speak("El cliente canceló la solicitud");
                            }
                        }
                        new android.support.v7.app.AlertDialog.Builder(MainActivity.this)
                                .setTitle("Aviso")
                                .setMessage("El cliente ha cancelado la entrega")
                                .setCancelable(false)
                                .setPositiveButton("CALIFICAR CLIENTE", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        List<Solicitud> solicitudList = obtenerSolicitud(getApplicationContext());
                                        for (int i = 0; i < solicitudList.size(); i++) {
                                            if (solicitudList.get(i).getIdSolicitud() == idSolicitud) {
                                                solicitudList.get(i).setEstadoSolicitud(4);
                                            }
                                        }

                                        preferencia.setEstadoActividadConductor(true);
                                        guardarEstadoActividadConductor(preferencia, getApplicationContext());
                                        guardarSolicitud(getApplicationContext(), solicitudList);
                                        dialog.dismiss();

                                        if (alertDialog != null) {
                                            alertDialog.dismiss();
                                        }

                                        alertDialog = dialogCalificacionCliente();
                                        alertDialog.show();

                                        resetearSolicitud();
                                    }
                                }).show();
                    } else if (solicitudList.get(i).getEstadoSolicitud() == 3 && solicitudList.get(i).getIdSolicitud() == idSolicitud) {
                        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                        v.vibrate(2000);
                        if (reproducirTextAudio != null) {
                            reproducirTextAudio.speak("El cliente ha cancelado un pedido pendiente.");
                        }

                        if (idSolicitudChatPedido1 == idSolicitud) {
                            idSolicitudChatPedido1 = 0;
                        }

                        if (idSolicitudChatPedido2 == idSolicitud) {
                            idSolicitudChatPedido2 = 0;
                        }

                        solicitudList.get(i).setEstadoSolicitud(4);
                        guardarSolicitud(getApplicationContext(), solicitudList);

                        if (alertDialog != null) {
                            alertDialog.dismiss();
                        }
                        alertDialog = dialogCancelacionPedidoPendiente(solicitudList.get(i));
                        alertDialog.show();
                        actualizarNotificacion();


                    } else if (solicitudList.get(i).getEstadoSolicitud() == 1 && solicitudList.get(i).getIdSolicitud() == idSolicitud) {
                        solicitudList.get(i).setEstadoSolicitud(4);
                        guardarSolicitud(getApplicationContext(), solicitudList);
                        actualizarNotificacion();


                    }
                }

            }
        });
    }

    @Override
    public void clienteCanceloPedidoPendiente(int idSolicitud) {
        if (idSolicitud != 0) {
            List<Solicitud> solicitudList = obtenerSolicitud(getApplicationContext());
            for (Solicitud solicitud : solicitudList) {
                if (solicitud.getIdSolicitud() == idSolicitud && solicitud.getEstadoSolicitud() == 3) {
                    Vibrator v1 = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                        v1.vibrate(2000);
                        if (reproducirTextAudio != null) {
                            reproducirTextAudio.speak("Un pedido pendiente fue cancelado por el cliente.");
                        }

                        if (idSolicitudChatPedido1 == idSolicitud) {
                            idSolicitudChatPedido1 = 0;
                        }

                        if (idSolicitudChatPedido2 == idSolicitud) {
                            idSolicitudChatPedido2 = 0;
                        }


                    }
                }
            }

        }
    }

    @Override
    public void cerrarSesion(final String nombre, final String mensaje) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Aviso " + nombre)
                        .setMessage(mensaje)
                        .setCancelable(false)
                        .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (conexionServer != null) {
                                    Intent intent = new Intent(MainActivity.this, Servicio.class);
                                    if (mBound) {
                                        conexionServer.cancelNotification(MainActivity.this, 1);
                                    }
                                    if (mConnection != null) {
                                        unbindService(mConnection);
                                        mConnection = null;
                                    }
                                    stopService(intent);
                                    conexionServer.cerrar();
                                    preferencia.setEstadoLogin(false);
                                    preferencia.setEstadoActividadConductor(false);
                                    clearSolicitud();
                                    guardarEstadoActividadConductor(preferencia, getApplicationContext());
                                    guardarEstadoLogin(preferencia, getApplicationContext());
                                    startActivity(new Intent(MainActivity.this, Login.class));

                                    finish();
                                }
                            }
                        }).show();
            }
        });

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void resetearSolicitud() {
        if (marcadoUsuario != null) {
            marcadoUsuario.remove();
        }

        imgMore.setVisibility(View.GONE);
        txtDetalleSolicitud.setVisibility(View.GONE);
        contInfo.setVisibility(View.GONE);
        contUser.setVisibility(View.GONE);

        txtCronometro.setVisibility(View.GONE);

        List<Solicitud> solicitudList = obtenerSolicitud(getApplicationContext());
        for (int i = 0; i < solicitudList.size(); i++) {
            if (solicitudList.get(i).getEstadoSolicitud() == 2) {
                solicitudList.get(i).setEstadoSolicitud(4);
            }
        }

        if (polyline != null) {
            polyline.remove();
            polyline = null;
        }

        conexionServer.borrarMensajeChat();
        idSolicitudActiva = 0;
        userNameActiva = 0;
        telefonoUsuario = null;
        auxTrazado = 0;
        preferencia.setEstadoActividadConductor(false);
        guardarEstadoActividadConductor(preferencia, getApplicationContext());
        guardarSolicitud(getApplicationContext(), solicitudList);
    }

    public AlertDialog dialogSugerencia() {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_sugerencia, null);
        Button btnEnviar = v.findViewById(R.id.btn_enviar);
        Button btnCancelar = v.findViewById(R.id.btn_cancelar);
        final EditText editMotivo = v.findViewById(R.id.edit_motivo);
        final EditText editSugerencia = v.findViewById(R.id.edit_sugerencia);

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
            }
        });


        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editMotivo.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Ingrese el motivo", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (editSugerencia.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Ingrese la sugerencia", Toast.LENGTH_SHORT).show();
                    return;
                }
                mostrarProgressDialog(MainActivity.this, "Procesando. Espere por favor");
                presenterSugerencia.enviarSugerencia(3, obtenerDatosUsuario(getApplicationContext()).getIdUsuario(),
                        idSolicitudActiva, obtenerDatosUsuario(getApplicationContext()).getNombres() + " " + obtenerDatosUsuario(getApplicationContext()).getApellidos(),
                        obtenerDatosUsuario(getApplicationContext()).getCorreo(), obtenerDatosUsuario(getApplicationContext()).getCedula(),
                        editMotivo.getText().toString().trim(), editSugerencia.getText().toString().trim(),
                        lt_mapa, lg_mapa, "");

            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }

    public AlertDialog dialogContacto() {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_contacto, null);
        Button btnCancelar = v.findViewById(R.id.btn_cancelar);
        Button btnWhatsapp = v.findViewById(R.id.btn_whatsapp);
        Button btnLlamar = v.findViewById(R.id.btn_llamar);

        btnWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enviarWhatsapp(MainActivity.this, "0991503740");
            }
        });

        btnLlamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL);
                } else {
                    startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:+593991503740")));
                }
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }

    public AlertDialog dialogCambiarContrasena() {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_cambiar_contrasena, null);
        Button btnAceptar = v.findViewById(R.id.btn_aceptar);
        Button btnCancelar = v.findViewById(R.id.btn_cancelar);
        final EditText editContrasena = v.findViewById(R.id.edit_contrasena);
        final EditText editComprobarContrasena = v.findViewById(R.id.edit_comprobar_contrasena);
        CheckBox chPass1 = v.findViewById(R.id.ch_pass1);
        CheckBox chPass2 = v.findViewById(R.id.ch_pass2);

        chPass1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    editContrasena.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    int cursor = editContrasena.getText().length();
                    editContrasena.setSelection(cursor, cursor);
                } else {
                    editContrasena.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    int cursor = editContrasena.getText().length();
                    editContrasena.setSelection(cursor, cursor);
                }
            }
        });


        chPass2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    editComprobarContrasena.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    int cursor = editComprobarContrasena.getText().length();
                    editComprobarContrasena.setSelection(cursor, cursor);
                } else {
                    editComprobarContrasena.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    int cursor = editComprobarContrasena.getText().length();
                    editComprobarContrasena.setSelection(cursor, cursor);
                }
            }
        });

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editContrasena.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Ingrese la contraseña anterior", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (editComprobarContrasena.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Ingrese la nueva contraseña", Toast.LENGTH_SHORT).show();
                    return;
                }
                mostrarProgressDialog(MainActivity.this, "Procesando. Espere por favor...");

                presenterCambioClave.cambioContrasena(obtenerDatosUsuario(getApplicationContext()).getIdUsuario(),
                        MD5(editContrasena.getText().toString().trim()), MD5(editComprobarContrasena.getText().toString().trim()),
                        VariablesGlobales.NUM_ID_APLICATIVO);

            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }

    public AlertDialog dialogPerfil() {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_perfil, null);
        Button btnAceptar = v.findViewById(R.id.btn_aceptar);

        CircleImageView imgConductor = v.findViewById(R.id.img_conductor);
        TextView txtNombre = v.findViewById(R.id.txt_nombre);
        TextView txtCelular = v.findViewById(R.id.txt_celular);
        TextView txtCorreo = v.findViewById(R.id.txt_correo);
        TextView txtEmpresa = v.findViewById(R.id.txt_empresa);
        TextView txtPlaca = v.findViewById(R.id.txt_placa);
        TextView txtUnidad = v.findViewById(R.id.txt_unidad);
        TextView txtMarca = v.findViewById(R.id.txt_marca);
        TextView txtModelo = v.findViewById(R.id.txt_modelo);
        TextView txtAnio = v.findViewById(R.id.txt_anio);
        TextView txtCiudad = v.findViewById(R.id.txt_ciudad);

        obtenerImagen(imgConductor, obtenerDatosUsuario(getApplicationContext()).getImagenConductor(), getApplicationContext());
        txtNombre.setText(obtenerDatosUsuario(getApplicationContext()).getNombres() + " " + obtenerDatosUsuario(getApplicationContext()).getApellidos());
        txtCelular.setText(obtenerDatosUsuario(getApplicationContext()).getCelular());
        txtCorreo.setText(obtenerDatosUsuario(getApplicationContext()).getCorreo());
        txtEmpresa.setText(obtenerDatosUsuario(getApplicationContext()).getEmpresa());
        txtPlaca.setText(obtenerDatosUsuario(getApplicationContext()).getPlacaVehiculo());
        txtUnidad.setText(obtenerDatosUsuario(getApplicationContext()).getUnidadVehiculo() + "");
        txtMarca.setText(obtenerDatosUsuario(getApplicationContext()).getMarcaVehiculo());
        txtModelo.setText(obtenerDatosUsuario(getApplicationContext()).getModeloVehiculo());
        txtAnio.setText(obtenerDatosUsuario(getApplicationContext()).getAnioVehiculo() + "");
        txtCiudad.setText(obtenerDatosUsuario(getApplicationContext()).getCiudad());


        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }

    public AlertDialog informacionSolicitud() {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(true);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_informacion_solicitud, null);
        Button btnCerrar = v.findViewById(R.id.btn_cerrar);

        TextView txtNombreUsuario = v.findViewById(R.id.txt_usuario);
        TextView txtBarrio = v.findViewById(R.id.txt_barrio);
        TextView txtCallePrincipal = v.findViewById(R.id.txt_calle_principal);
        TextView txtCalleSecundaria = v.findViewById(R.id.txt_calle_secundaria);
        TextView txtReferencia = v.findViewById(R.id.txt_referencia);
        RecyclerView recyclerPedido = v.findViewById(R.id.recycler_pedido);
        LinearLayout contCaracteristicas = v.findViewById(R.id.cont_caracteristicas);
        LinearLayout contSecundaria = v.findViewById(R.id.cont_secundaria);
        LinearLayout contReferencia = v.findViewById(R.id.cont_referencia);

        txtNombreUsuario.setText(informacionSolicitud.getDatosSolicitud().getNombres());
        txtBarrio.setText(informacionSolicitud.getDatosSolicitud().getBarrioCliente());
        txtCallePrincipal.setText(informacionSolicitud.getDatosSolicitud().getCallePrincipal());

        LinearLayoutManager layoutManagerNueva = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerPedido.setNestedScrollingEnabled(false);
        recyclerPedido.setLayoutManager(layoutManagerNueva);


        if (informacionSolicitud.getlC() != null) {
            if (informacionSolicitud.getlC().size() > 0) {
                contCaracteristicas.setVisibility(View.VISIBLE);

                adaptadorListaCaracteristicas = new AdaptadorListaCaracteristicas(getApplicationContext(), informacionSolicitud.getlC(),
                        new AdaptadorListaCaracteristicas.OnClicklistener() {
                            @Override
                            public void onClic(int posicion) {

                            }
                        });

                recyclerPedido.setAdapter(adaptadorListaCaracteristicas);
            } else {
                contCaracteristicas.setVisibility(View.GONE);
            }

        } else {
            contCaracteristicas.setVisibility(View.GONE);
        }

        if (informacionSolicitud.getDatosSolicitud().getCalleSecundaria() == null || informacionSolicitud.getDatosSolicitud().getCalleSecundaria().isEmpty()) {
            contSecundaria.setVisibility(View.GONE);
        } else {
            contSecundaria.setVisibility(View.VISIBLE);
            txtCalleSecundaria.setText(informacionSolicitud.getDatosSolicitud().getCalleSecundaria());
        }

        if (informacionSolicitud.getDatosSolicitud().getReferenciaCliente() == null || informacionSolicitud.getDatosSolicitud().getReferenciaCliente().isEmpty()) {
            contReferencia.setVisibility(View.GONE);
        } else {
            contReferencia.setVisibility(View.VISIBLE);
            txtReferencia.setText(informacionSolicitud.getDatosSolicitud().getReferenciaCliente());
        }


        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }


    public AlertDialog dialogCalificacionCliente() {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_calificar_cliente, null);
        Button btnAceptar = v.findViewById(R.id.btn_enviar);
        final RatingBar rtCalificacion = v.findViewById(R.id.ratitngCalificacion);
        final EditText editComentario = v.findViewById(R.id.edit_calificacion);


        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarProgressDialog(MainActivity.this, "Enviando calificación, Espere por favor.");
                conexionServer.calificacionCliente(lt_mapa, lg_mapa, (int) rtCalificacion.getRating(),
                        editComentario.getText().toString().trim(), idSolicitudActiva);

                List<Solicitud> solicitudList = obtenerSolicitud(getApplicationContext());
                for (int i = 0; i < solicitudList.size(); i++) {
                    if (solicitudList.get(i).getEstadoSolicitud() == 3) {
                        contAudio = contAudio + 1;
                        if (contAudio == 1) {
                            Vibrator v1 = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                            if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                                if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                                    v1.vibrate(2000);
                                    if (reproducirTextAudio != null) {
                                        reproducirTextAudio.speak("Aun posee pedidos pendientes por realizar.");
                                    }
                                }

                            }
                        }
                    }
                    contAudio = 0;
                }


            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }


    public AlertDialog dialogCalificacionClientePedidoPendienteEntregado(final int idSolicitud) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_calificar_cliente, null);
        Button btnAceptar = v.findViewById(R.id.btn_enviar);
        final RatingBar rtCalificacion = v.findViewById(R.id.ratitngCalificacion);
        final EditText editComentario = v.findViewById(R.id.edit_calificacion);


        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarProgressDialog(MainActivity.this, "Enviando calificación, Espere por favor.");
                conexionServer.calificacionCliente(lt_mapa, lg_mapa, (int) rtCalificacion.getRating(),
                        editComentario.getText().toString().trim(), idSolicitud);

                List<Solicitud> solicitudList = obtenerSolicitud(getApplicationContext());
                for (int i = 0; i < solicitudList.size(); i++) {
                    if (solicitudList.get(i).getEstadoSolicitud() == 3) {
                        contAudio = contAudio + 1;
                        if (contAudio == 1) {
                            Vibrator v1 = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                            if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                                if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                                    v1.vibrate(2000);
                                    if (reproducirTextAudio != null) {
                                        reproducirTextAudio.speak("Aun posee pedidos pendientes por realizar.");
                                    }
                                }

                            }
                        }
                    }
                    contAudio = 0;
                }


                int numeroSolicitudes = obtenerNumeroSolicitudesPendientes(getApplicationContext()).getNumeroSolicitudes() - 1;

                preferencia.setNumeroSolicitudes(numeroSolicitudes);
                guardarNumeroSolicitudesPendientes(preferencia, getApplicationContext());
                actualizarNotificacion();


            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }


    public AlertDialog dialogSalirAplicacion() {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(true);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_salir_aplicacion, null);

        TextView txtSalir = v.findViewById(R.id.txt_salir);
        TextView txtMinimizar = v.findViewById(R.id.txt_minimizar);
        TextView txtCerrar = v.findViewById(R.id.txt_cerrar);

        txtSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (obtenerEstadoActividadConductor(getApplicationContext()).isEstadoActividadConductor()) {
                    Vibrator vi = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                        vi.vibrate(2000);
                        if (reproducirTextAudio != null) {
                            reproducirTextAudio.speak("Aun se encuentra entregando un pedido");
                        }
                    }
                } else if (obtenerNumeroSolicitudesPendientes(getApplicationContext()).getNumeroSolicitudes() > 0) {
                    cerrarSesion = true;
                    Vibrator vi = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                        vi.vibrate(2000);
                        if (reproducirTextAudio != null) {
                            reproducirTextAudio.speak("Aun posee pedidos pendientes por realizar");
                        }

                    }
                } else {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Aviso")
                            .setMessage("¿Está seguro que desea salir de la aplicación?. No recibirá solicitudes")
                            .setCancelable(false)
                            .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (conexionServer != null) {
                                        Intent intent = new Intent(MainActivity.this, Servicio.class);
                                        if (mBound) {
                                            conexionServer.cancelNotification(MainActivity.this, 1);
                                        }
                                        if (mConnection != null) {
                                            unbindService(mConnection);
                                            mConnection = null;
                                        }
                                        stopService(intent);
                                        conexionServer.cerrar();
                                        finish();
                                    }
                                }
                            })
                            .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                }

            }
        });

        txtMinimizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        txtCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (obtenerNumeroSolicitudesPendientes(getApplicationContext()).getNumeroSolicitudes() > 0) {
                    cerrarSesion = true;
                    Vibrator vi = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                        vi.vibrate(2000);
                        if (reproducirTextAudio != null) {
                            reproducirTextAudio.speak("Aun posee pedidos pendientes por realizar");
                        }

                    }
                } else if (obtenerEstadoActividadConductor(getApplicationContext()).isEstadoActividadConductor()) {
                    Vibrator vi = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                        vi.vibrate(2000);
                        if (reproducirTextAudio != null) {
                            reproducirTextAudio.speak("Aun se encuentra entregando un pedido");
                        }
                    }
                } else {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Aviso")
                            .setMessage("¿Está seguro que desea salir de la aplicación?")
                            .setCancelable(false)
                            .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (conexionServer != null) {
                                        Intent intent = new Intent(MainActivity.this, Servicio.class);
                                        if (mBound) {
                                            conexionServer.cancelNotification(MainActivity.this, 1);
                                        }
                                        if (mConnection != null) {
                                            unbindService(mConnection);
                                            mConnection = null;
                                        }
                                        stopService(intent);
                                        conexionServer.cerrar();
                                        clearSolicitud();
                                        clearDatosUsuario();
                                        clearEstadoActividadConductor();
                                        clearEstadoLogin();
                                        clearIdSolicitudActiva();
                                        clearNumeroSolicitudesPendiente();
                                        clearSeguimiento();
                                        startActivity(new Intent(MainActivity.this, Login.class));
                                        finish();
                                    }
                                }
                            })
                            .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();

                }
            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }

    public AlertDialog dialogCalificacionPedidoCancelado(final int idSolicitud) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_calificar_cliente, null);
        Button btnAceptar = v.findViewById(R.id.btn_enviar);
        final RatingBar rtCalificacion = v.findViewById(R.id.ratitngCalificacion);
        final EditText editComentario = v.findViewById(R.id.edit_calificacion);


        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarProgressDialog(MainActivity.this, "Enviando calificación, Espere por favor.");
                conexionServer.calificacionCliente(lt_mapa, lg_mapa, (int) rtCalificacion.getRating(),
                        editComentario.getText().toString().trim(), idSolicitud);

                List<Solicitud> solicitudList = obtenerSolicitud(getApplicationContext());
                for (int i = 0; i < solicitudList.size(); i++) {
                    if (solicitudList.get(i).getEstadoSolicitud() == 3) {
                        contAudio = contAudio + 1;
                        if (contAudio == 1) {
                            Vibrator v1 = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                            if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                                if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                                    v1.vibrate(2000);
                                    if (reproducirTextAudio != null) {
                                        reproducirTextAudio.speak("Aun posee pedidos pendientes por realizar.");
                                    }
                                }

                            }
                        }
                    }
                    contAudio = 0;
                }


            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }

    public AlertDialog dialogCancelacionPedidoPendiente(final Solicitud solicitud) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_cancelacion_pedido_pediente, null);
        Button btnAceptar = v.findViewById(R.id.btn_enviar);
        TextView txtCallePrincipal = v.findViewById(R.id.txt_calle_principal);
        TextView txtCalleSecundaria = v.findViewById(R.id.txt_calle_secundaria);
        TextView lblCalleSecundaria = v.findViewById(R.id.lbl_calle_secundaria);
        TextView txtUsuario = v.findViewById(R.id.txt_usuario);
        TextView txtBarrio = v.findViewById(R.id.txt_barrio);

        txtBarrio.setText(solicitud.getDatosSolicitud().getBarrioCliente());
        txtCallePrincipal.setText(solicitud.getDatosSolicitud().getCallePrincipal());

        if (solicitud.getDatosSolicitud().getCalleSecundaria() != null &&
                !solicitud.getDatosSolicitud().getCalleSecundaria().isEmpty()) {
            txtCalleSecundaria.setText(solicitud.getDatosSolicitud().getCalleSecundaria());

        } else {
            txtCalleSecundaria.setVisibility(View.GONE);
            lblCalleSecundaria.setVisibility(View.GONE);
        }

        txtUsuario.setText(solicitud.getDatosSolicitud().getNombres());

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialogPedidoCancelado != null) {
                    alertDialogPedidoCancelado.dismiss();
                }

                int numeroSolicitudes = obtenerNumeroSolicitudesPendientes(getApplicationContext()).getNumeroSolicitudes() - 1;

                preferencia.setNumeroSolicitudes(numeroSolicitudes);
                guardarNumeroSolicitudesPendientes(preferencia, getApplicationContext());

                alertDialogPedidoCancelado = dialogCalificacionPedidoCancelado(solicitud.getIdSolicitud());
                alertDialogPedidoCancelado.show();
            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }

    public AlertDialog dialogUsuarioPedidoPendienteEntrego(final int idSolicitud, final int userName) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_pedido_pendiente_entregado, null);
        Button btnAceptar = v.findViewById(R.id.btn_enviar);
        Button btnCancelar = v.findViewById(R.id.btn_cancelar);
        TextView txtCallePrincipal = v.findViewById(R.id.txt_calle_principal);
        TextView txtCalleSecundaria = v.findViewById(R.id.txt_calle_secundaria);
        TextView lblCalleSecundaria = v.findViewById(R.id.lbl_calle_secundaria);
        TextView txtUsuario = v.findViewById(R.id.txt_usuario);
        TextView txtBarrio = v.findViewById(R.id.txt_barrio);

        List<Solicitud> listSolicitud = obtenerSolicitud(getApplicationContext());

        for (int i = 0; i < listSolicitud.size(); i++) {
            if (listSolicitud.get(i).getEstadoSolicitud() == 3 && listSolicitud.get(i).getIdSolicitud() == idSolicitud
                    || listSolicitud.get(i).getEstadoSolicitud() == 3 && listSolicitud.get(i).getIdSolicitud() == (idSolicitud * -1)) {

                txtBarrio.setText(listSolicitud.get(i).getDatosSolicitud().getBarrioCliente());
                txtCallePrincipal.setText(listSolicitud.get(i).getDatosSolicitud().getCallePrincipal());

                if (listSolicitud.get(i).getDatosSolicitud().getCalleSecundaria() != null &&
                        !listSolicitud.get(i).getDatosSolicitud().getCalleSecundaria().isEmpty()) {
                    txtCalleSecundaria.setText(listSolicitud.get(i).getDatosSolicitud().getCalleSecundaria());

                } else {
                    txtCalleSecundaria.setVisibility(View.GONE);
                    lblCalleSecundaria.setVisibility(View.GONE);
                }

                txtUsuario.setText(listSolicitud.get(i).getDatosSolicitud().getNombres());

                break;
            }
        }


        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialogUsuarioPedidoPendienteEntregado != null) {
                    alertDialogUsuarioPedidoPendienteEntregado.dismiss();
                }
                conexionServer.avisoEntregarGasPendiente(idSolicitud, userName);

                if (idSolicitudChatPedido1 == idSolicitud) {
                    idSolicitudChatPedido1 = 0;
                }

                if (idSolicitudChatPedido2 == idSolicitud) {
                    idSolicitudChatPedido2 = 0;
                }
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialogUsuarioPedidoPendienteEntregado != null) {
                    alertDialogUsuarioPedidoPendienteEntregado.dismiss();
                }
            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }


    public AlertDialog dialogDispositivoNoAutorizado(String mensaje, String nombres) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_mensaje_confirmacion, null);
        TextView txtMensaje = v.findViewById(R.id.txt_mensaje);

        Button btnAceptar = v.findViewById(R.id.btn_aceptar);

        txtMensaje.setText(mensaje + " " + nombres);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                startActivity(new Intent(MainActivity.this, Login.class));
                clearDatosUsuario();
                clearEstadoLogin();
                if (conexionServer != null) {
                    Intent intent = new Intent(MainActivity.this, Servicio.class);
                    if (mBound) {
                        conexionServer.cancelNotification(MainActivity.this, 1);
                    }
                    if (mConnection != null) {
                        unbindService(mConnection);
                        mConnection = null;
                    }
                    stopService(intent);
                    finish();
                }
            }
        });


        localBuilder.setView(v);
        return localBuilder.create();
    }

    public AlertDialog dialogCancelacionEntrega() {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_cancelacion_entrega, null);
        Button btnAceptar = v.findViewById(R.id.btn_enviar);
        Button btnCancelar = v.findViewById(R.id.btn_cancelar);
        final CheckBox rbMotivo1 = v.findViewById(R.id.rb_cliente_se_ha_ido);
        final CheckBox rbMotivo2 = v.findViewById(R.id.rb_problemas_mecanicos);
        final CheckBox rbMotivo3 = v.findViewById(R.id.rb_congestion_vehicular);
        final RatingBar rtCalificacion = v.findViewById(R.id.ratitngCalificacion);
        final EditText editDescripcion = v.findViewById(R.id.edit_cancelacion);
        final LinearLayout contCalificacion = v.findViewById(R.id.cont_calificacion);

        rbMotivo1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rbMotivo2.setChecked(false);
                    contCalificacion.setVisibility(View.VISIBLE);
                    rbMotivo3.setChecked(false);
                }

            }
        });

        rbMotivo2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    contCalificacion.setVisibility(View.GONE);
                    rbMotivo1.setChecked(false);
                    rbMotivo3.setChecked(false);
                }
            }
        });

        rbMotivo3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    contCalificacion.setVisibility(View.GONE);
                    rbMotivo1.setChecked(false);
                    rbMotivo2.setChecked(false);
                }
            }
        });

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int eventualidad = 0;

                if (rbMotivo1.isChecked()) {
                    eventualidad = 6;
                }

                if (rbMotivo2.isChecked()) {
                    eventualidad = 8;
                }

                if (rbMotivo3.isChecked()) {
                    eventualidad = 4;
                }

                if (eventualidad == 0) {
                    Toast.makeText(MainActivity.this, "Selecciones el motivo de la cancelación", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (editDescripcion.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Ingrese el motivo de la cancelación", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (editDescripcion.getText().toString().trim().length() < 12) {
                    Toast.makeText(MainActivity.this, "El motivo debe contener más de 12 caracteres.", Toast.LENGTH_SHORT).show();
                    return;
                }

                mostrarProgressDialog(MainActivity.this, "Enviando cancelación, Espere por favor.");

                conexionServer.enviarValidacionCancelacionCallCenter(informacionSolicitud.getDatosSolicitud().getIdSolicitudCc(), 0);

                conexionServer.cancelarSolicitud(userNameActiva, lt_mapa, lg_mapa, 6, 7,
                        idSolicitudActiva, (int) rtCalificacion.getRating(),
                        editDescripcion.getText().toString().trim());


            }
        });
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        localBuilder.setView(v);
        return localBuilder.create();
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void consultarSolicitud() {
        List<Solicitud> list = new ArrayList<>();
        if (obtenerEstadoActividadConductor(getApplicationContext()).isEstadoActividadConductor()) {
            imgMore.setVisibility(View.VISIBLE);
            txtDetalleSolicitud.setVisibility(View.VISIBLE);
            contUser.setVisibility(View.VISIBLE);


        } else {
            imgMore.setVisibility(View.GONE);
            txtDetalleSolicitud.setVisibility(View.GONE);
            contUser.setVisibility(View.GONE);
            txtCronometro.setVisibility(View.GONE);
        }

        if (obtenerSolicitud(getApplicationContext()) != null) {
            list = obtenerSolicitud(getApplicationContext());
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getEstadoSolicitud() == 2) {

                    if (mapaGoogle != null) {

                        if (list.get(i).getIdSolicitud() <= 0) {
                            contChat.setVisibility(View.GONE);
                        } else if (list.get(i).getIdSolicitud() > 0 && list.get(i).getUsername() < 0) {
                            contChat.setVisibility(View.GONE);

                        } else {
                            contChat.setVisibility(View.VISIBLE);

                        }

                        informacionSolicitud = list.get(i);


                        if (informacionSolicitud.getIdSolicitud() <= 0) {
                            txtCronometro.setVisibility(View.GONE);
                        } else {
                            txtIdSolicitud.setText(" - " + informacionSolicitud.getIdSolicitud());
                            txtCronometro.setVisibility(View.VISIBLE);
                        }

                        if (informacionSolicitud.getTiempoAceptado() < 10) {
                            tiempoSeleccionado = "0" + informacionSolicitud.getTiempoAceptado() + ":00 / ";
                        } else {
                            tiempoSeleccionado = informacionSolicitud.getTiempoAceptado() + ":00 / ";
                        }

                        idSolicitudActiva = list.get(i).getIdSolicitud();

                        preferencia.setIdSolicitudActiva(list.get(i).getIdSolicitud());
                        guardarIdSolicitudActiva(preferencia, getApplicationContext());

                        userNameActiva = list.get(i).getUsername();
                        telefonoUsuario = list.get(i).getDatosSolicitud().getCelular();
                        txtUsuario.setText(list.get(i).getDatosSolicitud().getNombres());
                        txtBarrio.setText(list.get(i).getDatosSolicitud().getBarrioCliente());
                        txtCallePrincipal.setText(list.get(i).getDatosSolicitud().getCallePrincipal());

                        LinearLayoutManager layoutManagerNueva = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                        recyclerPedido.setNestedScrollingEnabled(false);
                        recyclerPedido.setLayoutManager(layoutManagerNueva);

                        if (list.get(i).getlC() != null) {
                            if (list.get(i).getlC().size() > 0) {
                                contCaracteristicas.setVisibility(View.VISIBLE);

                                adaptadorListaCaracteristicas = new AdaptadorListaCaracteristicas(getApplicationContext(), list.get(i).getlC(),
                                        new AdaptadorListaCaracteristicas.OnClicklistener() {
                                            @Override
                                            public void onClic(int posicion) {

                                            }
                                        });

                                recyclerPedido.setAdapter(adaptadorListaCaracteristicas);
                            } else {
                                contCaracteristicas.setVisibility(View.GONE);
                            }

                        } else {
                            contCaracteristicas.setVisibility(View.GONE);
                        }

                        if (list.get(i).getDatosSolicitud().getCalleSecundaria() == null || list.get(i).getDatosSolicitud().getCalleSecundaria().isEmpty()) {
                            contSecundaria.setVisibility(View.GONE);
                        } else {
                            contSecundaria.setVisibility(View.VISIBLE);
                            txtCalleSecundaria.setText(list.get(i).getDatosSolicitud().getCalleSecundaria());
                        }

                        if (list.get(i).getDatosSolicitud().getReferenciaCliente() == null || list.get(i).getDatosSolicitud().getReferenciaCliente().isEmpty()) {
                            contReferencia.setVisibility(View.GONE);
                        } else {
                            contReferencia.setVisibility(View.VISIBLE);
                            txtReferencia.setText(list.get(i).getDatosSolicitud().getReferenciaCliente());
                        }

                        LatLng latLng = new LatLng(list.get(i).getDatosSolicitud().getLatitud(), list.get(i).getDatosSolicitud().getLongitud());
                        latLngTrazado = new LatLng(list.get(i).getDatosSolicitud().getLatitud(), list.get(i).getDatosSolicitud().getLongitud());

                        if (mapaGoogle != null) {
                            if (marcadoUsuario != null) {
                                marcadoUsuario.remove();
                            }
                            marcadoUsuario = mapaGoogle.addMarker(
                                    new MarkerOptions()
                                            .position(latLng)
                                            .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_solicitud))
                                            .title("Usuario")
                                            .snippet(list.get(i).getDatosSolicitud().getNombres())
                                            .anchor(0.5f, 0.5f));

                            mapaGoogle.moveCamera(CameraUpdateFactory.zoomTo(18));


                        }

                    }
                } else if (list.get(i).getEstadoSolicitud() == 3) {
                    imgListaSolicitudes.setImageResource(R.mipmap.avisonotificacion);
                } else if (list.get(i).getEstadoSolicitud() == 1) {
                    imgListaSolicitudes.setImageResource(R.mipmap.solicitudactivo);
                } else {
                    imgListaSolicitudes.setImageResource(R.mipmap.solicitudinactivo);
                }
            }

            if (informacionSolicitud != null) {
                cancelarCronometro();
                if (informacionSolicitud.getTiempoEntrega() != null && countDownTimer == null) {
                    SimpleDateFormat f = new SimpleDateFormat("HH:mm");
                    String horaEntrega = informacionSolicitud.getTiempoEntrega();
                    String horaActual = f.format(new Date());

                    Date d1 = null, d2 = null;
                    try {
                        d1 = f.parse(horaEntrega);
                        d2 = f.parse(horaActual);
                        long milisegundosEntrega = d1.getTime();
                        long milisegundosActual = d2.getTime();

                        startTimer(milisegundosEntrega - milisegundosActual);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                }
            }
        }
    }

    private void getRoute(Position origin, Position destination) {

        MapboxDirections client;
        try {
            client = new MapboxDirections.Builder()
                    .setOrigin(origin)
                    .setDestination(destination)
                    .setProfile(DirectionsCriteria.PROFILE_DRIVING)
                    .setAccessToken(getString(R.string.access_token))
                    .build();
        } catch (ServicesException e) {
            e.printStackTrace();
            return;
        }

        client.enqueueCall(new Callback<DirectionsResponse>() {
            @Override
            public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                ocultarProgressDialog();
                if (response.body() == null || isFinishing()) {
                    return;
                }
                try {
                    List<DirectionsRoute> listRoute = response.body().getRoutes();
                    if (listRoute.size() > 0) {
                        DirectionsRoute currentRoute = listRoute.get(0);
                        drawRoute(currentRoute);
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<DirectionsResponse> call, Throwable t) {
                ocultarProgressDialog();
            }
        });
    }


    private void drawRoute(DirectionsRoute route) {
        PolylineOptions options = new PolylineOptions().width(10).
                color(Color.parseColor("#00793F")).geodesic(true);

        LineString lineString = LineString.fromPolyline(route.getGeometry(), Constants.PRECISION_6);
        List<Position> coordinates = lineString.getCoordinates();

        for (int i = 0; i < coordinates.size(); i++) {
            options.add(new LatLng(coordinates.get(i).getLatitude(),
                    coordinates.get(i).getLongitude()));
        }
        if (polyline != null) {
            polyline.remove();
            polyline = null;
        }

        polyline = mapaGoogle.addPolyline(options);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent i = new Intent(Intent.ACTION_CALL,
                            Uri.parse("tel:" + telefonoUsuario));
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    } else {
                        MainActivity.this.startActivity(i);
                    }

                } else {
                    Snackbar.make(mainContent, "Al no activar el permiso, no se podrá llamar al cliente.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_CODE: {
                if (((grantResults.length > 0) && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                        && ((grantResults.length > 0) && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                        && ((grantResults.length > 0) && grantResults[2] == PackageManager.PERMISSION_GRANTED)) {
                    checkConnection();
                } else {
                    checkPermission();
                }

            }

            break;

        }
    }


    public void actualizarNotificacion() {
        if (obtenerSolicitud(getApplicationContext()) != null) {
            List<Solicitud> list = obtenerSolicitud(getApplicationContext());
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getEstadoSolicitud() == 3) {
                    imgListaSolicitudes.setImageResource(R.mipmap.avisonotificacion);
                } else if (list.get(i).getEstadoSolicitud() == 1) {
                    imgListaSolicitudes.setImageResource(R.mipmap.solicitudactivo);
                } else {
                    imgListaSolicitudes.setImageResource(R.mipmap.solicitudinactivo);
                }
            }
        }
    }


    //Conexion server

    public void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (!isConnected) {
            message = "Se ha perdido la conexión";
            color = Color.GREEN;
            Snackbar snackbar = Snackbar.make(mainContent, message, Snackbar.LENGTH_INDEFINITE);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();

        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (!isConnected) {
            String message = "Se ha perdido la conexión";
            int color = Color.GREEN;
            Snackbar snackbar = Snackbar.make(mainContent, message, Snackbar.LENGTH_INDEFINITE);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();

        } else {
            String message = "Se ha establecido la conexión";
            int color = Color.WHITE;
            Snackbar snackbar = Snackbar.make(mainContent, message, Snackbar.LENGTH_LONG);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();

            Intent intent = new Intent(getApplicationContext(), Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }

    }


    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            Intent intentbind = new Intent(MainActivity.this, Servicio.class);
            bindService(intentbind, mConnection, Context.BIND_AUTO_CREATE);
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
        consultarSolicitud();


    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        verf = false;
        LojagasAplicacion.getInstance().setConnectivityListener(this);
    }

    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        this.wakelock.release();
    }


    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
        verf = true;
        wakelock.acquire();

    }

    @Override
    protected void onStop() {
        mapView.onStop();
        super.onStop();
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
        this.wakelock.release();

    }

    @Override
    public void conectadoServ() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    @Override
    public void respuestaCambioClave(ResponseApi responseApi) {
        if (responseApi != null) {
            if (responseApi.getEstado() == 1) {
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
                ocultarProgressDialog();
                Toast.makeText(this, responseApi.getMensaje(), Toast.LENGTH_SHORT).show();
            } else {
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
                ocultarProgressDialog();
                Toast.makeText(this, responseApi.getMensaje(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void respuestSugerencia(ResponseApi responseApi) {
        if (responseApi != null) {
            if (responseApi.getEn() == 1) {
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
                ocultarProgressDialog();
                Toast.makeText(this, responseApi.getMensaje(), Toast.LENGTH_SHORT).show();
            } else {
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
                ocultarProgressDialog();
                Toast.makeText(this, responseApi.getMensaje(), Toast.LENGTH_SHORT).show();
            }
        }
    }

// Fin conexion server


    protected void checkPermission() {
        if (ContextCompat.checkSelfPermission(
                MainActivity.this, Manifest.permission.RECORD_AUDIO) +
                ContextCompat.checkSelfPermission(
                        MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) +
                ContextCompat.checkSelfPermission(
                        MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Do something, when permissions not granted
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    MainActivity.this, Manifest.permission.RECORD_AUDIO)
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                    MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(
                            MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an alert dialog here with request explanation
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setCancelable(false);
                builder.setMessage("Es un requisito necesario para el envio de audios de la aplicación");
                builder.setTitle("Por favor, Conceda los permisos");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ActivityCompat.requestPermissions(
                                MainActivity.this,
                                new String[]{
                                        Manifest.permission.RECORD_AUDIO,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                        Manifest.permission.READ_EXTERNAL_STORAGE
                                },
                                MY_PERMISSIONS_REQUEST_CODE
                        );
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                ActivityCompat.requestPermissions(
                        MainActivity.this,
                        new String[]{
                                Manifest.permission.RECORD_AUDIO,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        },
                        MY_PERMISSIONS_REQUEST_CODE
                );
            }
        } else {
            startActivity(new Intent(MainActivity.this, ChatBroadCast.class));
        }

    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (alertDialog != null) {
                alertDialog.dismiss();
            }

            alertDialog = dialogSalirAplicacion();
            alertDialog.show();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void startTimer(long total) {

        countDownTimer = new CountDownTimer(total, 1000) {

            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);

                int hours = seconds / (60 * 60);
                int tempMint = (seconds - (hours * 60 * 60));
                int minutes = tempMint / 60;
                seconds = tempMint - (minutes * 60);


                txtCronometro.setText(tiempoSeleccionado + String.format("%02d", hours)
                        + ":" + String.format("%02d", minutes)
                        + ":" + String.format("%02d", seconds));

            }

            public void onFinish() {
                txtCronometro.setText(tiempoSeleccionado + "00:00:00");
                if (countDownTimer != null) {
                    countDownTimer.cancel();
                }
            }
        }.start();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void cancelarCronometro() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }

    @Override
    public void mensajesUsuarioPedidoPendiente(final ArrayList<ChatMessage> chatHistory,
                                               final int idSolicitud, final int userName, final String barrio,
                                               final String nombreUsuario) {
        if (chatHistory != null) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (idSolicitudChatPedido1 == 0) {
                        Log.e("mensaje", "bien1");
                        idSolicitudChatPedido1 = idSolicitud;
                        adapter = new ChatAdapter(MainActivity.this, new ArrayList<ChatMessage>());
                        List<ChatMessage> nuevoChat = new ArrayList<>();
                        nuevoChat.clear();
                        adapter.removeAll();
                        adapter.notifyDataSetChanged();
                        for (ChatMessage cm : chatHistory) {
                            if (cm.getIdSolicitud() == idSolicitudChatPedido1) {
                                nuevoChat.add(cm);
                            }
                        }
                        adapter.add(nuevoChat);
                        adapter.notifyDataSetChanged();
                        if (alertDialogChatPedidoPendiente1 == null) {
                            Log.e("mensaje", "bien2");
                            alertDialogChatPedidoPendiente1 = dialogChatPedidoPendiente1(adapter, idSolicitudChatPedido1, userName, barrio, nombreUsuario);
                            alertDialogChatPedidoPendiente1.show();
                        }
                    } else if (idSolicitudChatPedido1 == idSolicitud) {
                        idSolicitudChatPedido1 = idSolicitud;
                        adapter = new ChatAdapter(MainActivity.this, new ArrayList<ChatMessage>());
                        List<ChatMessage> nuevoChat = new ArrayList<>();
                        nuevoChat.clear();
                        adapter.removeAll();
                        adapter.notifyDataSetChanged();
                        for (ChatMessage cm : chatHistory) {
                            if (cm.getIdSolicitud() == idSolicitudChatPedido1) {
                                nuevoChat.add(cm);
                            }
                        }
                        adapter.add(nuevoChat);
                        adapter.notifyDataSetChanged();

                        Log.e("mensaje", "bien3");
                        if (alertDialogChatPedidoPendiente1 == null) {
                            Log.e("mensaje", "bien4");
                            alertDialogChatPedidoPendiente1 = dialogChatPedidoPendiente1(adapter, idSolicitudChatPedido1, userName, barrio, nombreUsuario);
                            alertDialogChatPedidoPendiente1.show();
                        } else {
                            Log.e("mensaje", "bien5");
                            dialogChatPedidoPendiente1(adapter, idSolicitudChatPedido1, userName, barrio, nombreUsuario);
                        }
                    } else if (idSolicitudChatPedido2 == 0) {
                        Log.e("mensaje", "bien6");
                        idSolicitudChatPedido2 = idSolicitud;
                        adapter = new ChatAdapter(MainActivity.this, new ArrayList<ChatMessage>());
                        List<ChatMessage> nuevoChat = new ArrayList<>();
                        nuevoChat.clear();
                        adapter.removeAll();
                        adapter.notifyDataSetChanged();
                        for (ChatMessage cm : chatHistory) {
                            if (cm.getIdSolicitud() == idSolicitudChatPedido2) {
                                nuevoChat.add(cm);

                            }
                        }
                        adapter.add(nuevoChat);
                        adapter.notifyDataSetChanged();
                        if (alertDialogChatPedidoPendiente2 == null) {
                            Log.e("mensaje", "bien7");
                            alertDialogChatPedidoPendiente2 = dialogChatPedidoPendiente2(adapter, idSolicitudChatPedido2, userName, barrio, nombreUsuario);
                            alertDialogChatPedidoPendiente2.show();
                        }
                    } else if (idSolicitudChatPedido2 == idSolicitud) {
                        idSolicitudChatPedido2 = idSolicitud;
                        adapter = new ChatAdapter(MainActivity.this, new ArrayList<ChatMessage>());
                        List<ChatMessage> nuevoChat = new ArrayList<>();
                        nuevoChat.clear();
                        adapter.removeAll();
                        adapter.notifyDataSetChanged();
                        for (ChatMessage cm : chatHistory) {
                            if (cm.getIdSolicitud() == idSolicitudChatPedido2) {
                                nuevoChat.add(cm);

                            }
                        }
                        adapter.add(nuevoChat);
                        adapter.notifyDataSetChanged();
                        Log.e("mensaje", "bien8");
                        if (alertDialogChatPedidoPendiente2 == null) {
                            Log.e("mensaje", "bien9");
                            alertDialogChatPedidoPendiente2 = dialogChatPedidoPendiente2(adapter, idSolicitudChatPedido2, userName, barrio, nombreUsuario);
                            alertDialogChatPedidoPendiente2.show();
                        } else {
                            Log.e("mensaje", "bien10");
                            dialogChatPedidoPendiente2(adapter, idSolicitudChatPedido2, userName, barrio, nombreUsuario);
                        }

                    }
                }
            });

        }
    }

    public AlertDialog dialogChatPedidoPendiente1(ChatAdapter adapter, final int idSolicitud, final int userName,
                                                  final String barrio, final String nombreUsuario) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_chat_pendiente, null);
        final EditText messageEdit = v.findViewById(R.id.messageEdit);
        final ImageButton chatSendButton = v.findViewById(R.id.chatSendButton);
        final ImageButton btnAudio = v.findViewById(R.id.btn_audio);
        final ListView messagesContainer = v.findViewById(R.id.messagesContainer);
        final TextView txtBarrio = v.findViewById(R.id.txt_barrio);
        final TextView txtNombreUsuario = v.findViewById(R.id.txt_usuario);
        final ImageView imgMessage = v.findViewById(R.id.img_message);
        final ImageView imgCerrar = v.findViewById(R.id.img_cerrar);

        txtBarrio.setText(barrio);
        txtNombreUsuario.setText(nombreUsuario);

        cerrarTeclado(messageEdit);

        messagesContainer.setAdapter(adapter);
        chatSendButton.setVisibility(View.VISIBLE);

        messagesContainer.setSelection(messagesContainer.getCount() - 1);

        imgMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialogChatOpciones != null) {
                    alertDialogChatOpciones.dismiss();
                }
                alertDialogChatOpciones = dialogChatOpciones(idSolicitud, userName, barrio, nombreUsuario);
                alertDialogChatOpciones.show();
            }
        });

        imgCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialogChatPedidoPendiente1 != null) {
                    alertDialogChatPedidoPendiente1.dismiss();
                    alertDialogChatPedidoPendiente1 = null;
                }
            }
        });

        chatSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageText = messageEdit.getText().toString();
                if (TextUtils.isEmpty(messageText)) {
                    return;
                }
                if (messageEdit.getText().toString().trim().equals("")) {
                    return;
                }
                Log.e("datosSolicitud", idSolicitud + "");
                conexionServer.enviarChatPedidoPendiente(idSolicitud, userName,
                        messageEdit.getText().toString().trim(), barrio, nombreUsuario);

                messageEdit.setText("");
                cerrarTeclado(messageEdit);

            }
        });

        messageEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (isAudio) {
                    if (!messageEdit.getText().toString().isEmpty()) {
                        chatSendButton.setVisibility(View.VISIBLE);
                        btnAudio.setVisibility(View.GONE);
                    } else {
                        chatSendButton.setVisibility(View.VISIBLE);
                        btnAudio.setVisibility(View.GONE);
                    }
                }
            }
        });

        localBuilder.setView(v);
        return localBuilder.create();
    }

    public AlertDialog dialogChatPedidoPendiente2(ChatAdapter adapter, final int idSolicitud, final int userName,
                                                  final String barrio, final String nombreUsuario) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_chat_pendiente, null);
        final EditText messageEdit = v.findViewById(R.id.messageEdit);
        final ImageButton chatSendButton = v.findViewById(R.id.chatSendButton);
        final ImageButton btnAudio = v.findViewById(R.id.btn_audio);
        final ListView messagesContainer = v.findViewById(R.id.messagesContainer);
        final TextView txtBarrio = v.findViewById(R.id.txt_barrio);
        final TextView txtNombreUsuario = v.findViewById(R.id.txt_usuario);
        final ImageView imgMessage = v.findViewById(R.id.img_message);
        final ImageView imgCerrar = v.findViewById(R.id.img_cerrar);

        txtBarrio.setText(barrio);
        txtNombreUsuario.setText(nombreUsuario);

        cerrarTeclado(messageEdit);

        messagesContainer.setAdapter(adapter);
        chatSendButton.setVisibility(View.VISIBLE);

        messagesContainer.setSelection(messagesContainer.getCount() - 1);

        imgMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialogChatOpciones != null) {
                    alertDialogChatOpciones.dismiss();
                }
                alertDialogChatOpciones = dialogChatOpciones(idSolicitud, userName, barrio, nombreUsuario);
                alertDialogChatOpciones.show();
            }
        });

        imgCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialogChatPedidoPendiente2 != null) {
                    alertDialogChatPedidoPendiente2.dismiss();
                    alertDialogChatPedidoPendiente2 = null;
                }
            }
        });

        chatSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageText = messageEdit.getText().toString();
                if (TextUtils.isEmpty(messageText)) {
                    return;
                }
                if (messageEdit.getText().toString().trim().equals("")) {
                    return;
                }
                conexionServer.enviarChatPedidoPendiente(idSolicitud, userName,
                        messageEdit.getText().toString().trim(), barrio, nombreUsuario);

                messageEdit.setText("");
                cerrarTeclado(messageEdit);

            }
        });

        messageEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (isAudio) {
                    if (!messageEdit.getText().toString().isEmpty()) {
                        chatSendButton.setVisibility(View.VISIBLE);
                        btnAudio.setVisibility(View.GONE);
                    } else {
                        chatSendButton.setVisibility(View.VISIBLE);
                        btnAudio.setVisibility(View.GONE);
                    }
                }
            }
        });

        localBuilder.setView(v);
        return localBuilder.create();
    }


    public AlertDialog dialogChatOpciones(final int idSolicitud, final int userName, final String barrio, final String nombreUsuario) {
        final AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(true);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.item_lista_mensaje, null);
        ListView listView = (ListView) v.findViewById(R.id.listView);
        final ArrayList<Mensaje> itemTiemposList = new ArrayList<>();
        itemTiemposList.add(new Mensaje("Estoy en camino"));
        itemTiemposList.add(new Mensaje("Me encuentro en la ubicación seleccionada"));
        itemTiemposList.add(new Mensaje("Espere por favor"));
        itemTiemposList.add(new Mensaje("Solicito un poco más de tiempo de espera"));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mBound) {
                    conexionServer.enviarChatPedidoPendiente(idSolicitud, userName,
                            itemTiemposList.get(position).getMensaje(), barrio, nombreUsuario);
                    alertDialogChatOpciones.dismiss();
                }
            }
        });
        ListAdapterMensajes miAdaptador = new ListAdapterMensajes(this, R.layout.item_lista_mensaje, itemTiemposList);
        listView.setAdapter(miAdaptador);


        localBuilder.setView(v);
        return localBuilder.create();
    }

}
