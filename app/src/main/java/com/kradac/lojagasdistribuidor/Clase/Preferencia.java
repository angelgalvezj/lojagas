package com.kradac.lojagasdistribuidor.Clase;

public class Preferencia {

    private boolean estadoLogin;
    private boolean estadoActividadConductor;
    private int numeroSolicitudes=0;
    private boolean seguimiento=false;
    private int idSolicitudActiva;

    public boolean isEstadoLogin() {
        return estadoLogin;
    }

    public void setEstadoLogin(boolean estadoLogin) {
        this.estadoLogin = estadoLogin;
    }


    public boolean isEstadoActividadConductor() {
        return estadoActividadConductor;
    }

    public void setEstadoActividadConductor(boolean estadoActividadConductor) {
        this.estadoActividadConductor = estadoActividadConductor;
    }

    public int getNumeroSolicitudes() {
        return numeroSolicitudes;
    }

    public void setNumeroSolicitudes(int numeroSolicitudes) {
        this.numeroSolicitudes = numeroSolicitudes;
    }

    public boolean isSeguimiento() {
        return seguimiento;
    }

    public void setSeguimiento(boolean seguimiento) {
        this.seguimiento = seguimiento;
    }

    public int getIdSolicitudActiva() {
        return idSolicitudActiva;
    }

    public void setIdSolicitudActiva(int idSolicitudActiva) {
        this.idSolicitudActiva = idSolicitudActiva;
    }
}
