package com.kradac.lojagasdistribuidor.Clase;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kradac.lojagasdistribuidor.R;
import com.kradac.lojagasdistribuidor.Util.ConnectivityReceiver;
import com.kradac.lojagasdistribuidor.Util.Funciones;
import com.kradac.lojagasdistribuidor.Util.Gps;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.kradac.lojagasdistribuidor.Util.Gps.REQUEST_CHECK_SETTINGS;

public class Splash extends Funciones {

    private static final int MY_PERMISSIONS_REQUEST_CODE = 100;

    @BindView(R.id.txt_version_app)
    TextView txtVersionApp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);


        txtVersionApp.setText("Version: " + obtenerVersion(getApplicationContext()));

        checkPermission();


    }

    private void verificarPermisosGps() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                + ContextCompat.checkSelfPermission(
                Splash.this, Manifest.permission.READ_PHONE_STATE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_PHONE_STATE}, 1);
        } else {
            centrarUbicacionGps();
        }
    }

    private void centrarUbicacionGps() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                + ContextCompat.checkSelfPermission(
                Splash.this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_PHONE_STATE}, 1);
        } else {
            if (gps == null) {
                gps = new Gps(getApplicationContext(), locationListener);
            }else{
                Location locationConductor;
                locationConductor = gps.getLastLocation();
                if (locationConductor != null) {
                    ingresar();
                }
            }

        }
    }

    Gps gps;

    Gps.LocationListener locationListener = new Gps.LocationListener() {
        @Override
        public void onLocationChange(Location location) {
            if (gps != null) {
                centrarUbicacionGps();
                gps.stopLocationUpdates();
                gps.onstop();
                gps = null;
            } else {
                verificarPermisosGps();
            }
        }

        @Override
        public void onGooglePlayServicesDisable(int status) {

        }

        @Override
        public void onGpsDisableNewApi(final Status status) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        status.startResolutionForResult(Splash.this, REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        public void onGpsDisable() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder alertDialog;
                    alertDialog = new AlertDialog.Builder(Splash.this);
                    alertDialog.setTitle(getString(R.string.msg_sericios_ubicacion_deshabilitados));
                    alertDialog.setMessage(getString(R.string.msg_activar_sericios_ubicacion));
                    alertDialog.setPositiveButton(getString(R.string.str_ajustes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent("android.settings.LOCATION_SOURCE_SETTINGS");
                            startActivity(intent);

                        }
                    });
                    alertDialog.setNegativeButton(getString(R.string.str_ignorar), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();

                        }
                    });
                    alertDialog.show();
                }
            });
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int
            resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        Toast.makeText(Splash.this, "GPS activado", Toast.LENGTH_LONG).show();
                        centrarUbicacionGps();
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        gps = null;
                        finish();
                    }
                    default: {
                        break;
                    }
                }
                break;
        }
    }


    public void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        if (!isConnected) {
            mensaje();
        } else {
            verificarPermisosGps();

        }
    }

    public void ingresar() {
        Thread splash = new Thread() {
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if (obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {

                        Intent intentManin = new Intent(Splash.this, MainActivity.class);
                        startActivity(intentManin);
                        finish();

                    }  else {
                        Intent intentLogin = new Intent(Splash.this, Login.class);
                        startActivity(intentLogin);
                        finish();
                    }
                }
            }
        };
        splash.start();
    }

    protected void checkPermission() {
        if (ContextCompat.checkSelfPermission(
                Splash.this, Manifest.permission.READ_PHONE_STATE) +
                ContextCompat.checkSelfPermission(
                        Splash.this, Manifest.permission.ACCESS_FINE_LOCATION) +
                ContextCompat.checkSelfPermission(
                        Splash.this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Do something, when permissions not granted
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    Splash.this, Manifest.permission.READ_PHONE_STATE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                    Splash.this, Manifest.permission.ACCESS_FINE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(
                            Splash.this, Manifest.permission.CAMERA)) {

                // Show an alert dialog here with request explanation
                AlertDialog.Builder builder = new AlertDialog.Builder(Splash.this);
                builder.setCancelable(false);
                builder.setMessage("Es un requisito necesario para el buen funcionamiento de la aplicación");
                builder.setTitle("Por favor, Conceda los permisos");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ActivityCompat.requestPermissions(
                                Splash.this,
                                new String[]{
                                        Manifest.permission.READ_PHONE_STATE,
                                        Manifest.permission.ACCESS_FINE_LOCATION,
                                        Manifest.permission.CAMERA
                                },
                                MY_PERMISSIONS_REQUEST_CODE
                        );
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                ActivityCompat.requestPermissions(
                        Splash.this,
                        new String[]{
                                Manifest.permission.READ_PHONE_STATE,
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.CAMERA
                        },
                        MY_PERMISSIONS_REQUEST_CODE
                );
            }
        } else {
            checkConnection();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CODE: {
                if (((grantResults.length > 0) && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                        && ((grantResults.length > 0) && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                        && ((grantResults.length > 0) && grantResults[2] == PackageManager.PERMISSION_GRANTED)) {
                    checkConnection();
                } else {
                    checkPermission();
                }

            }

            break;
        }
    }
}
