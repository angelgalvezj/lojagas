package com.kradac.lojagasdistribuidor.Modelo;

import java.util.ArrayList;

public class DatosSolicitud {

    private int id_credito;
    private int idServicioActivo;
    private int prV;
    private int id_aplicativo;
    private int tFp;
    private int c;
    private int room;
    private int estado;
    private int id_plataforma_ktaxi;
    private int id_ciudad;
    private int id_cliente;
    private int idEmpresa;
    private int saldo;
    private int p;
    private int d;
    private int conP;
    private int tiempo;
    private double longitud;
    private double distancia;
    private double latitud;
    private String color;
    private String servicio;
    private String ciudad;
    private String id_dispositivo;
    private String nombres;
    private String celular;
    private String plataformaKtaxi;
    private String callePrincipal;
    private String calleSecundaria;
    private String barrioCliente;
    private String referenciaCliente;
    private String modelo;
    private String idSolicitudCc;
    private ArrayList<Integer> tiempos;


    public int getId_credito() {
        return id_credito;
    }

    public void setId_credito(int id_credito) {
        this.id_credito = id_credito;
    }

    public int getIdServicioActivo() {
        return idServicioActivo;
    }

    public void setIdServicioActivo(int idServicioActivo) {
        this.idServicioActivo = idServicioActivo;
    }

    public int getPrV() {
        return prV;
    }

    public void setPrV(int prV) {
        this.prV = prV;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getId_aplicativo() {
        return id_aplicativo;
    }

    public void setId_aplicativo(int id_aplicativo) {
        this.id_aplicativo = id_aplicativo;
    }

    public int gettFp() {
        return tFp;
    }

    public void settFp(int tFp) {
        this.tFp = tFp;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public int getRoom() {
        return room;
    }

    public void setRoom(int room) {
        this.room = room;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getId_plataforma_ktaxi() {
        return id_plataforma_ktaxi;
    }

    public void setId_plataforma_ktaxi(int id_plataforma_ktaxi) {
        this.id_plataforma_ktaxi = id_plataforma_ktaxi;
    }

    public String getId_dispositivo() {
        return id_dispositivo;
    }

    public void setId_dispositivo(String id_dispositivo) {
        this.id_dispositivo = id_dispositivo;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getPlataformaKtaxi() {
        return plataformaKtaxi;
    }

    public void setPlataformaKtaxi(String plataformaKtaxi) {
        this.plataformaKtaxi = plataformaKtaxi;
    }

    public int getId_ciudad() {
        return id_ciudad;
    }

    public void setId_ciudad(int id_ciudad) {
        this.id_ciudad = id_ciudad;
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getCallePrincipal() {
        return callePrincipal;
    }

    public void setCallePrincipal(String callePrincipal) {
        this.callePrincipal = callePrincipal;
    }

    public String getCalleSecundaria() {
        return calleSecundaria;
    }

    public void setCalleSecundaria(String calleSecundaria) {
        this.calleSecundaria = calleSecundaria;
    }

    public String getBarrioCliente() {
        return barrioCliente;
    }

    public void setBarrioCliente(String barrioCliente) {
        this.barrioCliente = barrioCliente;
    }

    public String getReferenciaCliente() {
        return referenciaCliente;
    }

    public void setReferenciaCliente(String referenciaCliente) {
        this.referenciaCliente = referenciaCliente;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public int getP() {
        return p;
    }

    public void setP(int p) {
        this.p = p;
    }

    public int getD() {
        return d;
    }

    public void setD(int d) {
        this.d = d;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getConP() {
        return conP;
    }

    public void setConP(int conP) {
        this.conP = conP;
    }

    public double getDistancia() {
        return distancia;
    }

    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public ArrayList<Integer> getTiempos() {
        return tiempos;
    }

    public void setTiempos(ArrayList<Integer> tiempos) {
        this.tiempos = tiempos;
    }

    public String getIdSolicitudCc() {
        return idSolicitudCc;
    }

    public void setIdSolicitudCc(String idSolicitudCc) {
        this.idSolicitudCc = idSolicitudCc;
    }

    @Override
    public String toString() {
        return "DatosSolicitud{" +
                "id_credito=" + id_credito +
                ", idServicioActivo=" + idServicioActivo +
                ", prV=" + prV +
                ", id_aplicativo=" + id_aplicativo +
                ", tFp=" + tFp +
                ", c=" + c +
                ", room=" + room +
                ", estado=" + estado +
                ", id_plataforma_ktaxi=" + id_plataforma_ktaxi +
                ", id_ciudad=" + id_ciudad +
                ", id_cliente=" + id_cliente +
                ", idEmpresa=" + idEmpresa +
                ", saldo=" + saldo +
                ", p=" + p +
                ", d=" + d +
                ", conP=" + conP +
                ", tiempo=" + tiempo +
                ", longitud=" + longitud +
                ", distancia=" + distancia +
                ", latitud=" + latitud +
                ", color='" + color + '\'' +
                ", servicio='" + servicio + '\'' +
                ", ciudad='" + ciudad + '\'' +
                ", id_dispositivo='" + id_dispositivo + '\'' +
                ", nombres='" + nombres + '\'' +
                ", celular='" + celular + '\'' +
                ", plataformaKtaxi='" + plataformaKtaxi + '\'' +
                ", callePrincipal='" + callePrincipal + '\'' +
                ", calleSecundaria='" + calleSecundaria + '\'' +
                ", barrioCliente='" + barrioCliente + '\'' +
                ", referenciaCliente='" + referenciaCliente + '\'' +
                ", modelo='" + modelo + '\'' +
                ", idSolicitudCc=" + idSolicitudCc +
                ", tiempos=" + tiempos +
                '}';
    }
}
