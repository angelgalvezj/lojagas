package com.kradac.lojagasdistribuidor.Modelo;

public class Historial {

    private String pin;
    private String ob;
    private int vl;
    private int t;
    private int id;
    private int e;
    private String cP;
    private String cS;
    private String b;
    private String r;
    private int d;
    private String h;
    private int v;
    private String pl;
    private String rM;
    private String nbE;

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getOb() {
        return ob;
    }

    public void setOb(String ob) {
        this.ob = ob;
    }

    public int getVl() {
        return vl;
    }

    public void setVl(int vl) {
        this.vl = vl;
    }

    public int getT() {
        return t;
    }

    public void setT(int t) {
        this.t = t;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getE() {
        return e;
    }

    public void setE(int e) {
        this.e = e;
    }

    public String getcP() {
        return cP;
    }

    public void setcP(String cP) {
        this.cP = cP;
    }

    public String getcS() {
        return cS;
    }

    public void setcS(String cS) {
        this.cS = cS;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public String getR() {
        return r;
    }

    public void setR(String r) {
        this.r = r;
    }

    public int getD() {
        return d;
    }

    public void setD(int d) {
        this.d = d;
    }

    public String getH() {
        return h;
    }

    public void setH(String h) {
        this.h = h;
    }

    public int getV() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }

    public String getPl() {
        return pl;
    }

    public void setPl(String pl) {
        this.pl = pl;
    }

    public String getrM() {
        return rM;
    }

    public void setrM(String rM) {
        this.rM = rM;
    }

    public String getNbE() {
        return nbE;
    }

    public void setNbE(String nbE) {
        this.nbE = nbE;
    }

    @Override
    public String toString() {
        return "Historial{" +
                "pin='" + pin + '\'' +
                ", ob='" + ob + '\'' +
                ", vl=" + vl +
                ", t=" + t +
                ", id=" + id +
                ", e=" + e +
                ", cP='" + cP + '\'' +
                ", cS='" + cS + '\'' +
                ", b='" + b + '\'' +
                ", r='" + r + '\'' +
                ", d=" + d +
                ", h='" + h + '\'' +
                ", v=" + v +
                ", pl='" + pl + '\'' +
                ", rM='" + rM + '\'' +
                ", nbE='" + nbE + '\'' +
                '}';
    }
}