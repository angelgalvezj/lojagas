package com.kradac.lojagasdistribuidor.Modelo;

public class Pedido {

    private String caracteristica;

    public String getCaracteristica() {
        return caracteristica;
    }

    public void setCaracteristica(String caracteristica) {
        this.caracteristica = caracteristica;
    }

    @Override
    public String toString() {
        return "Pedido{" +
                "caracteristica='" + caracteristica + '\'' +
                '}';
    }
}
