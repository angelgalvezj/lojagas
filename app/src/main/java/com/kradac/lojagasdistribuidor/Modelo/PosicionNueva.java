package com.kradac.lojagasdistribuidor.Modelo;

public class PosicionNueva {

    private int idVehiculo;
    private String fecha;
    private int idEquipo;
    private double latitud;
    private double longitud;
    private int altitud;
    private int velocidad;
    private int acury;
    private int direccion;
    private int bateria;
    private int conexion;
    private int gps;
    private int estado;
    private int temperatura;
    private double consumo;
    private double tipoRed;

    public PosicionNueva() {
    }

    public int getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(int idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public int getAltitud() {
        return altitud;
    }

    public void setAltitud(int altitud) {
        this.altitud = altitud;
    }

    public int getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(int velocidad) {
        this.velocidad = velocidad;
    }

    public int getAcury() {
        return acury;
    }

    public void setAcury(int acury) {
        this.acury = acury;
    }

    public int getDireccion() {
        return direccion;
    }

    public void setDireccion(int direccion) {
        this.direccion = direccion;
    }

    public int getBateria() {
        return bateria;
    }

    public void setBateria(int bateria) {
        this.bateria = bateria;
    }

    public int getConexion() {
        return conexion;
    }

    public void setConexion(int conexion) {
        this.conexion = conexion;
    }

    public int getGps() {
        return gps;
    }

    public void setGps(int gps) {
        this.gps = gps;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(int temperatura) {
        this.temperatura = temperatura;
    }

    public double getConsumo() {
        return consumo;
    }

    public void setConsumo(double consumo) {
        this.consumo = consumo;
    }

    public double getTipoRed() {
        return tipoRed;
    }

    public void setTipoRed(double tipoRed) {
        this.tipoRed = tipoRed;
    }

    @Override
    public String toString() {
        return "PosicionNueva{" +
                "idVehiculo=" + idVehiculo +
                ", fecha='" + fecha + '\'' +
                ", idEquipo=" + idEquipo +
                ", latitud=" + latitud +
                ", longitud=" + longitud +
                ", altitud=" + altitud +
                ", velocidad=" + velocidad +
                ", acury=" + acury +
                ", direccion=" + direccion +
                ", bateria=" + bateria +
                ", conexion=" + conexion +
                ", gps=" + gps +
                ", estado=" + estado +
                ", temperatura=" + temperatura +
                ", consumo=" + consumo +
                ", tipoRed=" + tipoRed +
                '}';
    }
}
