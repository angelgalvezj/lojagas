package com.kradac.lojagasdistribuidor.Modelo;

import java.io.Serializable;
import java.util.List;

public class Solicitud implements Serializable {

    private int username;
    private int idSolicitud;
    private String tiempoTranscurrido;
    private String idPlataformaktaxi;
    private int idCiudad;
    private int tiempoAceptado;
    private boolean postulacion;
    private String tiempoEntrega;


    // 1: Nueva solicitud
    // 2: Solicitud aceptada
    // 3: Solicitud pendiente
    // 4: Solicitud realizada

    private int estadoSolicitud;

    private List<Pedido> lC;

    private DatosSolicitud datosSolicitud;

    public int getUsername() {
        return username;
    }

    public void setUsername(int username) {
        this.username = username;
    }

    public int getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(int idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public String getTiempoTranscurrido() {
        return tiempoTranscurrido;
    }

    public void setTiempoTranscurrido(String tiempoTranscurrido) {
        this.tiempoTranscurrido = tiempoTranscurrido;
    }

    public String getIdPlataformaktaxi() {
        return idPlataformaktaxi;
    }

    public void setIdPlataformaktaxi(String idPlataformaktaxi) {
        this.idPlataformaktaxi = idPlataformaktaxi;
    }

    public int getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(int idCiudad) {
        this.idCiudad = idCiudad;
    }

    public List<Pedido> getlC() {
        return lC;
    }

    public void setlC(List<Pedido> lC) {
        this.lC = lC;
    }

    public DatosSolicitud getDatosSolicitud() {
        return datosSolicitud;
    }

    public void setDatosSolicitud(DatosSolicitud datosSolicitud) {
        this.datosSolicitud = datosSolicitud;
    }

    public int getEstadoSolicitud() {
        return estadoSolicitud;
    }

    public void setEstadoSolicitud(int estadoSolicitud) {
        this.estadoSolicitud = estadoSolicitud;
    }

    public int getTiempoAceptado() {
        return tiempoAceptado;
    }

    public void setTiempoAceptado(int tiempoAceptado) {
        this.tiempoAceptado = tiempoAceptado;
    }

    public boolean isPostulacion() {
        return postulacion;
    }

    public void setPostulacion(boolean postulacion) {
        this.postulacion = postulacion;
    }

    public String getTiempoEntrega() {
        return tiempoEntrega;
    }

    public void setTiempoEntrega(String tiempoEntrega) {
        this.tiempoEntrega = tiempoEntrega;
    }

    @Override
    public String toString() {
        return "Solicitud{" +
                "username=" + username +
                ", idSolicitud=" + idSolicitud +
                ", tiempoTranscurrido='" + tiempoTranscurrido + '\'' +
                ", idPlataformaktaxi='" + idPlataformaktaxi + '\'' +
                ", idCiudad=" + idCiudad +
                ", tiempoAceptado=" + tiempoAceptado +
                ", postulacion=" + postulacion +
                ", tiempoEntrega='" + tiempoEntrega + '\'' +
                ", estadoSolicitud=" + estadoSolicitud +
                ", lC=" + lC +
                ", datosSolicitud=" + datosSolicitud +
                '}';
    }
}
