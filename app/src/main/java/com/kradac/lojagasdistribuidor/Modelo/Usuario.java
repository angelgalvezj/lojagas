package com.kradac.lojagasdistribuidor.Modelo;

public class Usuario {

    private int bloqueado;
    private int id_rol_usuario;
    private int rol;
    private int idEquipo;
    private int tipoVehiculo;
    private int idCiudad;
    private int idEmpresa;
    private int anioVehiculo;
    private int unidadVehiculo;
    private int idVehiculo;
    private int habilitado_ktaxidriver;
    private int idUsuario;
    private String mensaje_bloqueo_driver;
    private String usuario;
    private String imagenConductor;
    private String nombres;
    private String apellidos;
    private String correo;
    private String celular;
    private String cedula;
    private String empresa;
    private String placaVehiculo;
    private String regMunVehiculo;
    private String marcaVehiculo;
    private String modeloVehiculo;
    private String ciudad;

    public int getBloqueado() {
        return bloqueado;
    }

    public void setBloqueado(int bloqueado) {
        this.bloqueado = bloqueado;
    }

    public String getMensaje_bloqueo_driver() {
        return mensaje_bloqueo_driver;
    }

    public void setMensaje_bloqueo_driver(String mensaje_bloqueo_driver) {
        this.mensaje_bloqueo_driver = mensaje_bloqueo_driver;
    }

    public int getId_rol_usuario() {
        return id_rol_usuario;
    }

    public void setId_rol_usuario(int id_rol_usuario) {
        this.id_rol_usuario = id_rol_usuario;
    }

    public int getRol() {
        return rol;
    }

    public void setRol(int rol) {
        this.rol = rol;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getImagenConductor() {
        return imagenConductor;
    }

    public void setImagenConductor(String imagenConductor) {
        this.imagenConductor = imagenConductor;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public int getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(int tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getPlacaVehiculo() {
        return placaVehiculo;
    }

    public void setPlacaVehiculo(String placaVehiculo) {
        this.placaVehiculo = placaVehiculo;
    }

    public String getRegMunVehiculo() {
        return regMunVehiculo;
    }

    public void setRegMunVehiculo(String regMunVehiculo) {
        this.regMunVehiculo = regMunVehiculo;
    }

    public String getMarcaVehiculo() {
        return marcaVehiculo;
    }

    public void setMarcaVehiculo(String marcaVehiculo) {
        this.marcaVehiculo = marcaVehiculo;
    }

    public String getModeloVehiculo() {
        return modeloVehiculo;
    }

    public void setModeloVehiculo(String modeloVehiculo) {
        this.modeloVehiculo = modeloVehiculo;
    }

    public int getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(int idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public int getAnioVehiculo() {
        return anioVehiculo;
    }

    public void setAnioVehiculo(int anioVehiculo) {
        this.anioVehiculo = anioVehiculo;
    }

    public int getUnidadVehiculo() {
        return unidadVehiculo;
    }

    public void setUnidadVehiculo(int unidadVehiculo) {
        this.unidadVehiculo = unidadVehiculo;
    }

    public int getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(int idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public int getHabilitado_ktaxidriver() {
        return habilitado_ktaxidriver;
    }

    public void setHabilitado_ktaxidriver(int habilitado_ktaxidriver) {
        this.habilitado_ktaxidriver = habilitado_ktaxidriver;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "bloqueado=" + bloqueado +
                ", id_rol_usuario=" + id_rol_usuario +
                ", rol=" + rol +
                ", idEquipo=" + idEquipo +
                ", tipoVehiculo=" + tipoVehiculo +
                ", idCiudad=" + idCiudad +
                ", idEmpresa=" + idEmpresa +
                ", anioVehiculo=" + anioVehiculo +
                ", unidadVehiculo=" + unidadVehiculo +
                ", idVehiculo=" + idVehiculo +
                ", habilitado_ktaxidriver=" + habilitado_ktaxidriver +
                ", idUsuario=" + idUsuario +
                ", mensaje_bloqueo_driver='" + mensaje_bloqueo_driver + '\'' +
                ", usuario='" + usuario + '\'' +
                ", imagenConductor='" + imagenConductor + '\'' +
                ", nombres='" + nombres + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", correo='" + correo + '\'' +
                ", celular='" + celular + '\'' +
                ", cedula='" + cedula + '\'' +
                ", empresa='" + empresa + '\'' +
                ", placaVehiculo='" + placaVehiculo + '\'' +
                ", regMunVehiculo='" + regMunVehiculo + '\'' +
                ", marcaVehiculo='" + marcaVehiculo + '\'' +
                ", modeloVehiculo='" + modeloVehiculo + '\'' +
                ", ciudad='" + ciudad + '\'' +
                '}';
    }
}
