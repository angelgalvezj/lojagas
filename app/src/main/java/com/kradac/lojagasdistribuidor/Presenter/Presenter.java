package com.kradac.lojagasdistribuidor.Presenter;

import com.google.gson.GsonBuilder;
import com.kradac.lojagasdistribuidor.Util.VariablesGlobales;

import java.lang.reflect.Modifier;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Presenter {

    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

    private OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(interceptor.setLevel(HttpLoggingInterceptor.Level.BODY))
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build();

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(VariablesGlobales.IP_CONEXION)
            .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                    .serializeNulls()
                    .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                    .create()))
            .client(client)
            .build();

}
