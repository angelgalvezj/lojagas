package com.kradac.lojagasdistribuidor.Presenter;

import com.kradac.lojagasdistribuidor.Api.LojagasApi;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionCambioClave;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionLogin;
import com.kradac.lojagasdistribuidor.Response.ResponseApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PresenterCambioClave extends Presenter {

    private static final String TAG = PresenterCambioClave.class.getName();
    private OnComunicacionCambioClave onComunicacionCambioClave;

    public PresenterCambioClave(OnComunicacionCambioClave onComunicacionCambioClave) {
        this.onComunicacionCambioClave = onComunicacionCambioClave;
    }


    public void cambioContrasena(int idUsuario,String claveAnterior,String claveNueva,int idAplicativo) {
        LojagasApi service = retrofit.create(LojagasApi.class);
        Call<ResponseApi> call = service.cambioClave(idUsuario,claveAnterior,claveNueva,idAplicativo);
        call.enqueue(new Callback<ResponseApi>() {
            @Override
            public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                if (response.code() == 200) {
                    ResponseApi responseApi = response.body();
                    onComunicacionCambioClave.respuestaCambioClave(responseApi);
                } else {
                    onComunicacionCambioClave.respuestaCambioClave(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseApi> call, Throwable t) {
                onComunicacionCambioClave.respuestaCambioClave(null);
            }
        });
    }
}

