package com.kradac.lojagasdistribuidor.Presenter;

import com.kradac.lojagasdistribuidor.Api.LojagasApi;
import com.kradac.lojagasdistribuidor.Modelo.Historial;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionHistorialAnioMes;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionHistorialSolicitud;
import com.kradac.lojagasdistribuidor.Response.ResponseApi;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PresenterHistorialAnioMes extends Presenter {

    private OnComunicacionHistorialAnioMes onComunicacionHistorialAnioMes;

    public PresenterHistorialAnioMes(OnComunicacionHistorialAnioMes onComunicacionHistorialAnioMes) {
        this.onComunicacionHistorialAnioMes = onComunicacionHistorialAnioMes;
    }


    public void historialAnioMes(int idUsuario, int anio,
                                 int mes, int tipo, int desde, int cuantos) {
        LojagasApi service = retrofit.create(LojagasApi.class);
        Call<List<Historial>> call = service.obtenerHistorialAnioMes(idUsuario, anio,
                mes, tipo, desde, cuantos);
        call.enqueue(new Callback<List<Historial>>() {
            @Override
            public void onResponse(Call<List<Historial>> call, Response<List<Historial>> response) {
                if (response.code() == 200) {
                    List<Historial> responseApi = response.body();
                    onComunicacionHistorialAnioMes.respuestaHistorialAnioMes(responseApi);
                } else {
                    onComunicacionHistorialAnioMes.respuestaHistorialAnioMes(null);
                }
            }

            @Override
            public void onFailure(Call<List<Historial>> call, Throwable t) {
                onComunicacionHistorialAnioMes.respuestaHistorialAnioMes(null);
            }
        });
    }
}

