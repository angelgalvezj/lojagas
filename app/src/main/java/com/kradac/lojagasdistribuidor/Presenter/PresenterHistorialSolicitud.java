package com.kradac.lojagasdistribuidor.Presenter;

import com.kradac.lojagasdistribuidor.Api.LojagasApi;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionHistorialSolicitud;
import com.kradac.lojagasdistribuidor.Response.ResponseApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PresenterHistorialSolicitud extends Presenter {

    private OnComunicacionHistorialSolicitud onComunicacionHistorialSolicitud;

    public PresenterHistorialSolicitud(OnComunicacionHistorialSolicitud onComunicacionHistorialSolicitud) {
        this.onComunicacionHistorialSolicitud = onComunicacionHistorialSolicitud;
    }


    public void historialSolicitud(int idUsuario, int anio, int mes, int tipo) {
        LojagasApi service = retrofit.create(LojagasApi.class);
        Call<ResponseApi> call = service.historialSolicitud(idUsuario, anio, mes, tipo);
        call.enqueue(new Callback<ResponseApi>() {
            @Override
            public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                if (response.code() == 200) {
                    ResponseApi responseApi = response.body();
                    onComunicacionHistorialSolicitud.respuestaHistorial(responseApi);
                } else {
                    onComunicacionHistorialSolicitud.respuestaHistorial(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseApi> call, Throwable t) {
                onComunicacionHistorialSolicitud.respuestaHistorial(null);
            }
        });
    }
}

