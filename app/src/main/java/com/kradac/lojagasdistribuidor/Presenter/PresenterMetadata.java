package com.kradac.lojagasdistribuidor.Presenter;

import com.kradac.lojagasdistribuidor.Api.LojagasApi;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionLogin;
import com.kradac.lojagasdistribuidor.Response.ResponseApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PresenterMetadata extends Presenter {

    private static final String TAG = PresenterMetadata.class.getName();
    private OnComunicacionLogin onComunicacionLogin;

    public PresenterMetadata(OnComunicacionLogin onComunicacionLogin) {
        this.onComunicacionLogin = onComunicacionLogin;
    }


    public void enviarMetaData(int idPltaformaKtaxi, String versionKtaxi,
                               String versionSo, String marca, String modelo, int idUsuario) {
        LojagasApi service = retrofit.create(LojagasApi.class);
        Call<ResponseApi> call = service.enviarMetaData(idPltaformaKtaxi, versionKtaxi,
                versionSo, marca, modelo, idUsuario);
        call.enqueue(new Callback<ResponseApi>() {
            @Override
            public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                if (response.code() == 200) {
                    ResponseApi responseApi = response.body();
                    onComunicacionLogin.enviarMetaData(responseApi);
                } else {
                    onComunicacionLogin.enviarMetaData(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseApi> call, Throwable t) {
                onComunicacionLogin.enviarMetaData(null);
            }
        });
    }
}

