package com.kradac.lojagasdistribuidor.Presenter;

import com.kradac.lojagasdistribuidor.Api.LojagasApi;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionCambioClave;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionRecuperarClave;
import com.kradac.lojagasdistribuidor.Response.ResponseApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PresenterRecuperarClave extends Presenter {

    private static final String TAG = PresenterRecuperarClave.class.getName();
    private OnComunicacionRecuperarClave onComunicacionRecuperarClave;

    public PresenterRecuperarClave(OnComunicacionRecuperarClave onComunicacionRecuperarClave) {
        this.onComunicacionRecuperarClave = onComunicacionRecuperarClave;
    }


    public void recuperarClave(String usuario,int idAplicativo) {
        LojagasApi service = retrofit.create(LojagasApi.class);
        Call<ResponseApi> call = service.recuperarContrasenia(usuario,idAplicativo);
        call.enqueue(new Callback<ResponseApi>() {
            @Override
            public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                if (response.code() == 200) {
                    ResponseApi responseApi = response.body();
                    onComunicacionRecuperarClave.respuestaRecuperarClave(responseApi);
                } else {
                    onComunicacionRecuperarClave.respuestaRecuperarClave(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseApi> call, Throwable t) {
                onComunicacionRecuperarClave.respuestaRecuperarClave(null);
            }
        });
    }
}

