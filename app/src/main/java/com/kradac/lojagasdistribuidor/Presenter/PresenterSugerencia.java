package com.kradac.lojagasdistribuidor.Presenter;

import android.content.Context;

import com.kradac.lojagasdistribuidor.Api.LojagasApi;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionCambioClave;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionSugerencia;
import com.kradac.lojagasdistribuidor.Response.ResponseApi;
import com.kradac.lojagasdistribuidor.Util.Funciones;
import com.kradac.lojagasdistribuidor.Util.VariablesGlobales;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;

public class PresenterSugerencia extends Presenter {

    private static final String TAG = PresenterSugerencia.class.getName();
    private OnComunicacionSugerencia onComunicacionSugerencia;
    private Funciones funciones;
    private Context context;

    public PresenterSugerencia(Context context, OnComunicacionSugerencia onComunicacionSugerencia) {
        this.onComunicacionSugerencia = onComunicacionSugerencia;
        funciones = new Funciones();
        this.context = context;

    }


    public void enviarSugerencia(int tipo, int idUsuario, int idSolicitud, String nombre, String correo, String telefono,
                                 String motivo, String mensaje,
                                 double latitud, double longitud, String codigoPais) {
        LojagasApi service = retrofit.create(LojagasApi.class);
        Call<ResponseApi> call = service.enviarSugerencia(tipo, VariablesGlobales.NUM_ID_APLICATIVO,
                0, idUsuario, idUsuario, 1, idSolicitud, nombre, correo,
                telefono, motivo, mensaje, latitud, longitud, funciones.obtenerVersion(context),
                funciones.obtenerVersionSo(), funciones.obtenerMarca(), funciones.obtenerModelo(),
                funciones.getTipoRed(context), funciones.statusGPS(context), funciones.getConnectivityStatus(context),
                funciones.getNameOperator(context), funciones.obtenerIMEI(context), 2, codigoPais, 0);
        call.enqueue(new Callback<ResponseApi>() {
            @Override
            public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                if (response.code() == 200) {
                    ResponseApi responseApi = response.body();
                    onComunicacionSugerencia.respuestSugerencia(responseApi);
                } else {
                    onComunicacionSugerencia.respuestSugerencia(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseApi> call, Throwable t) {
                onComunicacionSugerencia.respuestSugerencia(null);
            }
        });
    }
}

