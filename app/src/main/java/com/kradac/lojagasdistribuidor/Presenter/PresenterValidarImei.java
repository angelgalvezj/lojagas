package com.kradac.lojagasdistribuidor.Presenter;

import android.util.Log;

import com.kradac.lojagasdistribuidor.Api.LojagasApi;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionLogin;
import com.kradac.lojagasdistribuidor.Response.ResponseApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PresenterValidarImei extends Presenter {

    private static final String TAG = PresenterValidarImei.class.getName();
    private OnComunicacionLogin onComunicacionLogin;

    public PresenterValidarImei(OnComunicacionLogin onComunicacionLogin) {
        this.onComunicacionLogin = onComunicacionLogin;
    }


    public void validarImei(int idUsuario,String idDispositivo,String marca,String modelo,
                            String versionSO,String versionAplicativo,double ltGPS,double lgGPS) {
        LojagasApi service = retrofit.create(LojagasApi.class);
        Call<ResponseApi> call = service.validarImei( idUsuario, idDispositivo, marca, modelo,
                 versionSO, versionAplicativo, ltGPS, lgGPS);
        call.enqueue(new Callback<ResponseApi>() {
            @Override
            public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                if (response.code() == 200) {
                    ResponseApi responseApi = response.body();
                    onComunicacionLogin.respuestaValidarImei(responseApi);
                } else {
                    onComunicacionLogin.respuestaValidarImei(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseApi> call, Throwable t) {
                onComunicacionLogin.respuestaValidarImei(null);
            }
        });
    }
}

