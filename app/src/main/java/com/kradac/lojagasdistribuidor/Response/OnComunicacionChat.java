package com.kradac.lojagasdistribuidor.Response;

import com.kradac.lojagasdistribuidor.Modelo.ChatMessage;

import java.util.ArrayList;

public interface OnComunicacionChat {

    void conectadoServ();
    void mensajesUsuario(ArrayList<ChatMessage> chatHistory);

}
