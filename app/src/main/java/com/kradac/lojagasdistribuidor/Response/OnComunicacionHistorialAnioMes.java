package com.kradac.lojagasdistribuidor.Response;

import com.kradac.lojagasdistribuidor.Modelo.Historial;

import java.util.List;

public interface OnComunicacionHistorialAnioMes  {

    void respuestaHistorialAnioMes(List<Historial> historialList);
}
