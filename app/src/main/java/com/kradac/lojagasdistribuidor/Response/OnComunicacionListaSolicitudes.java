package com.kradac.lojagasdistribuidor.Response;

public interface OnComunicacionListaSolicitudes {

    void conectadoServ();

    void mensajeDeuda(String mensaje, int idSolicitud);

    void noAceptoTiempo(int idSolicitud);

    void borrarSolicitud(int idSolicitud);

    void aceptoTiempo(int idSolicitud);

    void nuevaSolicitud();

    void solicitudAsignadaCallCenter();

    void clienteCanceloPedido(int idSolicitud);

    void cerrarSesion(String nombre,String mensaje);
}
