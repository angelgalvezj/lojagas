package com.kradac.lojagasdistribuidor.Response;

import com.kradac.lojagasdistribuidor.Modelo.Usuario;

import org.json.JSONArray;
import org.json.JSONObject;

public interface OnComunicacionLogin {

    void conectadoServ();

    void mensajeActivacionDispositivo(int auxIdUsuario, String mensaje);

    void mensajeErrorContrasena(String mensaje);

    void respuestaValidarImei(ResponseApi responseApi);

    void cerrarSesion(String mensaje, String datos);

    void mensajeRespuesta(String mensaje, String datosUsuario);

    void mensajeRespuestaCierreListaVehiculos(String mensaje, Usuario usuario);

    void mensajeDeuda(String mensaje);

    void enviarMetaData(ResponseApi responseApi);

    void logueoExitoso(String mensaje, String datos);

    void logueoExitosoListaVehiculo(String mensaje, Usuario datos);

    void dispositivoRegistrado(String mensaje);

    void seleccionDispositivo(String datos);

    void cerrarSesionListaVehiculo(String mensaje, Usuario usuario);


}
