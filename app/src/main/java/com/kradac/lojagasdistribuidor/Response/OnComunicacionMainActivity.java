package com.kradac.lojagasdistribuidor.Response;

import com.kradac.lojagasdistribuidor.Modelo.ChatMessage;
import com.kradac.lojagasdistribuidor.Modelo.Solicitud;

import java.util.ArrayList;

public interface OnComunicacionMainActivity {

    void conectadoServ();

    void dispositivoNoAutorizado(String mensaje, String nombres);

    void actualizacionUsuario(String datosUsuario);

    void encenderPantalla();

    void confirmacionPedidoEntregado();

    void avisoClientePedidoEntregado(int idSolicitud);

    void calificacionCliente(String mensaje);

    void cancelacionPedido();

    void solicitudAsignadaCallCenter();

    void mensajesUsuario(ArrayList<ChatMessage> chatHistory);

    void mensajesUsuarioPedidoPendiente(ArrayList<ChatMessage> chatHistory,int idSolicitud,int userName,
                                        String barrio,String nombreUsuario);

    void clienteCanceloPedido(int idSolicitud);

    void clienteCanceloPedidoPendiente(int idSolicitud);

    void cerrarSesion(String nombre, String mensaje);

    void entregaGasPendiente(int idSolicitud);


}
