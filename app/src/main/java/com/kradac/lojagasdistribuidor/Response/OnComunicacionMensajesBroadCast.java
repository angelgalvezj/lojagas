package com.kradac.lojagasdistribuidor.Response;

import com.kradac.lojagasdistribuidor.Modelo.ChatMessage;

import java.util.ArrayList;

public interface OnComunicacionMensajesBroadCast {

    void mensajesCallCenter(ArrayList<ChatMessage> chatHistory);

    void mensajesBroadCastEmpresa(ArrayList<ChatMessage> chatHistory);

    void mensajesBroadCast(ArrayList<ChatMessage> chatHistory);
}
