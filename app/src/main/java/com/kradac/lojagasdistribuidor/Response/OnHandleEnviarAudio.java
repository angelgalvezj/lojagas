package com.kradac.lojagasdistribuidor.Response;

import com.kradac.lojagasdistribuidor.Modelo.ChatMessage;
import com.kradac.lojagasdistribuidor.Servicio.Servicio;

import java.util.ArrayList;

public interface OnHandleEnviarAudio {

    void onGrabarEnviarAudio(String nameAudio, int fragment);
    Servicio onServicioSocket();
    ArrayList<ChatMessage> getChatCallCenter();
    ArrayList<ChatMessage> getChatBroadCastEmpresa();
    ArrayList<ChatMessage> getChatBroadCast();
}
