package com.kradac.lojagasdistribuidor.Response;

public class ResponseApi {

    private int en;
    private String m;
    private int estado;
    private String mensaje;
    private int registros;

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public int getRegistros() {
        return registros;
    }

    public void setRegistros(int registros) {
        this.registros = registros;
    }

    @Override
    public String toString() {
        return "ResponseApi{" +
                "en=" + en +
                ", m='" + m + '\'' +
                ", estado=" + estado +
                ", mensaje='" + mensaje + '\'' +
                ", registros=" + registros +
                '}';
    }
}
