package com.kradac.lojagasdistribuidor.Servicio;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.location.Location;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.BatteryManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kradac.lojagasdistribuidor.Clase.Chat;
import com.kradac.lojagasdistribuidor.Clase.ListaSolicitudes;
import com.kradac.lojagasdistribuidor.Clase.Login;
import com.kradac.lojagasdistribuidor.Clase.MainActivity;
import com.kradac.lojagasdistribuidor.Modelo.ChatMessage;
import com.kradac.lojagasdistribuidor.Modelo.DatosSolicitud;
import com.kradac.lojagasdistribuidor.Modelo.Pedido;
import com.kradac.lojagasdistribuidor.Modelo.PosicionNueva;
import com.kradac.lojagasdistribuidor.Modelo.Rastreo;
import com.kradac.lojagasdistribuidor.Modelo.Solicitud;
import com.kradac.lojagasdistribuidor.Modelo.Usuario;
import com.kradac.lojagasdistribuidor.R;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionChat;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionListaSolicitudes;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionLogin;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionMainActivity;
import com.kradac.lojagasdistribuidor.Response.OnComunicacionMensajesBroadCast;
import com.kradac.lojagasdistribuidor.Util.Funciones;
import com.kradac.lojagasdistribuidor.Util.GPSRastreo;
import com.kradac.lojagasdistribuidor.Util.Gps;
import com.kradac.lojagasdistribuidor.Util.ReproducirTextAudio;
import com.kradac.lojagasdistribuidor.Util.UtilStream;
import com.kradac.lojagasdistribuidor.Util.VariablesGlobales;
import com.mapbox.services.commons.models.Position;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.transports.WebSocket;

import static com.kradac.lojagasdistribuidor.Util.Gps.REQUEST_CHECK_SETTINGS;

public class Servicio extends Service implements GPSRastreo.LocationListener {

    private static final String TAG = Servicio.class.getName();
    protected IBinder mBinder = new LocalBinder();
    protected Socket mSocket;
    protected boolean isConnect = false;
    protected OnComunicacionMainActivity onComunicacionMainActivity;
    protected OnComunicacionListaSolicitudes onComunicacionListaSolicitudes;
    protected OnComunicacionChat onComunicacionChat;
    protected OnComunicacionLogin onComunicacionLogin;
    protected OnComunicacionMensajesBroadCast onComunicacionMensajesBroadCast;
    private NotificationManager notificationManager;
    private Funciones funciones;
    private JSONObject datosSolicitud;
    private GpsLocationReceiver wfs;
    private PowerManager powerManager;
    private KeyguardManager keyguardManager;
    private PowerManager.WakeLock wakeLock;
    private ReproducirTextAudio reproducirTextAudio;
    private boolean tipoSolicitud = false;
    public ArrayList<ChatMessage> chatHistory, chatCallCenter, chatBroadCastEmpresa, chatBroadCast, chatPedidoPendiente;
    private int idMensaje = 0, count = 0, idMensajePedidoPendiente;
    private Location location;
    private GPSRastreo gps;
    private int statusLevel, temperaturaBateria;
    private String idSolicitudCc;

    public Servicio() {

    }

    public class LocalBinder extends Binder {
        public Servicio getService() {
            return Servicio.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void registrarUsuario(Activity activity, String nameClase) {
        switch (nameClase) {
            case "Login":
                onComunicacionLogin = (OnComunicacionLogin) activity;
                break;

            case "MainActivity":
                onComunicacionMainActivity = (OnComunicacionMainActivity) activity;
                break;

            case "ListaSolicitudes":
                onComunicacionListaSolicitudes = (OnComunicacionListaSolicitudes) activity;
                break;

            case "ChatActivity":
                onComunicacionChat = (OnComunicacionChat) activity;
                break;
            case "ChatBrodcast":
                onComunicacionMensajesBroadCast = (OnComunicacionMensajesBroadCast) activity;
                break;


        }
    }

    private TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {

        @SuppressLint("TrustAllX509TrustManager")
        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

        }

        @SuppressLint("TrustAllX509TrustManager")
        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[]{};
        }
    }};


    public void iniciarSesion(final String usuario, final String contrasenia) {
        if (mSocket != null && isConnect) {
            JSONObject object = new JSONObject();
            try {
                object.put("usuario", usuario);
                object.put("contrasenia", funciones.MD5(contrasenia));
                object.put("idDispositivo", funciones.obtenerIMEI(getApplicationContext()));
                object.put("idAplicativo", VariablesGlobales.NUM_ID_APLICATIVO);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.emit(VariablesGlobales.EMIT_LOGEAR_KTAXI, object, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.e(TAG, "InicioSesion: " + args[0].toString());
                    try {
                        JSONObject data = new JSONObject(args[0].toString());
                        if (data.has("estado")) {
                            if (data.getInt("estado") == 0) {
                                if (onComunicacionLogin != null) {
                                    onComunicacionLogin.mensajeActivacionDispositivo(data.getInt("iU"),
                                            data.getString("mensaje"));

                                }
                            } else if (data.getInt("estado") == -1) {
                                if (onComunicacionLogin != null) {
                                    onComunicacionLogin.mensajeErrorContrasena(data.getString("mensaje"));

                                }
                            } else if (data.getInt("estado") == 1) {
                                if (onComunicacionLogin != null) {
                                    String datosUsuario = args[0].toString();
                                    onComunicacionLogin.cerrarSesion(data.getString("mensaje"), datosUsuario);
                                }
                            } else if (data.getInt("estado") == -2) {
                                if (onComunicacionLogin != null) {
                                    onComunicacionLogin.mensajeDeuda(data.getString("mensaje"));
                                }
                            } else if (data.getInt("estado") == 6) {
                                if (onComunicacionLogin != null) {
                                    String datosUsuario = args[0].toString();
                                    Log.e("datosUser1", datosUsuario);
                                    onComunicacionLogin.logueoExitoso(data.getString("mensaje"), datosUsuario);
                                }
                            } else if (data.getInt("estado") == 4) {
                                if (onComunicacionLogin != null) {
                                    String datosUsuario = args[0].toString();
                                    Log.e("datosUser2", datosUsuario);
                                    onComunicacionLogin.seleccionDispositivo(datosUsuario);
                                }
                            } else if (data.getInt("estado") == -7) {
                                if (onComunicacionLogin != null) {
                                    onComunicacionLogin.dispositivoRegistrado(data.getString("mensaje"));
                                }
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }

    public void iniciarConexion() {
        try {
            SSLContext sc = null;
            try {
                sc = SSLContext.getInstance("TLS");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            try {
                sc.init(null, trustAllCerts, new SecureRandom());
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }
            IO.setDefaultSSLContext(sc);
            IO.Options options = new IO.Options();
            options.forceNew = true;
            options.reconnection = true;
            options.sslContext = sc;
            options.secure = true;
            options.transports = new String[]{WebSocket.NAME};
            mSocket = IO.socket(VariablesGlobales.IP_CONEXION, options);
            mSocket.connect();


            mSocket.on(Socket.EVENT_CONNECTING, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.e("Contectando", "si");
                }
            });
            mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if (mSocket != null && !isConnect) {
                        if (onComunicacionMainActivity != null) {
                            onComunicacionMainActivity.conectadoServ();
                        }
                        isConnect = true;
                        try {
                            String idAndroid = funciones.obtenerIMEI(getApplicationContext());
                            JSONObject object = new JSONObject(consultaLista(0).toString());

                            mSocket.emit(VariablesGlobales.EMIT_RECONECTAR_KTAXI,
                                    funciones.obtenerDatosUsuario(getApplicationContext()).getIdVehiculo(),
                                    funciones.obtenerDatosUsuario(getApplicationContext()).getIdCiudad(),
                                    funciones.obtenerDatosUsuario(getApplicationContext()).getIdEmpresa(),
                                    funciones.obtenerDatosUsuario(getApplicationContext()).getIdUsuario(),
                                    funciones.obtenerDatosUsuario(getApplicationContext()).getNombres() + " " +
                                            funciones.obtenerDatosUsuario(getApplicationContext()).getApellidos(),
                                    idAndroid, object, new Ack() {
                                        @Override
                                        public void call(Object... args) {
                                            Log.e("conexion", args[0].toString());
                                            try {
                                                JSONObject data = new JSONObject(args[0].toString());
                                                if (data != null) {
                                                    if (data.has("estado")) {
                                                        if (data.getInt("estado") == 1) {
                                                            if (onComunicacionMainActivity != null) {
                                                                onComunicacionMainActivity.dispositivoNoAutorizado(data.getString("mensaje"), data.getString("nombres"));
                                                            }
                                                        } else if (data.getInt("estado") == 3) {
                                                            String datosUsuario = args[0].toString();
                                                            if (onComunicacionMainActivity != null) {
                                                                onComunicacionMainActivity.actualizacionUsuario(datosUsuario);
                                                            }
                                                        }
                                                    }
                                                }

                                                boolean isSaltar = false;
                                            } catch (Exception e) {
                                            }
                                        }
                                    });


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void logueoPorVehiculo(int idVehiculo, int idCiudad,
                                  int idEmpresa, int idUsuario, final String datos, final Usuario usuario) {
        if (mSocket != null) {
            String idAndroid = funciones.obtenerIMEI(getApplicationContext());
            mSocket.emit(VariablesGlobales.EMIT_LOGEAR_KTAXI_POR_VEHICULO, idVehiculo, idCiudad,
                    idEmpresa, idUsuario,
                    idAndroid, new Ack() {
                        @Override
                        public void call(Object... args) {
                            Log.e("estadoLoginDispositivo", args[0].toString());
                            try {
                                JSONObject data = new JSONObject(args[0].toString());
                                if (data.has("estado")) {
                                    if (data.getInt("estado") == 5) {
                                        if (onComunicacionLogin != null) {
                                            onComunicacionLogin.cerrarSesionListaVehiculo(data.getString("mensaje"), usuario);
                                        }
                                    } else if (data.getInt("estado") == 7) {
                                        if (onComunicacionLogin != null) {
                                            onComunicacionLogin.logueoExitosoListaVehiculo(data.getString("mensaje"), usuario);
                                        }
                                    }
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
        }
    }

    public void cerrarSesionDispositivo(final String datos, int idVehiculo, int idCiudad,
                                        int idEmpresa, int idUsuario, String nombres) {
        if (mSocket != null && isConnect) {

            String idAndroid = funciones.obtenerIMEI(getApplicationContext());
            mSocket.emit(VariablesGlobales.EMIT_DESCONECTAR_KTAXI,
                    idVehiculo, idCiudad, idEmpresa, idUsuario, nombres, idAndroid, new Ack() {
                        @Override
                        public void call(Object... args) {
                            Log.e(TAG, "CerrarSesion: " + args[0].toString());
                            try {
                                JSONObject data = new JSONObject(args[0].toString());
                                if (data.getInt("estado") == 3) {
                                    if (onComunicacionLogin != null) {
                                        onComunicacionLogin.mensajeRespuesta(data.getString("mensaje"), datos);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e("errorCierreSesion", e.getMessage());
                            }

                        }
                    });
        }
    }

    public void cerrarSesionListaDispositivo(final Usuario usuario) {
        if (mSocket != null && isConnect) {

            String idAndroid = funciones.obtenerIMEI(getApplicationContext());
            mSocket.emit(VariablesGlobales.EMIT_DESCONECTAR_KTAXI,
                    usuario.getIdVehiculo(), usuario.getIdCiudad(), usuario.getIdEmpresa(),
                    usuario.getIdUsuario(), usuario.getNombres(), idAndroid, new Ack() {
                        @Override
                        public void call(Object... args) {
                            Log.e(TAG, "CerrarSesion: " + args[0].toString());
                            try {
                                JSONObject data = new JSONObject(args[0].toString());
                                if (data.getInt("estado") == 3) {
                                    if (onComunicacionLogin != null) {
                                        onComunicacionLogin.mensajeRespuestaCierreListaVehiculos(data.getString("mensaje"), usuario);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e("errorCierreSesion", e.getMessage());
                            }

                        }
                    });
        }
    }


    public void enviarChat(final int idSolicitud, int userName, String mensaje) {

        ChatMessage msg = new ChatMessage();
        msg.setId(122);
        msg.setMe(true);
        msg.setMessage(mensaje);
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
        msg.setDate(hourdateFormat.format(new Date()));
        chatHistory.add(msg);

        if (mSocket != null) {
            mSocket.emit(VariablesGlobales.EMIT_OPERADOR_ENVIA_MENSAJE,
                    idSolicitud,
                    userName,
                    mensaje, new Ack() {
                        @Override
                        public void call(Object... args) {
                            Log.e("ENVIAR_CHAT", args[0].toString());
                            obtenerMensaje();
                        }
                    });
        }

    }

    public void enviarChatPedidoPendiente(final int idSolicitud, final int userName,
                                          final String mensaje,final String barrio,final String nombreUsuario) {

        Log.e("datosSolicitud",idSolicitud+"");
        ChatMessage msg = new ChatMessage();
        msg.setId(123);
        msg.setMe(true);
        msg.setIdSolicitud(idSolicitud);
        msg.setMessage(mensaje);
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
        msg.setDate(hourdateFormat.format(new Date()));
        chatPedidoPendiente.add(msg);

        if (mSocket != null) {
            mSocket.emit(VariablesGlobales.EMIT_OPERADOR_ENVIA_MENSAJE,
                    idSolicitud,
                    userName,
                    mensaje, new Ack() {
                        @Override
                        public void call(Object... args) {
                            Log.e("ENVIAR_CHAT", args[0].toString());
                            obtenerMensajePedidoPendiente(idSolicitud,userName,barrio,nombreUsuario);
                        }
                    });
        }

    }

    public void enviarAudioBroasCast(String filename) {
        UtilStream.compressGzipFile(Chat.path + filename, Chat.path + "audioOut.gzip");
        byte[] data = UtilStream.fileToByteArray(Chat.path + "audioOut.gzip");
        File file = new File(Chat.path + "audioOut.gzip");
        file.delete();
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setTipo(1);
        chatMessage.setId(2);
        chatMessage.setNameArchivo(filename);
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
        chatMessage.setDate(hourdateFormat.format(new Date()));
        chatMessage.setMe(true);
        chatBroadCast.add(chatMessage);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("n", funciones.obtenerDatosUsuario(getApplicationContext()).getNombres());
            jsonObject.put("iE", funciones.obtenerDatosUsuario(getApplicationContext()).getIdEmpresa());
            jsonObject.put("iU", funciones.obtenerDatosUsuario(getApplicationContext()).getIdUsuario());
            jsonObject.put("u", funciones.obtenerDatosUsuario(getApplicationContext()).getUnidadVehiculo());
            jsonObject.put("iC", funciones.obtenerDatosUsuario(getApplicationContext()).getIdCiudad());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("enviar_audio_broadcast", jsonObject, data, new Ack() {
            @Override
            public void call(Object... args) {
                obtenerMensajesBroadCast();
            }
        });
    }

    public void enviarAudioBroasCastEmpresa(String filename) {
        UtilStream.compressGzipFile(Chat.path + filename, Chat.path + "audioOut.gzip");
        byte[] data = UtilStream.fileToByteArray(Chat.path + "audioOut.gzip");
        File file = new File(Chat.path + "audioOut.gzip");
        file.delete();
        JSONObject jsonObject = new JSONObject();
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setTipo(1);
        chatMessage.setId(2);
        chatMessage.setNameArchivo(filename);
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
        chatMessage.setDate(hourdateFormat.format(new Date()));
        chatMessage.setMe(true);
        chatBroadCastEmpresa.add(chatMessage);
        try {
            jsonObject.put("n", funciones.obtenerDatosUsuario(getApplicationContext()).getNombres());
            jsonObject.put("iE", funciones.obtenerDatosUsuario(getApplicationContext()).getIdEmpresa());
            jsonObject.put("iU", funciones.obtenerDatosUsuario(getApplicationContext()).getIdUsuario());
            jsonObject.put("u", funciones.obtenerDatosUsuario(getApplicationContext()).getUnidadVehiculo());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("enviar_audio_broadcast_por_empresa", jsonObject, data, new Ack() {
            @Override
            public void call(Object... args) {
                obtenerMensajesBroadCastEmpresa();
            }
        });
    }

    public void enviarAudioCallCenter(String filename) {
        UtilStream.compressGzipFile(Chat.path + filename, Chat.path + "audioOut.gzip");
        byte[] data = UtilStream.fileToByteArray(Chat.path + "audioOut.gzip");
        File file = new File(Chat.path + "audioOut.gzip");
        file.delete();
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setTipo(1);
        chatMessage.setId(2);
        chatMessage.setNameArchivo(filename);
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
        chatMessage.setDate(hourdateFormat.format(new Date()));
        chatMessage.setMe(true);
        chatCallCenter.add(chatMessage);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("n", funciones.obtenerDatosUsuario(getApplicationContext()).getNombres());
            jsonObject.put("iE", funciones.obtenerDatosUsuario(getApplicationContext()).getIdEmpresa());
            jsonObject.put("iU", funciones.obtenerDatosUsuario(getApplicationContext()).getIdUsuario());
            jsonObject.put("u", funciones.obtenerDatosUsuario(getApplicationContext()).getUnidadVehiculo());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("enviar_audio_call_center", jsonObject, data, new Ack() {
            @Override
            public void call(Object... args) {
                obtenerMensajesCallCenter();
            }
        });
    }

    public void enviarAudio(int userName, final int idSolicitud, String filename) {
        UtilStream.compressGzipFile(Chat.path + filename, Chat.path + "audioOut.gzip");
        byte[] data = UtilStream.fileToByteArray(Chat.path + "audioOut.gzip");
        File file = new File(Chat.path + "audioOut.gzip");
        file.delete();
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setTipo(1);
        chatMessage.setId(2);
        chatMessage.setNameArchivo(filename);
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
        chatMessage.setDate(hourdateFormat.format(new Date()));
        chatMessage.setMe(true);
        chatHistory.add(chatMessage);
        JSONObject object = new JSONObject();
        try {

            object.put("tipo", 2);
            object.put("to", userName);
            object.put("idPedido", idSolicitud);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (mSocket != null) {
            mSocket.emit(VariablesGlobales.EMIT_OPERADOR_ENVIA_MENSAJE_DATA, object, data, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.e("ENVIO_AUDIO", args[0].toString());
                    obtenerMensaje();
                }
            });
        }

    }

    public String nameAudio() {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
            String currentTimeStamp = dateFormat.format(new Date());
            return "ktaxi_" + currentTimeStamp + ".mp3";
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public StringBuilder consultaLista(int id) {
        StringBuilder objetodato;
        if (id > 0) {
            objetodato = new StringBuilder("{lista:[");
            objetodato.append(id);
            objetodato.append("]}");
        } else {
            objetodato = new StringBuilder("{lista:[]}");
        }
        return objetodato;
    }


    public StringBuilder posicionLoteNueva(PosicionNueva s) {
        StringBuilder lote = new StringBuilder();
        lote.append(s.getIdVehiculo());
        lote.append(",'");
        lote.append(s.getFecha().split(" ")[0]);
        lote.append("','");
        lote.append(s.getFecha().split(" ")[1]);
        lote.append("',");
        lote.append(s.getIdEquipo());
        lote.append(",");
        lote.append(s.getLatitud());
        lote.append(",");
        lote.append(s.getLongitud());
        lote.append(",");
        lote.append(s.getAltitud());
        lote.append(",");
        lote.append(s.getVelocidad());
        lote.append(",");
        lote.append(s.getAcury());
        lote.append(",");
        lote.append(s.getDireccion());
        lote.append(",");
        lote.append(s.getBateria());
        lote.append(",");
        lote.append(s.getConexion());
        lote.append(",");
        lote.append(s.getGps());
        lote.append(",");
        lote.append(s.getEstado());
        lote.append(",");
        lote.append(s.getTemperatura());
        lote.append(",");
        lote.append(s.getConsumo());
        lote.append(",");
        lote.append(s.getTipoRed());
        return lote;
    }

    public void avisoEntregarGas(final int idSolicitud, int userNameActiva) {

        JSONObject object = new JSONObject();
        try {
            object.put("idSolicitud", idSolicitud);
            object.put("estado", 9);
            object.put("abordadoPor", 5);
            mSocket.emit(VariablesGlobales.EMIT_ABORDAR_VEHICULO, userNameActiva, object, new Ack() {
                @Override
                public void call(Object... args) {
                    try {
                        JSONObject data = new JSONObject(args[0].toString());
                        Log.e("entregarPedido", args[0].toString());
                        if (data.has("en")) {
                            if (data.getInt("en") == 1) {
                                if (data.has("m")) {
                                    if (onComunicacionMainActivity != null) {
                                        onComunicacionMainActivity.confirmacionPedidoEntregado();
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void avisoEntregarGasPendiente(final int idSolicitud, int userNameActiva) {

        JSONObject object = new JSONObject();
        try {
            object.put("idSolicitud", idSolicitud);
            object.put("estado", 9);
            object.put("abordadoPor", 5);
            mSocket.emit(VariablesGlobales.EMIT_ABORDAR_VEHICULO, userNameActiva, object, new Ack() {
                @Override
                public void call(Object... args) {
                    try {
                        JSONObject data = new JSONObject(args[0].toString());
                        if (data.has("en")) {
                            if (data.getInt("en") == 1) {
                                if (data.has("m")) {
                                    if (onComunicacionMainActivity != null) {
                                        onComunicacionMainActivity.entregaGasPendiente(idSolicitud);
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void enviarValidacionCallCenter(int tipo) {
        if (location != null && location.getLatitude() != 0) {
            JSONObject data = new JSONObject();
            try {
                data.put("t", tipo);
                data.put("idSolicitudCc", idSolicitudCc);
                data.put("idEmpresa", funciones.obtenerDatosUsuario(getApplicationContext()).getIdEmpresa());
                data.put("idVehiculo", funciones.obtenerDatosUsuario(getApplicationContext()).getIdVehiculo());
                data.put("idUsuario", funciones.obtenerDatosUsuario(getApplicationContext()).getIdUsuario());
                data.put("latitud", location.getLatitude());
                data.put("longitud", location.getLongitude());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.emit(VariablesGlobales.EMIT_ESTADO_GPS_VALIDACION_CALLCENTER, data, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.e("respuestaValidacion1", args[0].toString());
                }
            });
        }
    }

    public void enviarValidacionEntregadoCallCenter(String idSolicitudCc, int tipo) {
        if (location != null && location.getLatitude() != 0) {
            JSONObject data = new JSONObject();
            try {
                data.put("t", tipo);
                data.put("idSolicitudCc", idSolicitudCc);
                data.put("idEmpresa", funciones.obtenerDatosUsuario(getApplicationContext()).getIdEmpresa());
                data.put("idVehiculo", funciones.obtenerDatosUsuario(getApplicationContext()).getIdVehiculo());
                data.put("idUsuario", funciones.obtenerDatosUsuario(getApplicationContext()).getIdUsuario());
                data.put("latitud", location.getLatitude());
                data.put("longitud", location.getLongitude());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.emit(VariablesGlobales.EMIT_ESTADO_GPS_VALIDACION_CALLCENTER, data, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.e("respuestaValidacion2", args[0].toString());
                    if (onComunicacionMainActivity != null) {
                        onComunicacionMainActivity.confirmacionPedidoEntregado();
                    }
                }
            });
        }
    }

    public void enviarValidacionEnLugarCallCenter(String idSolicitudCc, int tipo) {
        if (location != null && location.getLatitude() != 0) {
            JSONObject data = new JSONObject();
            try {
                data.put("t", tipo);
                data.put("idSolicitudCc", idSolicitudCc);
                data.put("idEmpresa", funciones.obtenerDatosUsuario(getApplicationContext()).getIdEmpresa());
                data.put("idVehiculo", funciones.obtenerDatosUsuario(getApplicationContext()).getIdVehiculo());
                data.put("idUsuario", funciones.obtenerDatosUsuario(getApplicationContext()).getIdUsuario());
                data.put("latitud", location.getLatitude());
                data.put("longitud", location.getLongitude());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.emit(VariablesGlobales.EMIT_ESTADO_GPS_VALIDACION_CALLCENTER, data, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.e("respuestaValidacion3", args[0].toString());
                }
            });
        }
    }

    public void enviarValidacionCancelacionCallCenter(String idSolicitudCc, int tipo) {
        if (location != null && location.getLatitude() != 0) {
            JSONObject data = new JSONObject();
            try {
                data.put("t", tipo);
                data.put("idSolicitudCc", idSolicitudCc);
                data.put("idEmpresa", funciones.obtenerDatosUsuario(getApplicationContext()).getIdEmpresa());
                data.put("idVehiculo", funciones.obtenerDatosUsuario(getApplicationContext()).getIdVehiculo());
                data.put("idUsuario", funciones.obtenerDatosUsuario(getApplicationContext()).getIdUsuario());
                data.put("latitud", location.getLatitude());
                data.put("longitud", location.getLongitude());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.emit(VariablesGlobales.EMIT_ESTADO_GPS_VALIDACION_CALLCENTER, data, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.e("respuestaValidacion4", args[0].toString());
                }
            });
        }
    }


    public void avisoEnLugar(int idSolicitud, int userNameUser, String aviso) {
        if (mSocket != null) {
            mSocket.emit(VariablesGlobales.EMIT_ENVIAR_AVISO, idSolicitud, userNameUser, aviso,
                    funciones.obtenerDatosUsuario(getApplicationContext()).getIdUsuario(), new Ack() {
                        @Override
                        public void call(Object... args) {
                            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                            if (funciones.obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                                v.vibrate(2000);
                                if (reproducirTextAudio != null) {
                                    reproducirTextAudio.speak("El aviso se ha enviado correctamente al cliente");
                                }
                            }
                        }
                    });

        }
    }

    private Emitter.Listener onDesconectar = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (mSocket != null) {
                if (!mSocket.connected()) {
                    isConnect = false;
                }
            }
        }
    };

    private Emitter.Listener onEscuchaEvento = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("ESCUCHANDO", args[0].toString());
            try {
                datosSolicitud = new JSONObject(args[0].toString());
                if (datosSolicitud != null) {
                    if (funciones.obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                        parcearSolicitud(datosSolicitud);

                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    private Emitter.Listener onAtendiendoSolicitud = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("ESCUCHANDO_ATENDIDO", args[0].toString());
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            try {
                JSONObject resultadoEnvioTiempo = new JSONObject(args[0].toString());
                if (resultadoEnvioTiempo != null) {
                    if (resultadoEnvioTiempo.getInt("estado") == 6) {
                        switch (resultadoEnvioTiempo.getInt("razon")) {
                            case 8:
                                v.vibrate(2000);
                                if (funciones.obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                                    if (reproducirTextAudio != null) {
                                        reproducirTextAudio.speak("El cliente no aceptó el tiempo");
                                    }

                                }
                                if (onComunicacionListaSolicitudes != null) {
                                    onComunicacionListaSolicitudes.noAceptoTiempo(resultadoEnvioTiempo.getInt("idSolicitud"));
                                }
                                break;
                            case 4:
                                if (onComunicacionListaSolicitudes != null) {
                                    onComunicacionListaSolicitudes.noAceptoTiempo(resultadoEnvioTiempo.getInt("idSolicitud"));
                                }
                                break;
                            case 5:
                                /*if (onComunicacionListaSolicitudes != null) {
                                    onComunicacionListaSolicitudes.noAceptoTiempo(resultadoEnvioTiempo.getInt("idSolicitud"));
                                }*/
                                /*if (onComunicacionListaSolicitudes != null) {
                                    onComunicacionListaSolicitudes.clienteCanceloPedido(resultadoEnvioTiempo.getInt("idSolicitud"));
                                }*/
                                if (onComunicacionMainActivity != null) {
                                    onComunicacionMainActivity.clienteCanceloPedido(resultadoEnvioTiempo.getInt("idSolicitud"));
                                }
                                break;
                            case 12:
                                if (onComunicacionMainActivity != null) {
                                    onComunicacionMainActivity.clienteCanceloPedido(resultadoEnvioTiempo.getInt("idSolicitud"));
                                }
                                if (onComunicacionListaSolicitudes != null) {
                                    onComunicacionListaSolicitudes.clienteCanceloPedido(resultadoEnvioTiempo.getInt("idSolicitud"));
                                }
                                break;
                            case 6:
                                if (onComunicacionListaSolicitudes != null) {
                                    onComunicacionListaSolicitudes.noAceptoTiempo(resultadoEnvioTiempo.getInt("idSolicitud"));
                                }
                                break;

                            case 1:
                                if (onComunicacionMainActivity != null) {
                                    onComunicacionMainActivity.clienteCanceloPedido(resultadoEnvioTiempo.getInt("idSolicitud"));
                                }
                                if (onComunicacionListaSolicitudes != null) {
                                    onComunicacionListaSolicitudes.clienteCanceloPedido(resultadoEnvioTiempo.getInt("idSolicitud"));
                                }
                                break;

                            case 2:
                                if (onComunicacionMainActivity != null) {
                                    onComunicacionMainActivity.clienteCanceloPedido(resultadoEnvioTiempo.getInt("idSolicitud"));
                                }
                                if (onComunicacionListaSolicitudes != null) {
                                    onComunicacionListaSolicitudes.clienteCanceloPedido(resultadoEnvioTiempo.getInt("idSolicitud"));
                                }
                                break;
                            case 3:
                                if (onComunicacionMainActivity != null) {
                                    onComunicacionMainActivity.clienteCanceloPedido(resultadoEnvioTiempo.getInt("idSolicitud"));
                                }
                                if (onComunicacionListaSolicitudes != null) {
                                    onComunicacionListaSolicitudes.clienteCanceloPedido(resultadoEnvioTiempo.getInt("idSolicitud"));
                                }
                                break;
                        }

                    } else if (resultadoEnvioTiempo.getInt("estado") == 5) {
                        if (onComunicacionListaSolicitudes != null) {
                            onComunicacionListaSolicitudes.aceptoTiempo(resultadoEnvioTiempo.getInt("idSolicitud"));
                        }
                    } else if (resultadoEnvioTiempo.getInt("estado") == 9) {
                        if (onComunicacionMainActivity != null) {
                            onComunicacionMainActivity.avisoClientePedidoEntregado(resultadoEnvioTiempo.getInt("idSolicitud"));
                        }

                    } else if (resultadoEnvioTiempo.getInt("estado") == 15) {
                        if (onComunicacionListaSolicitudes != null) {
                            onComunicacionListaSolicitudes.noAceptoTiempo(resultadoEnvioTiempo.getInt("idSolicitud"));
                        }

                    } else if (resultadoEnvioTiempo.getInt("estado") == 2) {
                        if (onComunicacionListaSolicitudes != null) {
                            onComunicacionListaSolicitudes.borrarSolicitud(resultadoEnvioTiempo.getInt("idSolicitud"));
                        }

                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    public LatLng localizacion() {
        LatLng localozacion = new LatLng(location.getLatitude(), location.getLongitude());
        return localozacion;
    }

    private Emitter.Listener onMensajes = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("ESCUCHA_CHAT", args[0].toString());
            try {
                JSONObject data = new JSONObject(args[0].toString());
                if (data != null) {

                    if (data.has("estado")) {
                        if (data.getInt("estado") == 2) {
                            if (onComunicacionListaSolicitudes != null) {
                                onComunicacionListaSolicitudes.cerrarSesion(data.getString("nombres"), data.getString("mensaje"));
                            }

                            if (onComunicacionMainActivity != null) {
                                onComunicacionMainActivity.cerrarSesion(data.getString("nombres"), data.getString("mensaje"));
                            }

                        }
                    } else {
                        if (data.getInt("idSolicitud") == funciones.obtenerIdSolicitudActiva(getApplicationContext()).getIdSolicitudActiva()) {
                            ChatMessage msg = new ChatMessage();
                            msg.setId(idMensaje);
                            msg.setMe(false);
                            msg.setMessage(data.getString("message"));
                            DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
                            msg.setDate(hourdateFormat.format(new Date()));
                            chatHistory.add(msg);
                            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                            if (funciones.obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                                v.vibrate(2000);
                                if (reproducirTextAudio != null) {
                                    reproducirTextAudio.speak("El cliente ha escrito " + data.getString("message"));
                                }
                            }

                            idMensaje++;

                            obtenerMensaje();

                        } else {
                            final List<Solicitud> solicitudList = funciones.obtenerSolicitud(getApplicationContext());
                            for (int i = 0; i < solicitudList.size(); i++) {
                                if (solicitudList.get(i).getEstadoSolicitud() == 3 && solicitudList.get(i).getIdSolicitud() == data.getInt("idSolicitud")
                                        || solicitudList.get(i).getEstadoSolicitud() == 3 && solicitudList.get(i).getIdSolicitud() == (data.getInt("idSolicitud") * -1)) {

                                    ChatMessage msg = new ChatMessage();
                                    msg.setId(idMensajePedidoPendiente);
                                    msg.setMe(false);
                                    msg.setIdSolicitud(data.getInt("idSolicitud"));
                                    msg.setMessage(data.getString("message"));
                                    DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
                                    msg.setDate(hourdateFormat.format(new Date()));
                                    chatPedidoPendiente.add(msg);
                                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                    if (funciones.obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                                        v.vibrate(2000);
                                        if (reproducirTextAudio != null) {
                                            reproducirTextAudio.speak("El cliente de un pedido pendiente ha escrito " + data.getString("message"));
                                        }
                                    }

                                    idMensajePedidoPendiente++;

                                    obtenerMensajePedidoPendiente(solicitudList.get(i).getIdSolicitud(),
                                            solicitudList.get(i).getUsername(),solicitudList.get(i).getDatosSolicitud().getBarrioCliente(),
                                            solicitudList.get(i).getDatosSolicitud().getNombres());
                                }
                            }
                        }
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    private Emitter.Listener onChatBroadCastCallCenter = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            try {
                JSONObject jsonObject = new JSONObject(args[0].toString());
                String nombreAudio = nameAudio();
                UtilStream.byteArrayToFile((byte[]) args[1], Chat.path + count + "audioIn.gzip");
                byte[] data = UtilStream.decompressGzip(UtilStream.fileToByteArray(Chat.path + count + "audioIn.gzip"), getApplicationContext());
                File file = new File(Chat.path + count + "audioIn.gzip");
                file.delete();
                if (data.length < 50) {
                    return;
                }
                if (UtilStream.byteArrayToFile(data, Chat.path + nombreAudio)) {
                    escucharAudios(Chat.path + nombreAudio);
                    ChatMessage msg = new ChatMessage();
                    msg.setTipo(1);
                    msg.setId(1);
                    msg.setMe(false);
                    msg.setNameArchivo(nombreAudio);
                    // msg.setMessage(message);
                    if (jsonObject.has("n")) {
                        msg.setDate(jsonObject.getString("n") + "\n" + DateFormat.getDateTimeInstance().format(new Date()));
                    } else {
                        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
                        msg.setDate(hourdateFormat.format(new Date()));
                    }
                    chatCallCenter.add(msg);
                    obtenerMensajesCallCenter();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    };

    private Emitter.Listener onAdioVehiculoPorUnidadEmpresa = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            try {
                JSONObject jsonObject = new JSONObject(args[0].toString());
                String nombreAudio = nameAudio();
                UtilStream.byteArrayToFile((byte[]) args[1], Chat.path + count + "audioIn.gzip");
                byte[] data = UtilStream.decompressGzip(UtilStream.fileToByteArray(Chat.path + count + "audioIn.gzip"), getApplicationContext());
                File file = new File(Chat.path + count + "audioIn.gzip");
                file.delete();
                if (data.length < 50) {
                    return;
                }
                if (UtilStream.byteArrayToFile(data, Chat.path + nombreAudio)) {
                    escucharAudios(Chat.path + nombreAudio);
                    ChatMessage msg = new ChatMessage();
                    msg.setTipo(1);
                    msg.setId(1);
                    msg.setMe(false);
                    msg.setNameArchivo(nombreAudio);
                    // msg.setMessage(message);
                    if (jsonObject.has("n")) {
                        msg.setDate(jsonObject.getString("n") + "\n" + DateFormat.getDateTimeInstance().format(new Date()));
                    } else {
                        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
                        msg.setDate(hourdateFormat.format(new Date()));
                    }
                    chatBroadCastEmpresa.add(msg);
                    obtenerMensajesBroadCastEmpresa();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    private Emitter.Listener onAudioBroadCast = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            try {
                JSONObject jsonObject = new JSONObject(args[0].toString());
                String nombreAudio = nameAudio();
                UtilStream.byteArrayToFile((byte[]) args[1], Chat.path + count + "audioIn.gzip");
                byte[] data = UtilStream.decompressGzip(UtilStream.fileToByteArray(Chat.path + count + "audioIn.gzip"), getApplicationContext());
                File file = new File(Chat.path + count + "audioIn.gzip");
                file.delete();
                if (data.length < 50) {
                    return;
                }
                if (UtilStream.byteArrayToFile(data, Chat.path + nombreAudio)) {
                    escucharAudios(Chat.path + nombreAudio);
                    ChatMessage msg = new ChatMessage();
                    msg.setTipo(1);
                    msg.setId(1);
                    msg.setMe(false);
                    msg.setNameArchivo(nombreAudio);
                    // msg.setMessage(message);
                    if (jsonObject.has("n")) {
                        msg.setDate(jsonObject.getString("n") + "\n" + DateFormat.getDateTimeInstance().format(new Date()));
                    } else {
                        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
                        msg.setDate(hourdateFormat.format(new Date()));
                    }
                    chatBroadCast.add(msg);
                    obtenerMensajesBroadCast();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };


    public void escucharAudios(String nombre) {
        try {
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
            mediaPlayer.setDataSource(nombre);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void obtenerMensaje() {
        if (onComunicacionMainActivity != null) {
            onComunicacionMainActivity.mensajesUsuario(chatHistory);
        }

        if (onComunicacionChat != null) {
            onComunicacionChat.mensajesUsuario(chatHistory);
        }
    }

    public void obtenerMensajePedidoPendiente(int idSolicitud, int userName,String barrio,String nombreUsuario) {
        if (onComunicacionMainActivity != null) {
            onComunicacionMainActivity.mensajesUsuarioPedidoPendiente(chatPedidoPendiente, idSolicitud, userName,
                    barrio,nombreUsuario);
        }
    }

    public void obtenerMensajesCallCenter() {
        if (onComunicacionMensajesBroadCast != null) {
            onComunicacionMensajesBroadCast.mensajesCallCenter(chatCallCenter);
        }
    }

    public void obtenerMensajesBroadCastEmpresa() {
        if (onComunicacionMensajesBroadCast != null) {
            onComunicacionMensajesBroadCast.mensajesBroadCastEmpresa(chatBroadCastEmpresa);
        }
    }

    public void obtenerMensajesBroadCast() {
        if (onComunicacionMensajesBroadCast != null) {
            onComunicacionMensajesBroadCast.mensajesBroadCast(chatBroadCast);
        }
    }

    public void borrarMensajeChat() {
        chatHistory.clear();
    }

    public void parcearSolicitud(JSONObject solicitud) {
        try {
            if (funciones.obtenerNumeroSolicitudesPendientes(getApplicationContext()).getNumeroSolicitudes() < 2) {
                JSONObject datoSolicitud = solicitud.getJSONObject("datosSolicitud");
                Solicitud nuevaSolicitud = new Solicitud();
                DatosSolicitud nuevoDatosSolicitud = new DatosSolicitud();

                // Enviar que llego la solicitud

                enviarEvento(location, 3, solicitud.getInt("idSolicitud"), 0);

                nuevaSolicitud.setIdSolicitud(solicitud.getInt("idSolicitud"));
                nuevaSolicitud.setUsername(solicitud.getInt("username"));

                nuevoDatosSolicitud.setReferenciaCliente(datoSolicitud.getString("referenciaCliente"));
                nuevoDatosSolicitud.setLatitud(datoSolicitud.getDouble("latitud"));
                nuevoDatosSolicitud.setCallePrincipal(datoSolicitud.getString("callePrincipal"));
                nuevoDatosSolicitud.setBarrioCliente(datoSolicitud.getString("barrioCliente"));
                nuevoDatosSolicitud.setLongitud(datoSolicitud.getDouble("longitud"));
                nuevoDatosSolicitud.setNombres(datoSolicitud.getString("nombres"));
                nuevoDatosSolicitud.setCelular(datoSolicitud.getString("celular"));

                if (datoSolicitud.has("idSolicitudCc")) {
                    nuevoDatosSolicitud.setIdSolicitudCc(datoSolicitud.getString("idSolicitudCc"));
                    idSolicitudCc = datoSolicitud.getString("idSolicitudCc");
                }

                if (solicitud.getInt("idSolicitud") > 0) {
                    nuevoDatosSolicitud.setCalleSecundaria(datoSolicitud.getString("calleSecundaria"));
                    nuevoDatosSolicitud.setDistancia(datoSolicitud.getDouble("distancia"));
                    nuevoDatosSolicitud.setTiempo(datoSolicitud.getInt("tiempo"));

                    if (datoSolicitud.has("conP")) {
                        nuevoDatosSolicitud.setConP(datoSolicitud.getInt("conP"));
                    }

                    String dat = datoSolicitud.getJSONArray("tiempos").toString();
                    JSONArray respJSON = new JSONArray(dat);
                    ArrayList<Integer> tiempos = new ArrayList<>();
                    for (int i = 0; i < respJSON.length(); i++) {
                        tiempos.add((Integer) respJSON.get(i));
                    }
                    nuevoDatosSolicitud.setTiempos(tiempos);

                    if (solicitud.has("lC")) {
                        JSONArray lcarac = solicitud.getJSONArray("lC");
                        List<Pedido> carac = new ArrayList<>();
                        for (int i = 0; i < lcarac.length(); i++) {
                            JSONObject data = lcarac.getJSONObject(i);
                            Pedido pedido = new Pedido();
                            pedido.setCaracteristica(data.getString("caracteristica"));
                            carac.add(pedido);
                        }
                        nuevaSolicitud.setlC(carac);
                    }

                } else {
                    nuevoDatosSolicitud.setIdSolicitudCc(datoSolicitud.getString("idSolicitudCc"));

                }

                nuevaSolicitud.setDatosSolicitud(nuevoDatosSolicitud);

                List<Solicitud> listaAnterior = funciones.obtenerSolicitud(getApplicationContext());
                List<Solicitud> nuevaLista = new ArrayList<>();
                nuevaLista.add(nuevaSolicitud);
                listaAnterior.addAll(nuevaLista);


                if (nuevaSolicitud.getIdSolicitud() <= 0) {
                    if (funciones.obtenerEstadoActividadConductor(getApplicationContext()).isEstadoActividadConductor()) {
                        nuevaSolicitud.setEstadoSolicitud(3);

                        funciones.guardarSolicitud(getApplicationContext(), listaAnterior);


                        if (onComunicacionListaSolicitudes != null) {
                            tipoSolicitud = false;
                            onComunicacionListaSolicitudes.solicitudAsignadaCallCenter();
                        }

                        if (onComunicacionMainActivity != null) {
                            tipoSolicitud = true;
                            onComunicacionMainActivity.solicitudAsignadaCallCenter();
                        }


                    } else {
                        nuevaSolicitud.setEstadoSolicitud(2);

                        funciones.guardarSolicitud(getApplicationContext(), listaAnterior);

                        if (onComunicacionListaSolicitudes != null) {
                            tipoSolicitud = false;
                            onComunicacionListaSolicitudes.solicitudAsignadaCallCenter();
                        }

                        if (onComunicacionMainActivity != null) {
                            tipoSolicitud = true;
                            onComunicacionMainActivity.solicitudAsignadaCallCenter();
                        }
                    }
                } else {

                    nuevaSolicitud.setEstadoSolicitud(1);
                    nuevaSolicitud.setPostulacion(false);

                    funciones.guardarSolicitud(getApplicationContext(), listaAnterior);

                    tipoSolicitud = false;
                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    if (funciones.obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                        v.vibrate(2000);
                        if (reproducirTextAudio != null) {
                            reproducirTextAudio.speak(datoSolicitud.getString("servicio") + datoSolicitud.getString("barrioCliente"));
                        }
                    }

                    if (onComunicacionListaSolicitudes != null) {
                        onComunicacionListaSolicitudes.nuevaSolicitud();
                    }


                }


                encenderPantalla();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void enviarTiempo(final Solicitud solicitud, int tiempo, int username) {
        mSocket.emit(VariablesGlobales.EMIT_ATENDER_SOLICITUD, username,
                objetoConDatosEnvio(solicitud, tiempo, 0, 4, 0),
                funciones.obtenerDatosUsuario(getApplicationContext()).getIdUsuario(), new Ack() {
                    @Override
                    public void call(Object... args) {
                        Log.e("envioTiempo: ", args[0].toString());
                        try {
                            JSONObject data = new JSONObject(args[0].toString());
                            if (data != null) {
                                if (data.getInt("estado") == -1) {
                                    if (onComunicacionListaSolicitudes != null) {
                                        onComunicacionListaSolicitudes.mensajeDeuda(data.getString("m"),
                                                solicitud.getIdSolicitud());
                                    }
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
    }


    public void calificacionCliente(double latitud, double longitud, int calificacion, String observacion, int idSolicitud) {
        JSONObject object = new JSONObject();
        try {
            object.put("lt", latitud);
            object.put("lg", longitud);
            object.put("vl", calificacion);
            object.put("ob", observacion);
            object.put("idS", idSolicitud);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (mSocket != null) {
            mSocket.emit("califica_conductor_solicitud", object, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.e("calificacionCliente", args[0].toString());
                    try {
                        JSONObject data = new JSONObject(args[0].toString());
                        if (data != null) {
                            if (data.getInt("en") == 1) {
                                if (onComunicacionMainActivity != null) {
                                    onComunicacionMainActivity.calificacionCliente(data.getString("m"));
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }


    public void cancelarSolicitud(final int userName, final double latitud,
                                  final double longitud, int idEventualidad, int estado,
                                  final int idSolicitud, final int calificacion,
                                  final String comentario) {
        final JSONObject object = new JSONObject();
        try {
            object.put("idSolicitud", idSolicitud);
            object.put("estado", estado);
            object.put("idEventualidad", idEventualidad);
            object.put("idUsuario", funciones.obtenerDatosUsuario(getApplicationContext()).getIdUsuario());

            if (mSocket != null) {
                mSocket.emit(VariablesGlobales.EMIT_CANCELAR_SOLICITUD_KTAXI,
                        userName, object, new Ack() {
                            @Override
                            public void call(Object... args) {
                                if (onComunicacionMainActivity != null) {
                                    onComunicacionMainActivity.cancelacionPedido();
                                }
                                if (calificacion != 0) {
                                    calificacionCliente(latitud, longitud, calificacion, comentario, idSolicitud);
                                }
                            }
                        });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private JSONObject objetoConDatosEnvio(Solicitud solicitud, int tiempo, double precio, int estado, double costo) {
        JSONObject object = new JSONObject();
        try {
            if (solicitud != null) {
                object.put("idSolicitud", solicitud.getIdSolicitud());
                object.put("atendidaDesde", 1);
                object.put("idEmpresa", funciones.obtenerDatosUsuario(getApplicationContext()).getIdEmpresa());
                object.put("idVehiculo", funciones.obtenerDatosUsuario(getApplicationContext()).getIdVehiculo());
                object.put("empresa", funciones.obtenerDatosUsuario(getApplicationContext()).getEmpresa());
                object.put("unidad", funciones.obtenerDatosUsuario(getApplicationContext()).getUnidadVehiculo());
                object.put("placa", funciones.obtenerDatosUsuario(getApplicationContext()).getPlacaVehiculo());
                object.put("regMunicipal", funciones.obtenerDatosUsuario(getApplicationContext()).getRegMunVehiculo());
                object.put("nombres", funciones.obtenerDatosUsuario(getApplicationContext()).getNombres());
                object.put("apellidos", funciones.obtenerDatosUsuario(getApplicationContext()).getApellidos());
                object.put("telefono", funciones.obtenerDatosUsuario(getApplicationContext()).getCelular());
                object.put("imagen", funciones.obtenerDatosUsuario(getApplicationContext()).getImagenConductor());
                object.put("costo", costo);
                object.put("distancia", solicitud.getDatosSolicitud().getDistancia());
                object.put("c", costo);
                object.put("precio", precio);
                object.put("tiempo", tiempo);
                switch (estado) {
                    case 4:
                        object.put("estado", 4);
                        break;
                    case 2:
                        object.put("estado", 2);
                        break;
                    case 5:
                        object.put("estado", 5);
                        break;
                    case 6:
                        object.put("estado", 6);
                        break;
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

    public void prenderEscucha() {
        if (mSocket != null) {
            mSocket.on(Socket.EVENT_DISCONNECT, onDesconectar);
            if (!mSocket.hasListeners(VariablesGlobales.ESCUCHA_EN_NUEVA_SOLICITUD)) {
                mSocket.on(VariablesGlobales.ESCUCHA_EN_NUEVA_SOLICITUD, onEscuchaEvento);
            }

            if (!mSocket.hasListeners(VariablesGlobales.ESCUCHA_EN_SOLICITUD)) {
                mSocket.on(VariablesGlobales.ESCUCHA_EN_SOLICITUD, onAtendiendoSolicitud);
            }
            if (!mSocket.hasListeners(VariablesGlobales.ESCUCHA_KTAXI_DRIVER_ESCUCHA_MENSAJES)) {
                mSocket.on(VariablesGlobales.ESCUCHA_KTAXI_DRIVER_ESCUCHA_MENSAJES, onMensajes);
            }
            if (!mSocket.hasListeners(VariablesGlobales.ESCUCHA_EN_AUDIO_VEHICULO_POR_UNIDAD)) {
                mSocket.on(VariablesGlobales.ESCUCHA_EN_AUDIO_VEHICULO_POR_UNIDAD, onChatBroadCastCallCenter);
            }
            if (!mSocket.hasListeners(VariablesGlobales.ESCUCHA_EN_AUDIO_BROADCAST_POR_EMPRESA)) {
                mSocket.on(VariablesGlobales.ESCUCHA_EN_AUDIO_BROADCAST_POR_EMPRESA, onAdioVehiculoPorUnidadEmpresa);
            }

            if (!mSocket.hasListeners(VariablesGlobales.ESCUCHA_EN_AUDIO_VEHICULO_POR_UNIDAD)) {
                mSocket.on(VariablesGlobales.ESCUCHA_EN_AUDIO_VEHICULO_POR_UNIDAD, onChatBroadCastCallCenter);
            }
            if (!mSocket.hasListeners(VariablesGlobales.ESCUCHA_EN_AUDIO_BROADCAST)) {
                mSocket.on(VariablesGlobales.ESCUCHA_EN_AUDIO_BROADCAST, onAudioBroadCast);
            }


        }

    }

    @Override
    public void onCreate() {
        super.onCreate();
        gps = new GPSRastreo(getApplicationContext());
        gps.setLocationListener(this);
        chatHistory = new ArrayList<>();
        chatPedidoPendiente = new ArrayList<>();
        chatCallCenter = new ArrayList<>();
        chatBroadCastEmpresa = new ArrayList<>();
        chatBroadCast = new ArrayList<>();
        reproducirTextAudio = new ReproducirTextAudio(this);
        funciones = new Funciones();
        registerReciver();
        iniciarConexion();
        prenderEscucha();

    }


    public void runAsForeground() {

        if (funciones.obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
            Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);

            @SuppressLint("WrongConstant") PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent,
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notification;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                String CHANNEL_ID = "Lojagas_conductor";
                CharSequence name = "Lojagas";
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                notification = new Notification.Builder(getApplicationContext(), CHANNEL_ID)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setChannelId(CHANNEL_ID)
                        .setContentTitle(getString(R.string.app_name))
                        .setAutoCancel(true)
                        .setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, 0))
                        .build();
                notificationManager.createNotificationChannel(mChannel);
            } else {
                NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
                builder.setSmallIcon(R.mipmap.ic_launcher);
                builder.setContentTitle(getString(R.string.app_name));
                builder.setContentIntent(pendingIntent);
                notification = builder.build();
            }

            startForeground(1000, notification);
        } else {
            stopForeground(true);
        }


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        runAsForeground();
        Intent intentServicio = new Intent();
        intentServicio.setAction("servicioDeFondo");
        sendBroadcast(intentServicio);
        if (rastreoHandle == null) {
            actualizarRastreoTaxi();
        }
        return START_STICKY;
    }


    private int TIEMPORASTREO = 10000;
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture<?> rastreoHandle = null;
    private Runnable rastreoActualiza;

    public void cerrar() {
        if (mSocket != null) {
            mSocket.disconnect();
            mSocket = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void cancelNotification(Context ctx, int notifyId) {
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancel(notifyId);
    }


    private void registerReciver() {
        wfs = new GpsLocationReceiver();
        IntentFilter iFilter = new IntentFilter();
        iFilter.addAction(GpsLocationReceiver.PRENDER_PANTALLA);
        registerReceiver(wfs, iFilter);
    }

    public void encenderPantalla() {
        Intent intent = new Intent(GpsLocationReceiver.PRENDER_PANTALLA);
        sendBroadcast(intent);
    }

    public class GpsLocationReceiver extends BroadcastReceiver {
        public static final String PRENDER_PANTALLA = "com.kradac.lojagasdistribuidor.PRENDER_PANTALLA";
        private static final String ACTION_IGNITION = "hk.topicon.intent.action.IGNITION";

        public GpsLocationReceiver() {
        }

        @SuppressLint({"WrongConstant", "InvalidWakeLockTag"})
        @Override
        public void onReceive(Context context, Intent intent) {

            if (funciones.obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                if (intent.getAction().equals(ACTION_IGNITION)) {
                    if (onComunicacionMainActivity != null) {
                        onComunicacionMainActivity.encenderPantalla();
                        powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                        keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
                        KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("INFO");
                        wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "INFO");
                        wakeLock.acquire(3000);
                    }
                }


                if (intent.getAction().equals(PRENDER_PANTALLA)) {

                    powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                    keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
                    KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("INFO");
                    wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "INFO");
                    wakeLock.acquire(3000);
                    Intent intentListaSolicitudes = new Intent(context, ListaSolicitudes.class);
                    intentListaSolicitudes.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    intentListaSolicitudes.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
                    if (tipoSolicitud) {
                        if (onComunicacionListaSolicitudes == null) {
                            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                            if (funciones.obtenerEstadoLogin(getApplicationContext()).isEstadoLogin()) {
                                v.vibrate(2000);
                                if (reproducirTextAudio != null) {
                                    reproducirTextAudio.speak("Se le ha asignado un pedido desde la central");
                                }
                            }
                        }
                    }
                    intentListaSolicitudes.putExtra("solicitudCentral", tipoSolicitud);
                    context.startActivity(intentListaSolicitudes);

                }

                if (intent.getAction().equals(Intent.ACTION_BATTERY_CHANGED)) {
                    statusLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                    temperaturaBateria = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0) / 10;
                }
            }


        }
    }


    @Override
    public void onLocationChange(Location location) {
        if (location != null) {
            this.location = location;
        }
    }

    @Override
    public void onGooglePlayServicesDisable(int status) {

    }

    @Override
    public void onGpsDisableNewApi(Status status) {

    }

    @Override
    public void onGpsDisable() {

    }


    public void enviarRastreo() {
        PosicionNueva posicionNueva;
        if (location != null) {
            posicionNueva = new PosicionNueva();
            posicionNueva.setIdVehiculo(funciones.obtenerDatosUsuario(getApplicationContext()).getIdVehiculo());
            posicionNueva.setFecha(funciones.getTimeDeLocation(location, this));
            posicionNueva.setIdEquipo(funciones.obtenerDatosUsuario(getApplicationContext()).getIdEquipo());
            posicionNueva.setLatitud(funciones.formatDecimales(location.getLatitude(), 6));
            posicionNueva.setLongitud(funciones.formatDecimales(location.getLongitude(), 6));
            posicionNueva.setAltitud((int) location.getAltitude());
            posicionNueva.setVelocidad((int) ((location.getSpeed() * 3600) / 1000));
            posicionNueva.setAcury(funciones.getAcuryTramaEnvio((double) location.getAccuracy()));
            posicionNueva.setDireccion(funciones.valorMaximo((int) location.getBearing(), 32767));
            posicionNueva.setBateria(funciones.valorMaximo(funciones.getBatteryLevel(this), 127));
            posicionNueva.setConexion(funciones.getConnectivityStatus(getBaseContext()));
            posicionNueva.setGps(funciones.statusGPS(this));
            if (funciones.obtenerEstadoActividadConductor(getApplicationContext()).isEstadoActividadConductor()) {
                posicionNueva.setEstado(1);
            } else {
                posicionNueva.setEstado(0);
            }

            posicionNueva.setTemperatura(funciones.valorMaximo(temperaturaBateria, 127));
            posicionNueva.setConsumo(funciones.formatDecimales(funciones.getPakagesUsoDatos(this), 2));
            posicionNueva.setTipoRed(funciones.getTipoRed(this));
        } else {
            posicionNueva = new PosicionNueva();
            posicionNueva.setIdVehiculo(funciones.obtenerDatosUsuario(getApplicationContext()).getIdVehiculo());
            posicionNueva.setFecha(funciones.getTimeDeLocation(location, this));
            posicionNueva.setIdEquipo(funciones.obtenerDatosUsuario(getApplicationContext()).getIdEquipo());
            posicionNueva.setLatitud(0);
            posicionNueva.setLongitud(0);
            posicionNueva.setAltitud(0);
            posicionNueva.setVelocidad(0);
            posicionNueva.setAcury(0);
            posicionNueva.setDireccion(0);
            posicionNueva.setBateria(funciones.valorMaximo(funciones.getBatteryLevel(this), 127));
            posicionNueva.setConexion(funciones.getConnectivityStatus(getBaseContext()));
            posicionNueva.setGps(funciones.statusGPS(this));
            if (funciones.obtenerEstadoActividadConductor(getApplicationContext()).isEstadoActividadConductor()) {
                posicionNueva.setEstado(1);
            } else {
                posicionNueva.setEstado(0);
            }
            posicionNueva.setTemperatura(funciones.valorMaximo(temperaturaBateria, 127));
            posicionNueva.setConsumo(funciones.formatDecimales(funciones.getPakagesUsoDatos(this), 2));
            posicionNueva.setTipoRed(funciones.getTipoRed(this));
        }
        enviarRastreoServer(posicionNueva);

    }

    private void actualizarRastreoTaxi() {
        rastreoActualiza = new Runnable() {
            @Override
            public void run() {
                enviarRastreo();
                rastreoHandle.cancel(true);
                rastreoHandle = scheduler.scheduleAtFixedRate(rastreoActualiza, TIEMPORASTREO, TIEMPORASTREO, TimeUnit.MILLISECONDS);
            }
        };

        rastreoHandle = scheduler.scheduleAtFixedRate(rastreoActualiza, TIEMPORASTREO, TIEMPORASTREO, TimeUnit.MILLISECONDS);
    }

    public void enviarRastreoServer(PosicionNueva position) {
        if (mSocket != null) {
            mSocket.emit(VariablesGlobales.EMIT_ENVIAR_LOCALIZACION, posicionLoteNueva(position), new Ack() {
                @Override
                public void call(Object... args) {

                }
            });
        }

    }

    public void enviarEvento(Location location, int tipo, int id, int iPS) {
        if (location != null) {
            JSONObject object = new JSONObject();
            try {
                // tipo 1 panico
                // tipo 2 posibles solicitudes
                // tipo 3 ack de llego solicitud
                // tipo 4 recivo pedido
                // tipo 5 entregado de posible solicitud
                object.put("usuario", funciones.obtenerDatosUsuario(getApplicationContext()).getNombres() + " " + funciones.obtenerDatosUsuario(getApplicationContext()).getNombres());
                object.put("empresa", funciones.obtenerDatosUsuario(getApplicationContext()).getEmpresa());
                object.put("numeroUnidad", funciones.obtenerDatosUsuario(getApplicationContext()).getUnidadVehiculo());
                object.put("idAplicativo", VariablesGlobales.NUM_ID_APLICATIVO);
                object.put("t", tipo);
                object.put("iPS", iPS); //Id Posible solicitud
                if (location == null) {
                    object.put("lt", 0);
                    object.put("lg", 0);
                } else {
                    object.put("lt", location.getLatitude());
                    object.put("lg", location.getLongitude());

                }
                if (tipo == 4) {
                    object.put("iP", id);
                } else {
                    object.put("iS", id);
                }
                if (mSocket != null) {

                    mSocket.emit("notificar_evento",
                            funciones.obtenerDatosUsuario(getApplicationContext()).getIdUsuario(),
                            funciones.obtenerDatosUsuario(getApplicationContext()).getIdCiudad(),
                            funciones.obtenerDatosUsuario(getApplicationContext()).getIdVehiculo(),
                            object);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}
