package com.kradac.lojagasdistribuidor.Util;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;


public class CustomEventMapView extends MapView {

    private ScaleGestureDetector gestureDetector;
    private GoogleMap googleMap;
    private int fingers = 0;
    private long lastZoomTime = 0;
    private float lastSpan = -1;
    private Handler handler = new Handler();

    public CustomEventMapView.onTouchListerner getOnTouchListerner() {
        return onTouchListerner;
    }

    public void setOnTouchListerner(CustomEventMapView.onTouchListerner onTouchListerner) {
        this.onTouchListerner = onTouchListerner;
    }

    private onTouchListerner onTouchListerner;

    public CustomEventMapView(Context context, GoogleMapOptions options) {
        super(context, options);
    }

    public CustomEventMapView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomEventMapView(Context context) {
        super(context);
    }

    @Override
    public void getMapAsync(final OnMapReadyCallback callback) {
        super.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap googleMap) {
                gestureDetector = new ScaleGestureDetector(getContext(), new ScaleGestureDetector.OnScaleGestureListener() {
                    @Override
                    public boolean onScale(ScaleGestureDetector detector) {
                        if (lastSpan == -1) {
                            lastSpan = detector.getCurrentSpan();
                        } else if (detector.getEventTime() - lastZoomTime >= 50) {
                            lastZoomTime = detector.getEventTime();
                            googleMap.animateCamera(CameraUpdateFactory.zoomBy(getZoomValue(detector.getCurrentSpan(), lastSpan)), 50, null);
                            lastSpan = detector.getCurrentSpan();
                        }
                        return true;
                    }

                    @Override
                    public boolean onScaleBegin(ScaleGestureDetector detector) {
                        lastSpan = -1;
                        return true;
                    }

                    @Override
                    public void onScaleEnd(ScaleGestureDetector detector) {
                        lastSpan = -1;
                    }
                });
                CustomEventMapView.this.googleMap = googleMap;
                callback.onMapReady(googleMap);
            }
        });
    }

    private  boolean bloquearScroll;
    public void disableScrool(){
        bloquearScroll=true;
    }
    public void enableScrool(){
        bloquearScroll=false;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (onTouchListerner != null) {
            onTouchListerner.onTouch(ev);
        }
        if (bloquearScroll) {
            disableScrolling();
            return false;
        } else {
            if (ev.getAction() == MotionEvent.ACTION_UP && googleMap != null) {
                googleMap.getUiSettings().setAllGesturesEnabled(false);
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(googleMap.getCameraPosition().target));
            }
            switch (ev.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_POINTER_DOWN:
                    fingers = fingers + 1;
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                    fingers = fingers - 1;
                    break;
                case MotionEvent.ACTION_UP:
                    fingers = 0;
                    break;
                case MotionEvent.ACTION_DOWN:
                    fingers = 1;
                    break;
            }
            if (fingers > 1) {
                disableScrolling();
            } else if (fingers < 1) {
                enableScrolling();
            }
            if (fingers > 1) {
                if (gestureDetector != null) {
                    return gestureDetector.onTouchEvent(ev);
                } else {
                    return super.dispatchTouchEvent(ev);
                }
            } else {
                return super.dispatchTouchEvent(ev);
            }
        }

    }

    public interface onTouchListerner {
        void onTouch(MotionEvent event);
    }


    private void enableScrolling() {
        if (googleMap != null && !googleMap.getUiSettings().isScrollGesturesEnabled()) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    googleMap.getUiSettings().setAllGesturesEnabled(true);
                }
            }, 50);
        }
    }

    private void disableScrolling() {
        handler.removeCallbacksAndMessages(null);
        if (googleMap != null && googleMap.getUiSettings().isScrollGesturesEnabled()) {
            googleMap.getUiSettings().setAllGesturesEnabled(false);
        }
    }

    private float getZoomValue(float currentSpan, float lastSpan) {
        double value = (Math.log(currentSpan / lastSpan) / Math.log(1.55d));
        return (float) value;
    }

}