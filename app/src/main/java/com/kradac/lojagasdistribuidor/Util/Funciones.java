package com.kradac.lojagasdistribuidor.Util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kradac.lojagasdistribuidor.Clase.Preferencia;
import com.kradac.lojagasdistribuidor.Modelo.Solicitud;
import com.kradac.lojagasdistribuidor.Modelo.Usuario;
import com.kradac.lojagasdistribuidor.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Funciones extends AppCompatActivity {

    private ProgressDialog pDialog;
    private final String preferenceConfig = "config";
    public final static String preference1 = "login";
    public final static String preference2 = "datosUsuario";
    public final static String preference3 = "solicitud";
    public final static String preference4 = "estadoActividadConductor";
    public final static String preference5 = "numeroSolicitudesPendiente";
    public final static String preference6 = "seguimiento";
    public final static String preference7 = "idSolicitudActiva";

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;

    private ImageLoader imageLoader;

    protected PowerManager.WakeLock wakelock;

    public Funciones() {
    }


    public String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException ignored) {
        }
        return null;
    }

    @SuppressLint("MissingPermission")
    public String obtenerIMEI(Context context) {
        final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return telephonyManager.getImei();
        } else {
            return telephonyManager.getDeviceId();
        }

    }

    public int statusGPS(Context context) {
        if (this.comprobarGPS(context)) {
            return 1;
        } else {
            return 0;
        }
    }

   /* public int temperatura(){
        int temperaturaBateria=0;
        if (intent.getAction().equals(Intent.ACTION_BATTERY_CHANGED)) {
            statusLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            temperaturaBateria = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0) / 10;
        }
        return temperaturaBateria;
    }*/

    public double formatDecimales(double num, int decimales) {
        if (decimales < 0) throw new IllegalArgumentException();
        long factor = (long) Math.pow(10, decimales);
        num = num * factor;
        long tmp = Math.round(num);
        return (double) tmp / factor;
    }

    public double getPakagesUsoDatos(Context context) {
        try {
            ApplicationInfo app = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0);
            int UID = app.uid;
            double received = (double) TrafficStats.getUidRxBytes(UID) / (1024 * 1024);
            double send = (double) TrafficStats.getUidTxBytes(UID) / (1024 * 1024);
            return received + send;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    public void cerrarTeclado(EditText edit) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(edit.getWindowToken(), 0);
    }

    public int getTipoRed(Context context) {
        TelephonyManager teleMan =
                (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return teleMan.getNetworkType();
    }

    public String fechaLocalizacion(Location location, Context context) {
        SimpleDateFormat sdfBase = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date itemDate;
        if (location != null) {
            if (location.getTime() != 0 && comprobarGPS(context)) {
                itemDate = new Date(location.getTime());
            } else {
                itemDate = new Date();
            }
        } else {
            itemDate = new Date();
        }
        return sdfBase.format(itemDate);
    }

    public boolean comprobarGPS(Context context) {
        try {
            final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            return true;
        }
    }

    public int getAcuryTramaEnvio(double acury) {
        int envio = 0;
        if (acury <= 50) {
            envio = 1;
        } else if (acury >= 51 && acury <= 100) {
            envio = 2;
        } else if (acury >= 101 && acury <= 250) {
            envio = 3;
        } else if (acury >= 251 && acury <= 500) {
            envio = 4;
        } else if (acury >= 501 && acury <= 650) {
            envio = 5;
        } else if (acury >= 651 && acury <= 1000) {
            envio = 6;
        } else if (acury >= 1001 && acury <= 2000) {
            envio = 7;
        } else if (acury >= 2001 && acury <= 4000) {
            envio = 8;
        } else if (acury >= 4001) {
            envio = 9;
        }
        return envio;
    }

    public int valorMaximo(int numero, int valorMaximo) {
        if (numero < valorMaximo) {
            return numero;
        } else {
            return 0;
        }
    }

    public int getBatteryLevel(Context context) {
        Intent batteryIntent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        // Error checking that probably isn't needed but I added just in case.
        if (level == -1 || scale == -1) {
            return (int) 50.0f;
        }
        float a = ((float) level / (float) scale) * 100.0f;
        return (int) a;
    }

    public int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public void obtenerImagen(final ImageView imageView, String url, Context context) {
        DisplayImageOptions options = new DisplayImageOptions.Builder().bitmapConfig(Bitmap.Config.RGB_565).cacheOnDisk(false).build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        imageLoader.displayImage("http://sweb.ktaxi.com.ec/ktaxi/img/uploads/people/" + url, imageView, options, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        super.onLoadingComplete(imageUri, view, loadedImage);
                        if (imageUri != null) {
                            imageView.setImageBitmap(loadedImage);
                        } else {
                            imageView.setImageDrawable(getResources().getDrawable(R.mipmap.ic_launcher));
                        }
                    }
                }, new ImageLoadingProgressListener() {
                    @Override
                    public void onProgressUpdate(String imageUri, View view, int current, int total) {
                        imageView.setVisibility(View.GONE);
                    }
                }

        );
    }


    public void mensaje() {
        new AlertDialog.Builder(Funciones.this)
                .setTitle("Aviso")
                .setMessage("Verifique el acceso ha internet o red de datos del dispositivo")
                .setCancelable(false)
                .setPositiveButton("REINTENTAR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        checkConnection();
                    }
                })
                .setNegativeButton("SALIR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).show();
    }

    public void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        if (!isConnected) {
            mensaje();
        }
    }

    protected void mostrarProgressDialog(final Activity activity, final String mensaje) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ocultarProgressDialog();
                pDialog = new ProgressDialog(activity);
                pDialog.setMessage(mensaje);
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
            }
        });

    }

    protected void mostrarProgressDialogEnvioTiempo(final Activity activity, final String mensaje) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ocultarProgressDialog();
                pDialog = new ProgressDialog(activity);
                pDialog.setMessage(mensaje);
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
                pDialog.show();

            }
        });

    }


    protected void ocultarProgressDialog() {
        if (pDialog != null) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
        }

    }

    public String obtenerMarca() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return model;
        }
        return manufacturer + " " + model;
    }

    public String obtenerModelo() {
        return android.os.Build.MODEL;
    }

    public String obtenerVersion(Context context) {
        String versionName;
        try {
            versionName = context.getPackageManager().getPackageInfo(context.getPackageName(),
                    0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
        return versionName;
    }

    public void enviarWhatsapp(final Activity activity, final String numero) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (numero.length() == 10) {
                    if (estaInstaladaAplicacion("com.whatsapp", activity)) {
                        String url = "https://api.whatsapp.com/send?phone=".concat(obtenerNumero(numero));
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);
                    } else {
                        Toast.makeText(activity, "Usted no tiene instalado whatsapp", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(activity, "El número que intenta enviar el mensaje no es válido.", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public String getNameOperator(Context context) {
        TelephonyManager telephonyManager = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE));
        String operatorName = telephonyManager.getNetworkOperatorName();
        if (!operatorName.equalsIgnoreCase("")) {
            return operatorName;
        } else {
            return "SN";
        }
    }

    public void calificarApp(Context context) {
        final String my_package_name = context.getPackageName();
        String url = "";

        try {
            this.getPackageManager().getPackageInfo("com.android.vending", 0);

            url = "market://details?id=" + my_package_name;
        } catch (final Exception e) {
            url = "https://play.google.com/store/apps/details?id=" + my_package_name;
        }


        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        startActivity(intent);

    }

    public String getTimeDeLocation(Location location, Context context) {
        SimpleDateFormat sdfBase = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date itemDate;
        if (location != null) {
            if (location.getTime() != 0 && comprobarGPS(context)) {
                itemDate = new Date(location.getTime());
            } else {
                itemDate = new Date();
            }
        } else {
            itemDate = new Date();
        }
        return sdfBase.format(itemDate);
    }


    public String obtenerNumero(String numero) {
        String num;
        num = numero.substring(1);
        return "593".concat(num);
    }

    public boolean estaInstaladaAplicacion(String nombrePaquete, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(nombrePaquete, PackageManager.GET_ACTIVITIES);
            pm.getPackageInfo(nombrePaquete, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public String obtenerVersionSo() {
        return android.os.Build.VERSION.RELEASE;
    }

    public void guardarEstadoLogin(Preferencia preferencia, Context context) {
        SharedPreferences mPrefs = getSharedPreferences(preference1, context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(preferencia);
        prefsEditor.putString(preferenceConfig, json);
        prefsEditor.apply();
    }

    public Preferencia obtenerEstadoLogin(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(preference1, context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString(preferenceConfig, "");
        Preferencia preferencia = gson.fromJson(json, Preferencia.class);
        if (preferencia == null) return new Preferencia();
        return preferencia;
    }


    public void guardarDatosUsuario(Usuario preferencia) {
        SharedPreferences mPrefs = getSharedPreferences(preference2, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(preferencia);
        prefsEditor.putString(preferenceConfig, json);
        prefsEditor.apply();
    }

    public Usuario obtenerDatosUsuario(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(preference2, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString(preferenceConfig, "");
        Usuario preferencia = gson.fromJson(json, Usuario.class);
        if (preferencia == null) return new Usuario();
        return preferencia;
    }

    public void guardarSolicitud(Context context, List<Solicitud> preferencia) {
        SharedPreferences mPrefs = context.getSharedPreferences(preference3, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(preferencia);
        prefsEditor.putString(preferenceConfig, json);
        prefsEditor.apply();
    }

    public List<Solicitud> obtenerSolicitud(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(preference3, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString(preferenceConfig, "");
        Type listType = new TypeToken<List<Solicitud>>() {
        }.getType();
        List<Solicitud> preferencia = gson.fromJson(json, listType);
        if (preferencia == null) return new ArrayList<>();
        return preferencia;
    }

    public void guardarEstadoActividadConductor(Preferencia preferencia, Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(preference4, context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(preferencia);
        prefsEditor.putString(preferenceConfig, json);
        prefsEditor.apply();
    }

    public Preferencia obtenerEstadoActividadConductor(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(preference4, context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString(preferenceConfig, "");
        Preferencia preferencia = gson.fromJson(json, Preferencia.class);
        if (preferencia == null) return new Preferencia();
        return preferencia;
    }

    public void guardarNumeroSolicitudesPendientes(Preferencia preferencia, Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(preference5, context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(preferencia);
        prefsEditor.putString(preferenceConfig, json);
        prefsEditor.apply();
    }

    public Preferencia obtenerNumeroSolicitudesPendientes(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(preference5, context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString(preferenceConfig, "");
        Preferencia preferencia = gson.fromJson(json, Preferencia.class);
        if (preferencia == null) return new Preferencia();
        return preferencia;
    }

    public void guardarSeguimiento(Preferencia preferencia, Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(preference6, context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(preferencia);
        prefsEditor.putString(preferenceConfig, json);
        prefsEditor.apply();
    }

    public Preferencia obtenerSeguimiento(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(preference6, context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString(preferenceConfig, "");
        Preferencia preferencia = gson.fromJson(json, Preferencia.class);
        if (preferencia == null) return new Preferencia();
        return preferencia;
    }

    public void guardarIdSolicitudActiva(Preferencia preferencia, Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(preference7, context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(preferencia);
        prefsEditor.putString(preferenceConfig, json);
        prefsEditor.apply();
    }

    public Preferencia obtenerIdSolicitudActiva(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(preference7, context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString(preferenceConfig, "");
        Preferencia preferencia = gson.fromJson(json, Preferencia.class);
        if (preferencia == null) return new Preferencia();
        return preferencia;
    }

    public void clearIdSolicitudActiva() {
        SharedPreferences mPrefs = getSharedPreferences(preference7, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.clear().commit();
    }

    public void clearSeguimiento() {
        SharedPreferences mPrefs = getSharedPreferences(preference6, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.clear().commit();
    }

    public void clearNumeroSolicitudesPendiente() {
        SharedPreferences mPrefs = getSharedPreferences(preference5, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.clear().commit();
    }

    public void clearEstadoLogin() {
        SharedPreferences mPrefs = getSharedPreferences(preference1, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.clear().commit();
    }

    public void clearDatosUsuario() {
        SharedPreferences mPrefs = getSharedPreferences(preference2, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.clear().commit();
    }

    public void clearSolicitud() {
        SharedPreferences mPrefs = getSharedPreferences(preference3, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.clear().commit();
    }

    public void clearEstadoActividadConductor() {
        SharedPreferences mPrefs = getSharedPreferences(preference4, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.clear().commit();
    }


}
