package com.kradac.lojagasdistribuidor.Util;

import android.animation.TypeEvaluator;

import com.google.android.gms.maps.model.LatLng;

public class LatLngGoogleEvaluator implements TypeEvaluator<LatLng> {

    private LatLng latLng = new LatLng(0,0);

    @Override
    public LatLng evaluate(float fraction, LatLng startValue, LatLng endValue) {
        latLng=new LatLng(startValue.latitude
                + ((endValue.latitude - startValue.latitude) * fraction),startValue.longitude
                + ((endValue.longitude - startValue.longitude) * fraction));

        return latLng;
    }
}