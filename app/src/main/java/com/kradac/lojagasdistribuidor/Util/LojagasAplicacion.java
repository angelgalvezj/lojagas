package com.kradac.lojagasdistribuidor.Util;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

public class LojagasAplicacion extends Application {

    private static LojagasAplicacion mInstance;

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;


    }

    public static synchronized LojagasAplicacion getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

}
