package com.kradac.lojagasdistribuidor.Util;

public class VariablesGlobales {

    //IP CONEXION PRODUCCION
    //public static final String IP_CONEXION = "http://169.62.217.181/";
    //IP CONEXION DESARROLLO
    public static final String IP_CONEXION = "http://169.62.217.189/";

    public static final String EMIT_RECONECTAR_KTAXI = "reconectar_ktaxi";
    public static final int NUM_ID_APLICATIVO = 8;
    public static final String EMIT_LOGEAR_KTAXI = "logear_ktaxi_aplicativo";
    public static final String EMIT_DESCONECTAR_KTAXI = "desconectar_ktaxi";
    public static final String EMIT_ENVIAR_LOCALIZACION = "t";
    public static final String ESCUCHA_EN_NUEVA_SOLICITUD = "en_nueva_solicitud";
    public static final String EMIT_ATENDER_SOLICITUD = "atender_solicitud";
    public static final String ESCUCHA_EN_SOLICITUD = "en_solicitud";
    public static final String EMIT_ENVIAR_AVISO = "enviar_aviso";
    public static final String EMIT_ABORDAR_VEHICULO = "abordar_vehiculo";
    public static final String EMIT_CANCELAR_SOLICITUD_KTAXI = "cancelar_solicitud_ktaxi";
    public static final String EMIT_OPERADOR_ENVIA_MENSAJE = "operador_envia_mensaje";
    public static final String EMIT_OPERADOR_ENVIA_MENSAJE_DATA = "operador_envia_audio";
    public static final String ESCUCHA_KTAXI_DRIVER_ESCUCHA_MENSAJES = "operador_escucha_mensaje";
    public static final String ESCUCHA_EN_AUDIO_VEHICULO_POR_UNIDAD = "en_audio_vehiculo_por_unidad";
    public static final String ESCUCHA_EN_AUDIO_BROADCAST_POR_EMPRESA = "en_audio_broadcast_por_empresa";
    public static final String ESCUCHA_EN_AUDIO_BROADCAST = "en_audio_broadcast";
    public static final String EMIT_LOGEAR_KTAXI_POR_VEHICULO = "logear_ktaxi_por_vehiculo";
    public static final String EMIT_ESTADO_GPS_VALIDACION_CALLCENTER = "aceptacion_solicitud_cc";
}
